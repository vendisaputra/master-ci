function swalConfirm(text, func = null, canc = null) {
    Swal.fire({
        title: 'Konfirmasi!',
        type: 'question',
        html: text,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ya',
        cancelButtonText: '<i class="fa fa-thumbs-down"></i> Tidak'
    }).then(function (result) {
        if (result.value) {
            if (func) {
                func(result.value);
            }
        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            canc();
        }
    });
}

function swalSuccess(text, func = null) {
    Swal.fire("Sukses!", text, "success").then(function (result) {
        if (result.value) {
            if (func) {
                func(result.value);
            }
        }
    });
}

function swalAlert(text, func = null) {
    Swal.fire("Gagal!", text, "error").then(function (result) {
        if (result.value) {
            if (func) {
                func(result.value);
            }
        }
    });
}

function swalLoad(text, func = null) {
    Swal.fire(
        {
            title: text,
            onBeforeOpen: function onBeforeOpen() {
                Swal.showLoading();
            },
            onClose: function onClose() {
                func()
            }
        }
    );
}

/**
 * Modified by fajar at 1/29/20
 */

/**
 * Validate file from known valid extensions
 *
 * @return boolean
 */
function isValidateFile(files, validExtensions) {
    var isValid = false;

    for (var i = 0; i < files.length; i++) {
        if ((files[i].type === "file" && files[i].name.length > 0) || !isValid) {
            for (var j = 0; j < validExtensions.length; j++) {
                isValid = files[i].name
                    .substr(files[i].name.length - validExtensions[j].length, validExtensions[j].length)
                    .toLowerCase() === validExtensions[j].toLowerCase();

                if (isValid) {
                    break;
                }
            }
        } else {
            break;
        }
    }

    return isValid;
}

/**
 * Create and append preview file to specific div
 *
 * @param files array of files, or object json from api
 * @param previewElement html element to append
 * @param isFile indicate this files is json or file
 */
function createPreviewFile(files, previewElement, isFile) {
    if ($(previewElement)) {
        $(previewElement).empty();

        for (var i = 0; i < files.length; i++) {
            var url = isFile ? URL.createObjectURL(files[i]) : files[i].path;

            if (files[0].type === "application/pdf") {
                $('<iframe>')
                    .attr({type: files[i].type, class: "doc-preview-file", src: url})
                    .appendTo($(previewElement));
            } else if (files[0].type.includes("image")) {
                $('<img alt="Image" width="300px">')
                    .attr({class: "img-preview-file img-responsive", src: url}).appendTo($(previewElement));
            }
        }
    }
}

$(document).ready(function () {
    /**
     * Enable scroll to focusing active menu
     */
    (function () {
        var scrollMenu = $(".slimscroll-menu");
        var activeMenu = $("li.active:not(:has(li.active a.active))");

        if (scrollMenu && activeMenu && activeMenu.offset()) {
            scrollMenu.animate({scrollTop: activeMenu.offset().top}, 500);
        }
    })();

    /**
     * Enable fullscreen function with add class enable-fullscreen to div
     */
    (function () {
        var root = $(".enable-fullscreen");

        if (root) {
            var anchor = $("<a>").css("margin-left", "auto").css("cursor", "pointer");
            var icon = $("<i>").css("color", "#797979");

            var toggle = function (event) {
                event && event.preventDefault();

                var isActive = !event || anchor.data("isActive");

                icon.removeClass(isActive ? "fa fa-arrows" : "fa fa-arrows-alt");
                icon.addClass(isActive ? "fa fa-arrows-alt" : "fa fa-arrows");

                anchor.data("isActive", !isActive);
                anchor.attr({title: (!isActive ? "Collapse" : "Expand") + " fullscreen"});

                if (event) {
                    [
                        {"display": "block"},
                        {"z-index": "9999"},
                        {"position": "fixed"},
                        {"width": "100%"},
                        {"height": "100%"},
                        {"top": 0},
                        {"right": 0},
                        {"left": 0},
                        {"bottom": 0},
                        {"overflow": "auto"}
                    ].forEach(function (style) {
                        var key = Object.keys(style)[0];

                        root.parent().css(key, isActive ? "" : style[key]);
                    });
                }
            };

            icon.appendTo(anchor);

            anchor.appendTo(root);
            anchor.on("click", toggle);

            root.css("display", "flex").css("align-items", "center");

            toggle();
        }
    })();

    $.ajaxSetup({
        statusCode: {
            401: function() {
                window.location.href = window.location.origin;
            }
        }
    });

});

