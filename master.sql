/*
 Navicat Premium Data Transfer

 Source Server         : localhost-mysql
 Source Server Type    : MySQL
 Source Server Version : 100417
 Source Host           : localhost:3306
 Source Schema         : master

 Target Server Type    : MySQL
 Target Server Version : 100417
 File Encoding         : 65001

 Date: 31/03/2021 11:33:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for appuser
-- ----------------------------
DROP TABLE IF EXISTS `appuser`;
CREATE TABLE `appuser`  (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `roleid` int(11) NOT NULL,
  `isactive` int(11) NOT NULL DEFAULT 1,
  `islogin` int(11) NOT NULL DEFAULT 0,
  `createby` int(11) NULL DEFAULT NULL,
  `createdate` datetime(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `updateby` int(11) NULL DEFAULT NULL,
  `updatedate` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `izin_session` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `unit_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`userid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of appuser
-- ----------------------------
INSERT INTO `appuser` VALUES (3, 'admin', 'gdyb21LQTcIANtvYMT7QVQ==', 'admin', 1, 1, 1, NULL, '2021-03-31 09:05:39.996854', NULL, '2021-03-15 10:49:42.000000', 'vplk122tpotl5aaatmjkvu6oki0ll5b4', 1);

SET FOREIGN_KEY_CHECKS = 1;
