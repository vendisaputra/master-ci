<?php

/**
 * Created by fajar at 1/22/20
 */
class Filter
{

    private $controller;

    private $defaultAuthenticatedRoute = "/dashboard";

    private $nonAuthenticatedRoutes = array(
        "auth" => array("index", "login"),
        "template" => array("index")
    );

    private $nonAuthenticatedMenus = array(
        "dashboard" => array("index")
    );

    public function __construct()
    {
        $this->controller =& get_instance();

        // manual load model if not loaded
        if (!$this->controller->load->is_loaded("M_auth")) {
            $this->controller->load->model("auth/M_auth");
        }

        // manual load model if not loaded
        if (!$this->controller->load->is_loaded("M_sidebar")) {
            $this->controller->load->model("template/M_sidebar");
        }
    }

    /**
     * Validate session to current user session in db
     * if not match throw redirect
     */
    public function validate()
    {
        $userId = $this->controller->session->userdata("tempId");

        if ($userId) {
            $izinSession = $this->controller->session->session_id;

            $user = $this->controller->M_auth->findBySession($izinSession);

            if (!$user) {
                $this->redirect(401, true);
            } else if ($this->permittedMenu()) {
                $this->redirect(403, true);
            } else if (!$this->permittedRoleMenu()) {
                $this->redirect(403, false);
            }
        } else {
            $this->redirect(401, false);
        }
    }

    /**
     * Redirect to / with flash when current request not permitted
     * or return json 401 if from ajax
     *
     * @param $status {string} http status code
     * @param $isRelogin {boolean} indicate this authorization has re login
     */
    public function redirect($status, $isRelogin)
    {
        if ($this->controller->input->is_ajax_request()) {
            header("Content-Type: application/json");

            http_response_code($status);

            exit();
        } else if ($status == 403) {
            $this->controller->session->set_flashdata("alert", "Anda tidak perlu login ulang !");

            redirect($this->defaultAuthenticatedRoute, "auto");
        } else if (!$this->permittedMenu()) {
            if ($isRelogin) {
                $this->controller->session->set_flashdata("alert", "Anda terdeteksi melakukan login ulang !");
            }

            redirect(base_url(), "auto");
        }
    }

    /**
     * Default route validation {Controller & Action}
     *
     * @param null $params to be indicate is controller when null
     * @return bool
     */
    private function permittedMenu($params = null)
    {
        $isController = $params == null;
        $params = $isController ? $this->nonAuthenticatedRoutes : $params;
        $req = $isController ? $this->controller->router->fetch_class() : $this->controller->router->fetch_method();

        $isPermitted = false;

        foreach ($params as $key => $value) {
            if ($isPermitted = $req == ($isController ? $key : $value)) {
                $isPermitted = $isController ? $this->permittedMenu($value) : $isPermitted;

                if ($isPermitted) {
                    break;
                }
            }
        }

        return $isPermitted;
    }

    /**
     * Validate menu of current user role
     *
     * @return bool
     */
    private function permittedRoleMenu()
    {
        if (!$this->controller->input->is_ajax_request()) {
            $controller = $this->controller->router->fetch_class();
            $action = $this->controller->router->fetch_method();

            foreach (array_merge($this->nonAuthenticatedRoutes, $this->nonAuthenticatedMenus) as $key => $value) {
                if ($controller == $key && array_search($action, $value) >= 0) {
                    return true;
                }
            }

            $module = $this->controller->router->fetch_module();

            $path = $module == $controller ? $module : ($module . "/" . $controller);

            $menus = array_filter($this->controller->M_sidebar->getmenu($this->controller->session->userdata('tempRole')),
                function ($menu) use ($path) {
                    return $menu->path == $path;
                });

            return count($menus) > 0;
        }

        return true;
    }

}