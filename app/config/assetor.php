<?php
/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 26/08/2018
 * Time: 22:10
 */

$config['version'] = '2.1';
$config['css_folder'] = 'assets/styles/';
$config['less_folder'] = '';
$config['lessphp_folder'] = 'third_party/lessphp/';
$config['css_min_folder'] = 'assets/styles/min/';
$config['gcc'] = FALSE;
$config['gcc_level'] = 'WHITESPACE_ONLY';
$config['js_folder'] = 'assets/scripts/';
$config['js_min_folder'] = 'assets/scripts/min/';
