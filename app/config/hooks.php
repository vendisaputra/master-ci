<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Modifier by fajar at 1/22/20
 */

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['display_override'][] = array(
    'class' => '',
    'function' => 'CI_Minifier_Hook_Loader',
    'filename' => '',
    'filepath' => ''
);

// Simple hook to filter controller
$hook['post_controller_constructor'] = array(
    'class'    => 'Filter',
    'function' => 'validate',
    'filename' => 'Filter.php',
    'filepath' => 'hooks',
    'params'   => array()
);