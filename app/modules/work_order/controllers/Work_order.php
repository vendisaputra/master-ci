<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/** Load each module base controller */
require_once(__DIR__ . DIRECTORY_SEPARATOR . "BaseController.php");

/**
 * Created by fajar at 1/29/20
 * Modified by vendi at 02/05/20
 */
class Work_order extends BaseController
{

    /**
     * This for menu perizinan => work_order/index/getdata (data table)
     * Route map as => work_order/getdata
     */

    public function index()
    {
        $this->loadview(strtolower(get_class($this)),
            array(
                "columnSearch" => $this->M_Work_order->column_search,
                "columnSearchMask" => $this->M_Work_order->column_search_mask
            ), "modal_work_order");
    }

    function add(){
        $this->loadview("work_order_form",
            array(
                "fcss" => $this->assetor->generate("form-style"),
                "pic" => $this->M_Work_order->getPic(),
                "jenisMinta" => $this->M_Work_order->getJenisPermintaan(),
                "kabupaten" => $this->M_Work_order->getKabupaten($this->session->userdata('tempUnitId')),
                "mitra" => $this->M_Work_order->getMitra(),
                "kawasan" => $this->M_Work_order->getkawasan($this->session->userdata('tempUnitId')),
                "serialWorkorder" => $this->generateSerialWorkorder()
            ),
            "modal_work_order");
    }

    function edit($id){
        $this->loadview("work_order_form",
            array(
                "fcss" => $this->assetor->generate("form-style"),
                "pic" => $this->M_Work_order->getPic(),
                "jenisMinta" => $this->M_Work_order->getJenisPermintaan(),
                "kabupaten" => $this->M_Work_order->getKabupaten($this->session->userdata('tempUnitId')),
                "kawasan" => $this->M_Work_order->getkawasan($this->session->userdata('tempUnitId')),
                "mitra" => $this->M_Work_order->getMitra(),
                "data" => $this->M_Work_order->getDataEdit($id)
            ),
            "modal_work_order");
    }

    function saveSku(){
        $path = "./assets/".date("Y").'/'.date("m")."/". $this->session->userdata('tempUnitId');
        if (!is_dir($path)) {
            mkdir($path, 0777, TRUE);
        }
        $ori = $_FILES['filesku']['name'];
        $new_name = $this->generateKey()."--".$ori;

        $config['upload_path']          = $path;
        $config['allowed_types']        = 'gif|jpg|png|jpeg|gif|pdf|docx|doc|xlsx|xls';
        $config['max_size']             = 10240;
        $config['file_name']            = $new_name;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('filesku')){
            $error = array('error' => $this->upload->display_errors());

            $output = array(
                "result" => 'false',
                "message" => $error

            );
        }else{
            $data = array('upload_data' => $this->upload->data());
            $cek = $this->M_Work_order->getWoSku($this->input->post('woId'));;

            if (isset($cek)){
                $datasku = array(
                    'no_sku' => $this->input->post('NoSku'),
                    'tgl_sku' => convertDateGaring($this->input->post('tgl')),
                    'tujuan' => $this->input->post('tujuan'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'nama' => $data['upload_data']['file_name'],
                    'file_type' => $data['upload_data']['file_type'],
                    'path' => substr($path, 2, strlen($path))
                );

                if ($this->M_Work_order->updateSku($datasku, $this->input->post('woId'))){

                    $data = array(
                        'status' => 3
                    );

                    $this->M_Work_order->updateworkorder($data, $this->input->post('woId'));

                    $output = array(
                        "result" => 'true',
                        "message" => 'Data workorder berhasil disimpan !'
                    );
                }else{
                    $output = array(
                        "result" => 'false',
                        "message" => 'Data workorder gagal disimpan !'
                    );
                }
            }else{
                $datasku = array(
                    'workorder_id' => $this->input->post('woId'),
                    'no_sku' => $this->input->post('NoSku'),
                    'tgl_sku' => convertDateGaring($this->input->post('tgl')),
                    'tujuan' => $this->input->post('tujuan'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'nama' => $data['upload_data']['file_name'],
                    'file_type' => $data['upload_data']['file_type'],
                    'path' => substr($path, 2, strlen($path)),
                    'created_date' => date('Y-m-d H:i:s'),
                    'is_active' => 1,
                    'created_by' => $this->session->userdata('tempId')
                );

                if ($this->M_Work_order->savesku($datasku) > 0){
                    $output = array(
                        "result" => 'true',
                        "message" => 'Data workorder berhasil disimpan !'
                    );

                    $data = array(
                        'status' => 3
                    );

                    $this->M_Work_order->updateworkorder($data, $this->input->post('woId'));

                }else{
                    $output = array(
                        "result" => 'false',
                        "message" => 'Data workorder gagal disimpan !'
                    );
                }
            }
        }
        echo json_encode($output);
    }

    function addsku($idWo)
    {
        $this->ci_minifier->enable_obfuscator();

        $this->load->model("template/M_sidebar");

        $this->load->view("template/header", array(
            "css" => $this->assetor->generate("core-style")
        ));

        $this->load->view("template/sidebar", array(
            "menu" => $this->M_sidebar->getmenu($this->session->userdata("tempRole"))
        ));

        $this->load->view("form_sku", array(
            "fcss" => $this->assetor->generate("form-style"),
            "data" => $this->M_Work_order->getWoSku($idWo)
        ));

        $this->load->view("template/footer", array(
            "js" => $this->assetor->generate("core-script")
        ));

        $this->load->view("js/js_" . strtolower(get_class($this))."_sku", array(
            "tjs" => $this->assetor->generate("data-tables-script"),
            "fjs" => $this->assetor->generate("form-script")
        ));
    }

    function getdata()
    {
        $list = $this->M_Work_order->get_datatables();
        $data  = array();
        $no = 0;
        foreach ($list as $field){
            $no++;
            $row = array();
            $row[] = $field->id;
            $row[] = $field->sales_no;
            $row[] = short_date_indo($field->tgl_sales);
            $row[] = $field->wo_no;
            $row[] = short_date_indo($field->tgl_wo);
            $row[] = $field->status;
            $row[] = $field->name;
            $row[] = $field->mitra;
            $row[] = $field->filesku ?? "";
            $row[] = $field->idstatus;
            $row[] = $field->id;
            $row[] = $field->created_by;
            $row[] = $field->created_id;
            $row[] = $this->session->userdata('tempId');

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_Work_order->count_all(),
            "recordsFiltered" => $this->M_Work_order->count_filtered(),
            "data" => $data,
            "token" => $this->security->get_csrf_hash()
        );
        echo json_encode($output);
    }

    function getDataEdit()
    {
        $list = $this->M_Work_order->getDataEdit($this->input->post('idWo'));

        $output = array(
            "result" => $list,
            "token" => $this->security->get_csrf_hash()
        );
        echo json_encode($output);
    }

    function save()
    {
        $output = "";
        $mitraId = $this->input->post('mitra');

        $picId = $this->input->post('nmPIC');

        $jml = null;

        if ($this->input->post('id') != null){
            $jml = $this->input->post('generateWo');
        }else{
            $jml = $this->M_Work_order->cekNoWo($this->generateSerialWorkorder());
        }


        $status = 1;
        if ($this->input->post('status') > 1) {
            $status = $this->input->post('status');
        }else{
            if ($mitraId != null && $picId != null) {
                $status = 2;
            } else {
                $status = 1;
            }
        }

        if ($jml == 0) {

            $data = array(
                'unit_id' => $this->session->userdata('tempUnitId'),
                'sales_no' => $this->input->post('NoSales'),
                'tgl_sales' => convertDateGaring($this->input->post('tglSO')),
                'wo_no' => $this->generateSerialWorkorder(),
                'tgl_wo' => convertDateGaring($this->input->post('tglPI')),
                'jenis_perizinan' => $this->input->post('jenis_permintaan'),
                'tgl_mulai' => convertDateGaring($this->input->post('tglPermintaan')),
                'tgl_selesai' => convertDateGaring($this->input->post('tglSelesai')),
                'nama_pelanggan' => $this->input->post('Nmplgn'),
                'kawasan' => $this->input->post('kawasan'),
                'kelurahan' => $this->input->post('kelurahan'),
                'kecamatan' => $this->input->post('kecamatan'),
                'kota' => $this->input->post('kabupaten'),
                'kodepos' => $this->input->post('kode_pos') == "" ? null : $this->input->post('kode_pos'),
                'alamat' => $this->input->post('alamat'),
                'longitude' => $this->input->post('Longitude'),
                'no_io' => $this->input->post('NoIO'),
                'latitude' => $this->input->post('Latitude'),
                'status' => $status,
                'is_active' => 1,
                'created_by' => $this->session->userdata('tempId'),
                'created_date' => date('Y-m-d H:i:s')
            );

            if ($this->M_Work_order->saveworkorder($data) > 0) {

                $cekWo = $this->M_Work_order->cekWorkorder($data);
                $dataWoPenugasan = array();

                if ($mitraId != null && $picId != null) {
                    $dataWoPenugasan = array(
                        'workorder_id' => $cekWo->id,
                        'mitra_id' => $mitraId,
                        'active' => 1,
                        'created_by' => $this->session->userdata('tempId'),
                        'created_date' => date('Y-m-d H:i:s'),
                        'pic_id' => $picId
                    );

                    if ($this->M_Work_order->saveWorkorderPenugasan($dataWoPenugasan) > 0){
                        $output = array(
                            "result" => 'true',
                            "message" => 'Data berhasil disimpan !'
                        );
                    }else{
                        $output = array(
                            "result" => 'false',
                            "message" => 'Data gagal disimpan !'
                        );
                    }
                }else{
                    $dataWoPenugasan = array(
                        'workorder_id' => $cekWo->id,
                        'active' => 1,
                        'created_by' => $this->session->userdata('tempId'),
                        'created_date' => date('Y-m-d H:i:s')
                    );

                    if ($this->M_Work_order->saveWorkorderPenugasan($dataWoPenugasan) > 0) {
                        $output = array(
                            "result" => 'true',
                            "message" => 'Data berhasil disimpan !'
                        );
                    } else {
                        $output = array(
                            "result" => 'false',
                            "message" => 'Data gagal disimpan !'
                        );
                    }

                }

            } else {
                $output = array(
                    "result" => 'false',
                    "message" => 'Data gagal disimpan !'
                );
            }
        } else {
            if ($this->input->post('id') != "" || $this->input->post('id')!=null) {
                $data = array(
                    'unit_id' => $this->session->userdata('tempUnitId'),
                    'sales_no' => $this->input->post('NoSales'),
                    'tgl_sales' => convertDateGaring($this->input->post('tglSO')),
                    'tgl_wo' => convertDateGaring($this->input->post('tglPI')),
                    'jenis_perizinan' => $this->input->post('jenis_permintaan'),
                    'tgl_mulai' => convertDateGaring($this->input->post('tglPermintaan')),
                    'tgl_selesai' => convertDateGaring($this->input->post('tglSelesai')),
                    'nama_pelanggan' => $this->input->post('Nmplgn'),
                    'kawasan' => $this->input->post('kawasan'),
                    'kelurahan' => $this->input->post('kelurahan'),
                    'kecamatan' => $this->input->post('kecamatan'),
                    'kota' => $this->input->post('kabupaten'),
                    'kodepos' => $this->input->post('kode_pos') == "" ? null : $this->input->post('kode_pos'),
                    'alamat' => $this->input->post('alamat'),
                    'longitude' => $this->input->post('Longitude'),
                    'no_io' => $this->input->post('NoIO'),
                    'status' => $status,
                    'latitude' => $this->input->post('Latitude')
                );

                if ($this->M_Work_order->updateworkorder($data, $this->input->post('id')) > 0) {
                    if ($this->M_Work_order->cekPenugasan($this->input->post('id')) > 0) {
                        if ($mitraId != null && $picId != null) {
                            $dataWoPenugasan = array(
                                'mitra_id' => $mitraId,
                                'pic_id' => $picId
                            );

                            if ($this->M_Work_order->updateWorkorderPenugasan($dataWoPenugasan, $this->input->post('id')) > 0) {
                                $output = array(
                                    "result" => 'true',
                                    "message" => 'Data berhasil disimpan !'
                                );
                            } else {
                                $output = array(
                                    "result" => 'false',
                                    "message" => 'Data gagal disimpan !'
                                );
                            }
                        }else{
                            if ($mitraId != null){
                                $dataWoPenugasan = array(
                                    'mitra_id' => $mitraId
                                );

                                if ($this->M_Work_order->updateWorkorderPenugasan($dataWoPenugasan, $this->input->post('id')) > 0) {
                                    $output = array(
                                        "result" => 'true',
                                        "message" => 'Data berhasil disimpan !'
                                    );
                                } else {
                                    $output = array(
                                        "result" => 'false',
                                        "message" => 'Data gagal disimpan !'
                                    );
                                }
                            } elseif ($picId != null){
                                $dataWoPenugasan = array(
                                    'pic_id' => $picId
                                );

                                if ($this->M_Work_order->updateWorkorderPenugasan($dataWoPenugasan, $this->input->post('id')) > 0) {
                                    $output = array(
                                        "result" => 'true',
                                        "message" => 'Data berhasil disimpan !'
                                    );
                                } else {
                                    $output = array(
                                        "result" => 'false',
                                        "message" => 'Data gagal disimpan !'
                                    );
                                }
                            }
                        }
                    }
                    $output = array(
                        "result" => 'true',
                        "message" => 'Data workorder berhasil disimpan !'
                    );
                } else {
                    $output = array(
                        "result" => 'false',
                        "message" => 'Data workorder gagal disimpan !'
                    );
                }
            }else{
                $output = array(
                    "result" => 'false',
                    "jml" => $jml,
                    "message" => 'Data workorder gagal disimpan !'
                );
            }

        }

        echo json_encode($output);
    }

    function getKecamatan(){
        $exe = $this->M_Work_order->getKecamatan($this->input->post('kabupatenId'));
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getKelurahan(){
        $exe = $this->M_Work_order->getKelurahan($this->input->post('kecamatanId'));
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getMitra(){
        $exe = $this->M_Work_order->getMitra();
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getKawasan(){
        $exe = $this->M_Work_order->getLocationKawasan($this->input->post('id'));
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getSku(){
        $exe = $this->M_Work_order->getWoSku($this->input->post('idWo'));
        $output = array(
            "file" => array(
                "type" => $exe->file_type,
                "path" => base_url().$exe->path."/".$exe->nama
            )
        );
        echo json_encode($output);
    }


}
