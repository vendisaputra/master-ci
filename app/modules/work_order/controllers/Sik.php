<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/** Load each module base controller */
require_once(__DIR__ . DIRECTORY_SEPARATOR . "BaseController.php");

/**
 * Created by vendi 3/4/20
 */

class Sik extends BaseController
{
    public function index()
    {
        $this->loadview(strtolower(get_class($this)),
            array(
                "columnSearch" => $this->M_Sik->column_search,
                "columnSearchMask" => $this->M_Sik->column_search_mask
            ), "modal_work_order");
    }

    function getdata()
    {
        $list = $this->M_Sik->get_datatables();
        $data  = array();
        $no = 0;
        foreach ($list as $field){
            if ($field->filesku != null) {
                $oroginNameFile = explode("--", $field->filesku);
                $file = $oroginNameFile[1];
            }else{
                $file = "";
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->sales_no;
            $row[] = short_date_indo($field->tgl_sales);
            $row[] = $field->wo_no;
            $row[] = short_date_indo($field->tgl_wo);
            $row[] = $field->status;
            $row[] = $field->name;
            $row[] = $field->mitra;
            $row[] = $file;
            $row[] = $field->idstatus;
            $row[] = $field->id;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_Sik->count_all(),
            "recordsFiltered" => $this->M_Sik->count_filtered(),
            "data" => $data
        );
        echo json_encode($output);
    }

    function upload_sik($id){
        $this->loadview("sik_form", array(), "modal_work_order");
    }

    function getwo(){
        $output = array(
            "data" => $this->M_Sik->getWo($this->input->post('idWo'))
        );
        echo json_encode($output);
    }

    function getSik()
    {
        $list = $this->M_Sik->getDataSik($this->input->post('idWo'));

        $output = array(
            "result" => $list
        );
        echo json_encode($output);
    }

    function save()
    {
        $output = null;
        $path = "./assets/sik/" . date("Y") . '/' . date("m") . "/" . $this->session->userdata('tempUnitId');
        if (!is_dir($path)) {
            $mask = umask(0);
            mkdir($path, 0777, TRUE);
            umask($mask);
        }
        $ori = $_FILES['filesik']['name'];
        $new_name = $this->generateKey() . "--" . $ori;

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|gif|pdf|docx|doc|xlsx|xls';
        $config['max_size'] = 10240;
        $config['file_name'] = $new_name;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('filesik')) {
            $error = array('error' => $this->upload->display_errors());

            $output = array(
                "result" => 'false',
                "message" => $error

            );
        } else {
            $data = array('upload_data' => $this->upload->data());

            $dataSik = array(
                "workorder_id" => $this->input->post("id"),
                "no_sik" => $this->input->post("NoSKI"),
                "tgl_sik" => convertDateGaring($this->input->post("tglSKI")),
                "tgl_mulai" => convertDateGaring($this->input->post("tglMulai")),
                "tgl_selesai" => convertDateGaring($this->input->post("tglSelesaiSIK")),
                "deskripsi" => $this->input->post("deskripsi"),
                "out_by" => $this->input->post("dikeluarkanOleh"),
                "nama" => $data['upload_data']['file_name'],
                "path_sik" => substr($path, 2, strlen($path)),
                "file_type" => $data['upload_data']['file_type'],
                "is_active" => 1,
                "created_by" => $this->session->userdata('tempId'),
                "created_date" => date("Y-m-d H:i:s")
            );

            $dataWo = array(
                "status" => 9
            );

            if ($this->M_Sik->updateWorkorder($dataWo, $this->input->post("id")) > 0) {
                if ($this->M_Sik->saveSik($dataSik) > 0) {
                    $output = array(
                        "result" => 'true',
                        "message" => "Data berhasil disimpan!"
                    );
                } else {
                    $output = array(
                        "result" => 'false',
                        "message" => "Data gagal disimpan!"
                    );
                }
            }
        }
        echo json_encode($output);
    }
}