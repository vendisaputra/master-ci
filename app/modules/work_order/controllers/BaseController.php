<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by fajar at 1/28/20
 * Modified by vendi at 2/4/20
 */
class BaseController extends MX_Controller
{

    /**
     * Load common model & set default TZ
     */
    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set("Asia/Jakarta");

        $this->load->model("M_Work_order");

        $this->load->model("M_Pindah_pic");

        $this->load->model("M_Approval");

        $this->load->model("M_Sik");

        $this->load->model("M_Update_Progress");

        $this->load->model("M_Tgl_sewa");

        $this->load->helper("convertdate_helper");

        $this->load->helper("convert_number_helper");
    }

    /**
     * Common render view template
     *
     * @param $path string path file
     * @param $param array param to merge with view and js
     * @param $modal array param to merge with view and js
     */

    public function loadview($path, $param = array(), $modal = null){
        $this->ci_minifier->enable_obfuscator();

        $this->load->model("template/M_sidebar");

        $this->load->view("template/header", array(
            "css" => $this->assetor->generate("core-style")
        ));

        $this->load->view("template/sidebar", array(
            "menu" => $this->M_sidebar->getmenu($this->session->userdata("tempRole"))
        ));

        $this->load->view($path, array_merge(array(
            "fcss" => $this->assetor->generate("form-style")
        ), $param));

        if (isset($modal)){
            $this->load->view($modal);
        }

        $this->load->view("template/footer", array(
            "js" => $this->assetor->generate("core-script")
        ));

        $this->load->view("js/js_" . $path, array_merge(array(
            "tjs" => $this->assetor->generate("data-tables-script"),
            "fjs" => $this->assetor->generate("form-script")
        ), $param));
    }

    function generateKey()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $nkey = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($s = 0; $s < 4; $s++){
            $key = array();
            for ($i = 0; $i < 6; $i++) {
                $n = rand(0, $alphaLength);
                $key[] = $alphabet[$n];
            }
            $nkey[$s] = implode($key);
        }
        return join("-",$nkey);
    }

    public function generateSerialWorkorder(){
        $number = str_pad($this->M_Work_order->getCountWorkorder()+1, 3, '0', STR_PAD_LEFT);
        $unit = $this->M_Work_order->getMasterUnit($this->session->userdata('tempUnitId'))->kode;
        $date = date('m/Y');

        return $number."/".$unit."/".$date;
    }

}
