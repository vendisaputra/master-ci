<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/** Load each module base controller */
require_once(__DIR__ . DIRECTORY_SEPARATOR . "BaseController.php");

/**
 * Created by fajar at 1/29/20
 * Modified by vendi at 2/4/20
 */
class Pindah_pic extends BaseController{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("convertdate_helper");
    }

    function index(){
        $this->loadview(strtolower(get_class($this)),
            array(
                "columnSearch" => $this->M_Pindah_pic->column_search,
                "columnSearchMask" => $this->M_Pindah_pic->column_search_mask
            ));
    }

    function edit($id){
        $this->ci_minifier->enable_obfuscator();

        $this->load->model("template/M_sidebar");

        $this->load->view("template/header", array(
            "css" => $this->assetor->generate("core-style")
        ));

        $this->load->view("template/sidebar", array(
            "menu" => $this->M_sidebar->getmenu($this->session->userdata("tempRole"))
        ));

        $this->load->view("form_pindah_pic", array(
            "fcss" => $this->assetor->generate("form-style"),
            "pic" => $this->M_Pindah_pic->getPic($this->session->userdata('tempUnitId')),
            "jenisMinta" => $this->M_Pindah_pic->getJenisPermintaan(),
            "kabupaten" => $this->M_Pindah_pic->getKabupaten($this->session->userdata('tempUnitId')),
            "kawasan" => $this->M_Pindah_pic->getkawasan($this->session->userdata('tempUnitId')),
            "serialWorkorder" => $this->generateSerialWorkorder()
        ));

        $this->load->view("modal_pindah_pic");

        $this->load->view("template/footer", array(
            "js" => $this->assetor->generate("core-script")
        ));

        $this->load->view("js/js_" . strtolower(get_class($this))."_form", array(
            "tjs" => $this->assetor->generate("data-tables-script")
        ));
    }

    function getdata()
    {
        $list = $this->M_Pindah_pic->get_datatables();
        $data = array();
        $no = 0;
        foreach ($list as $field) {
            if ($field->filesku != null) {
                $oroginNameFile = explode("--", $field->filesku);
                $file = $oroginNameFile[1];
            } else {
                $file = "";
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->sales_no;
            $row[] = short_date_indo($field->tgl_sales);
            $row[] = $field->wo_no;
            $row[] = short_date_indo($field->tgl_wo);
            $row[] = $field->status;
            $row[] = $field->name;
            $row[] = $field->mitra;
            $row[] = $file;
            $row[] = $field->idstatus;
            $row[] = $field->id;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_Pindah_pic->count_all(),
            "recordsFiltered" => $this->M_Pindah_pic->count_filtered(),
            "data" => $data,
            "token" => $this->security->get_csrf_hash()
        );
        echo json_encode($output);
    }

    function getDataEdit()
    {
        $list = $this->M_Pindah_pic->getDataEdit($this->input->post('idWo'));

        $output = array(
            "result" => $list
        );
        echo json_encode($output);
    }

    function getKecamatan(){
        $exe = $this->M_Pindah_pic->getKecamatan($this->input->post('kabupatenId'));
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getKelurahan(){
        $exe = $this->M_Pindah_pic->getKelurahan($this->input->post('kecamatanId'));
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getMitra(){
        $exe = $this->M_Pindah_pic->getMitra($this->input->post('picId'));
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getKawasan(){
        $exe = $this->M_Work_order->getLocationKawasan($this->input->post('id'));
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function save(){
        $output = "";

        if ($this->M_Pindah_pic->cekPenugasan($this->input->post('id')) > 0){
            $dataWoPenugasan = array(
                'active' => 0
            );

            $this->M_Pindah_pic->updateWorkorderPenugasan($dataWoPenugasan, $this->input->post('id'));

            $dataWoPenugasan = array(
                'workorder_id' => $this->input->post('id'),
                'mitra_id' => $this->input->post('mitraId'),
                'active' => 1,
                'created_by' => $this->session->userdata('tempId'),
                'created_date' => date('Y-m-d H:i:s'),
                'pic_id' => $this->input->post('nmPIC')
            );
            $this->M_Pindah_pic->saveWorkorderPenugasan($dataWoPenugasan);
        }
        $output = array(
            "result" => 'true',
            "message" => 'Data user berhasil disimpan !'
        );


        echo json_encode($output);
    }

}
