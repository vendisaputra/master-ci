<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/** Load each module base controller */
require_once(__DIR__ . DIRECTORY_SEPARATOR . "BaseController.php");

/**
 * Created by fajar at 1/29/20
 * Modified by vendi at  02/13/20
 */
class Update_progress extends BaseController
{

    /**
     * This for menu perizinan => update_progress/getdata (data table)
     * Route map as => work_order/update_progress/getdata
     */
    public function index()
    {
        $this->loadview(strtolower(get_class($this)),
            array(
                "columnSearch" => $this->M_Update_Progress->column_search,
                "columnSearchMask" => $this->M_Update_Progress->column_search_mask
            ));
    }

    function getdata()
    {
        $list = $this->M_Update_Progress->get_datatables();
        $data  = array();
        $no = 0;
        foreach ($list as $field){
            if ($field->filesku != null) {
                $oroginNameFile = explode("--", $field->filesku);
                $file = $oroginNameFile[1];
            }else{
                $file = "";
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->sales_no;
            $row[] = short_date_indo($field->tgl_sales);
            $row[] = $field->wo_no;
            $row[] = $field->kode;
            $row[] = $field->status;
            $row[] = $field->progress;
            $row[] = $field->kendala;
            $row[] = $file;
            $row[] = $field->idstatus;
            $row[] = $field->id;
            $row[] = $field->pic_id;
            $row[] = $field->createby;
            $row[] = $this->session->userdata('tempId');

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_Update_Progress->count_all(),
            "recordsFiltered" => $this->M_Update_Progress->count_filtered(),
            "data" => $data,
            "token" => $this->security->get_csrf_hash()
        );
        echo json_encode($output);
    }

    function addProgress($id){
        $this->loadview("form_update_progress",
            array(
                "data" => $this->M_Update_Progress->getWo($id)
            ));
    }

    function saveProgress(){
        $path = "./assets/evidence/".date("Y").'/'.date("m")."/". $this->session->userdata('tempUnitId');
        if (!is_dir($path)) {
            mkdir($path, 0777, TRUE);
        }
        $ori = $_FILES['evidence']['name'];
        $new_name = $this->generateKey()."--".$ori;

        $config['upload_path']          = $path;
        $config['allowed_types']        = 'gif|jpg|png|jpeg|gif|pdf';
        $config['max_size']             = 10240;
        $config['file_name']            = $new_name;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('evidence')){
            $error = array('error' => $this->upload->display_errors());

            $output = array(
                "result" => 'false',
                "message" => $error

            );
        }else{
            $data = array('upload_data' => $this->upload->data());

            $dataProgress = array(
                "workorder_id" => $this->input->post("woId"),
                "progress" => $this->input->post("progress"),
                "tgl_progress" => convertDateGaring($this->input->post('tgl')),
                "deskripsi" => $this->input->post("deskripsi"),
                "kendala" => $this->input->post("kendala"),
                "evidence" => $data['upload_data']['file_name'],
                'file_type' => $data['upload_data']['file_type'],
                'path' => substr($path, 2, strlen($path)),
                "created_by" => $this->session->userdata('tempId'),
                "created_date" => date('Y-m-d H:i:s')
            );

            if ($this->M_Update_Progress->saveProgress($dataProgress) > 0){
                $output = array(
                    "result" => 'true',
                    "message" => 'Data berhasil disimpan !'
                );
            }else{
                $output = array(
                    "result" => 'false',
                    "message" => 'Data gagal disimpan !'
                );
            }


        }
        echo json_encode($output);
    }

    function detailProgress($id){
        $this->ci_minifier->enable_obfuscator();

        $this->load->model("template/M_sidebar");

        $this->load->view("template/header", array(
            "css" => $this->assetor->generate("core-style")
        ));

        $this->load->view("template/sidebar", array(
            "menu" => $this->M_sidebar->getmenu($this->session->userdata("tempRole"))
        ));

        $this->load->view("detail_update_progress", array(
            "fcss" => $this->assetor->generate("form-style")
        ));

        $this->load->view("template/footer", array(
            "js" => $this->assetor->generate("core-script")
        ));

        $this->load->view("js/js_detail_update_progress", array(
            "tjs" => $this->assetor->generate("data-tables-script"),
            "fjs" => $this->assetor->generate("form-script")
        ));
    }

    function getDetailUpdateProgress(){
        $list = $this->M_Update_Progress->get_datatables_detail($this->input->post('id'));
        $data  = array();
        $no = 0;
        foreach ($list as $field){
            if ($field->evidence != null) {
                $oroginNameFile = explode("--", $field->evidence);
                $file = $oroginNameFile[1];
            }else{
                $file = "";
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->wo_no;
            $row[] = short_date_indo($field->tgl_progress);
            $row[] = $field->progress;
            $row[] = $field->deskripsi;
            $row[] = $field->kendala;
            $row[] = $file;
            $row[] = $field->id;
            $row[] = base_url().$field->path."/".$field->evidence;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_Update_Progress->count_all_detail($this->input->post('id')),
            "recordsFiltered" => $this->M_Update_Progress->count_filtered_detail($this->input->post('id')),
            "data" => $data,
            "token" => $this->security->get_csrf_hash()
        );
        echo json_encode($output);
    }

    function getProgress(){
       $result = $this->M_Update_Progress->getWoProgress($this->input->post("id"));

        $output = array(
            "file" => array(
                "type" => $result->file_type,
                "path" => base_url().$result->path."/".$result->evidence
            )
        );
        echo json_encode($output);
    }



}
