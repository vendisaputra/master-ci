<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/** Load each module base controller */
require_once(__DIR__ . DIRECTORY_SEPARATOR . "BaseController.php");

/**
 * Created by fajar at 1/29/20
 * Created by vendi at 2/17/20
 */
class Approval extends BaseController
{

    /**
     * This for menu approval => work_order/getdata (data table)
     * Route map as => work_order/approval/getdata
     */

    public function index()
    {
        $this->loadview(strtolower(get_class($this)),
            array(
                "columnSearch" => $this->M_Approval->column_search,
                "columnSearchMask" => $this->M_Approval->column_search_mask
            ), "modal_work_order");
    }

    function closing($id){
        $this->ci_minifier->enable_obfuscator();

        $biayaDeposit = isset($id) ? $this->M_Approval->getBiayaDeposit($id) : null;

        $this->load->model("template/M_sidebar");

        $this->load->view("template/header", array(
            "css" => $this->assetor->generate("core-style")
        ));

        $this->load->view("template/sidebar", array(
            "menu" => $this->M_sidebar->getmenu($this->session->userdata("tempRole"))
        ));

        $this->load->view("form_approval", array(
            "fcss" => $this->assetor->generate("form-style"),
            "pic" => $this->M_Approval->getPic(),
            "jenisMinta" => $this->M_Approval->getJenisPermintaan(),
            "kabupaten" => $this->M_Approval->getKabupaten($this->session->userdata('tempUnitId')),
            "masterBiaya" => $this->M_Approval->getMasterBiaya(),
            "biayaDeposit" => $biayaDeposit,
            "mitra" => $this->M_Approval->getMitra(),
            "historyDate" => $this->M_Approval->getHistoryDate($id),
            "kawasan" => $this->M_Approval->getkawasan($this->session->userdata('tempUnitId')),
            "serialWorkorder" => $this->generateSerialWorkorder()
        ));

        $this->load->view("modal_work_order");
        $this->load->view("template/footer", array(
            "js" => $this->assetor->generate("core-script")
        ));

        $this->load->view("js/js_form_approval", array(
            "tjs" => $this->assetor->generate("data-tables-script"),
            "fjs" => $this->assetor->generate("form-script")
        ));
    }

    function getHistoryDate($id){
        $output = array(
            "data" => $this->M_Approval->getHistoryDate($id)
        );
        echo json_encode($output);
    }

    function getdata()
    {
        $list = $this->M_Approval->get_datatables();
        $data  = array();
        $no = 0;
        foreach ($list as $field){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->sales_no;
            $row[] = short_date_indo($field->tgl_sales);
            $row[] = $field->wo_no;
            $row[] = short_date_indo($field->tgl_wo);
            $row[] = $field->jenis_perizinan;
            $row[] = $field->name;
            $row[] = short_date_indo($field->tgl_mulai);
            $row[] = short_date_indo($field->tgl_selesai);
            $row[] = $field->status;
            $row[] = $field->biaya_deposit != null ? currency($field->biaya_deposit) : 0;
            $row[] = $field->idstatus;
            $row[] = $field->id;
            $row[] = currency($field->biaya_izin);
            $row[] = short_date_indo($field->detail_tgl_mulai);
            $row[] = short_date_indo($field->detail_tgl_selesai);

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_Approval->count_all(),
            "recordsFiltered" => $this->M_Approval->count_filtered(),
            "data" => $data,
            "token" => $this->security->get_csrf_hash()
        );
        echo json_encode($output);
    }

    function getDataEditWorkorder()
    {
        $list = $this->M_Approval->getDataEditWorkorder($this->input->post('idWo'));

        $output = array(
            "result" => $list,
            "token" => $this->security->get_csrf_hash()
        );
        echo json_encode($output);
    }

    function getSik()
    {
        $list = $this->M_Approval->getDataSik($this->input->post('idWo'));

        $output = array(
            "result" => $list,
            "token" => $this->security->get_csrf_hash()
        );
        echo json_encode($output);
    }

    function getKecamatan(){
        $exe = $this->M_Approval->getKecamatan($this->input->post('kabupatenId'));
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getKelurahan(){
        $exe = $this->M_Approval->getKelurahan($this->input->post('kecamatanId'));
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getMitra(){
        $exe = $this->M_Approval->getMitra();
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getKawasan(){
        $exe = $this->M_Approval->getLocationKawasan($this->input->post('id'));
        $output = array(
            "result" => $exe
        );
        echo json_encode($output);
    }

    function getSku(){
        $exe = $this->M_Approval->getWoSku($this->input->post('idWo'));
        $output = array(
            "file" => array(
                "type" => $exe->file_type,
                "path" => base_url().$exe->path."/".$exe->nama
            )
        );
        echo json_encode($output);
    }

    function saveClosing(){
        $output = "";
        $path = "./assets/sik/".date("Y").'/'.date("m")."/". $this->session->userdata('tempUnitId');
         if (!is_dir($path)) {
            $mask=umask(0);
            mkdir($path, 0777, TRUE);
            umask($mask);
         }

        if(isset($_FILES['filesik']['name'])){
            if ($_FILES['filesik']['name'] != ""){
                $ori = $_FILES['filesik']['name'];
                $new_name = $this->generateKey()."--".$ori;

                $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png|jpeg|gif|pdf|docx|doc|xlsx|xls';
                $config['max_size']             = 10240;
                $config['file_name']            = $new_name;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('filesik')){
                    $error = array('error' => $this->upload->display_errors());

                    $output = array(
                        "result" => 'false',
                        "message" => $error

                    );
                }else{
                    $data = array('upload_data' => $this->upload->data());

                    $status = 10;
                    if ($this->input->post("persetujuan") != 0) {
                        $status = 10;
                    }else{
                        $status = 11;
                    }

                    $dataApproval = array(
                        "workorder_id" => $this->input->post("id"),
                        "created_by" => $this->session->userdata('tempId'),
                        "created_date" => date("Y-m-d H:i:s")
                    );


                    if ($this->M_Approval->saveApproval($dataApproval) > 0) {
                        $cekApproval = $this->M_Approval->cekApproval($dataApproval);

                        $dataWo = array(
                            "status" => $status,
                            "workorder_approval_id" => $cekApproval->id
                        );

                        if ($this->M_Approval->updateWorkorder($dataWo, $this->input->post("id")) > 0) {
                            $dataSik = array(
                                "workorder_id" => $this->input->post("id"),
                                "no_sik" => $this->input->post("NoSKI"),
                                "tgl_sik" => convertDateGaring($this->input->post("tglSKI")),
                                "tgl_mulai" => convertDateGaring($this->input->post("tglMulai")),
                                "tgl_selesai" => convertDateGaring($this->input->post("tglSelesaiSIK")),
                                "deskripsi" => $this->input->post("deskripsi"),
                                "out_by" => $this->input->post("dikeluarkanOleh"),
                                "nama" => $data['upload_data']['file_name'],
                                "path_sik" => substr($path, 2, strlen($path)),
                                "file_type" => $data['upload_data']['file_type'],
                                "is_active" => 1,
                                "created_by" => $this->session->userdata('tempId'),
                                "created_date" => date("Y-m-d H:i:s")
                            );
                            if ($this->M_Approval->saveSik($dataSik) > 0) {
                                $output = array(
                                    "result" => 'true',
                                    "message" => "Data berhasil disimpan!",
                                    "status" => $status,
                                    "path" => $path
                                );
                            } else {
                                $output = array(
                                    "result" => 'false',
                                    "message" => "Data gagal disimpan!",
                                    "status" => $status,
                                    "path" => $path
                                );
                            }
                        }
                    }
                }
            }else{
              if ($this->input->post('deskripsi') != "" || $this->input->post('NoSKI') != "" || $this->input->post('dikeluarkanOleh') != "" ){
                  $status = 10;
                  if ($this->input->post("persetujuan") != 0) {
                      $status = 10;
                  }else{
                      $status = 11;
                  }

                  $dataApproval = array(
                      "workorder_id" => $this->input->post("id"),
                      "created_by" => $this->session->userdata('tempId'),
                      "created_date" => date("Y-m-d H:i:s")
                  );


                  if ($this->M_Approval->saveApproval($dataApproval) > 0) {
                      $cekApproval = $this->M_Approval->cekApproval($dataApproval);

                      $dataWo = array(
                          "status" => $status,
                          "workorder_approval_id" => $cekApproval->id
                      );

                      if ($this->M_Approval->updateWorkorder($dataWo, $this->input->post("id")) > 0) {
                          $dataSik = array(
                              "workorder_id" => $this->input->post("id"),
                              "no_sik" => $this->input->post("NoSKI"),
                              "tgl_sik" => convertDateGaring($this->input->post("tglSKI")),
                              "tgl_mulai" => convertDateGaring($this->input->post("tglMulai")),
                              "tgl_selesai" => convertDateGaring($this->input->post("tglSelesaiSIK")),
                              "deskripsi" => $this->input->post("deskripsi"),
                              "out_by" => $this->input->post("dikeluarkanOleh"),
                              "nama" => null,
                              "path_sik" => null,
                              "file_type" => null,
                              "is_active" => 1,
                              "created_by" => $this->session->userdata('tempId'),
                              "created_date" => date("Y-m-d H:i:s")
                          );
                          if ($this->M_Approval->saveSik($dataSik) > 0) {
                              $output = array(
                                  "result" => 'true',
                                  "message" => "Data berhasil disimpan!"
                              );
                          } else {
                              $output = array(
                                  "result" => 'false',
                                  "message" => "Data gagal disimpan!"
                              );
                          }
                      }
                  }
              }else{

                  $status = 10;
                  if ($this->input->post("persetujuan") != 0) {
                      $status = 10;
                  }else{
                      $status = 11;
                  }

                  $dataApproval = array(
                      "workorder_id" => $this->input->post("id"),
                      "created_by" => $this->session->userdata('tempId'),
                      "created_date" => date("Y-m-d H:i:s")
                  );


                  if ($this->M_Approval->saveApproval($dataApproval) > 0) {
                      $cekApproval = $this->M_Approval->cekApproval($dataApproval);

                      $dataWo = array(
                          "status" => $status,
                          "workorder_approval_id" => $cekApproval->id
                      );

                      if ($this->M_Approval->updateWorkorder($dataWo, $this->input->post("id")) > 0) {
                          $output = array(
                              "result" => 'true',
                              "message" => "Data berhasil disimpan!"
                          );
                      } else {
                          $output = array(
                              "result" => 'false',
                              "message" => "Data gagal disimpan!"
                          );
                      }
                  }
              }
            }
        }else{
             $status = 10;
            if ($this->input->post("persetujuan") != 0) {
                $status = 10;
            }else{
                $status = 11;
            }

            $dataApproval = array(
                "workorder_id" => $this->input->post("id"),
                "created_by" => $this->session->userdata('tempId'),
                "created_date" => date("Y-m-d H:i:s")
            );


            if ($this->M_Approval->saveApproval($dataApproval) > 0) {
                $cekApproval = $this->M_Approval->cekApproval($dataApproval);

                $dataWo = array(
                    "status" => $status,
                    "workorder_approval_id" => $cekApproval->id
                );

                if ($this->M_Approval->updateWorkorder($dataWo, $this->input->post("id")) > 0) {
                    $output = array(
                        "result" => 'true',
                        "message" => "Data berhasil disimpan!",
                        "status" => $status,
                        "path" => $path
                        );
                } else {
                    $output = array(
                        "result" => 'false',
                        "message" => "Data gagal disimpan!",
                        "status" => $status,
                        "path" => $path
                    );
                }
            }
        }
        echo json_encode($output);
    }

    function getFileDeposit(){
        $exe = $this->M_Approval->getFileDeposit($this->input->post('idWo'));
        $output = array(
            "file" => array(
                "type" => $exe->file_type,
                "path" => base_url().$exe->path."/".$exe->bukti_tf
            )
        );
        echo json_encode($output);
    }

}
