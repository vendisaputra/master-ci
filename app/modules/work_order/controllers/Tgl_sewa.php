<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/** Load each module base controller */
require_once(__DIR__ . DIRECTORY_SEPARATOR . "BaseController.php");

/**
 * Created by vendi at 4/7/20
 */
class Tgl_sewa extends BaseController
{
    public function index()
    {
        $this->loadview(strtolower(get_class($this)), array(
            "columnSearch" => $this->M_Tgl_sewa->column_search,
            "columnSearchMask" => $this->M_Tgl_sewa->column_search_mask
        ));
    }

    function edit($id)
    {
        $this->loadview(strtolower("form_tgl_sewa"), array(
            "masterBiaya" => $this->M_Tgl_sewa->getMasterBiaya(),
            "data" => $this->M_Tgl_sewa->getBiaya($id)
        ));
    }

    function getdata()
    {
        $session_id = $this->session->userdata('tempId');
        if ($session_id){
            $list = $this->M_Tgl_sewa->get_datatables();
            $data  = array();
            $no = 1;
            foreach ($list as $field){
                $no++;
                $row = array();
                $row[] = $field->id;
                $row[] = $field->sales_no;
                $row[] = short_date_indo($field->tgl_sales);
                $row[] = $field->wo_no;
                $row[] = short_date_indo($field->tgl_wo);
                $row[] = $field->kode;
                $row[] = $field->name;
                $row[] = $field->status;
                $row[] = $field->approval;
                $row[] = $field->txt_status;
                $row[] = currency($field->biaya_izin);
                $row[] = "edit";
                $row[] =  date_indo($field->detail_tgl_mulai)." - ".date_indo($field->detail_tgl_selesai);



                $data[] = $row;
            }

            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->M_Tgl_sewa->count_all(),
                "recordsFiltered" => $this->M_Tgl_sewa->count_filtered(),
                "data" => $data,
                "token" => $this->security->get_csrf_hash()
            );
            echo json_encode($output);
        }
    }

    function createBiayaIzin()
    {
        $cek = $this->M_Tgl_sewa->cekBiayaIzin($this->input->post('idWo'));
        if ($cek == 0) {
            $data = array(
                'workorder_id' => $this->input->post('idWo'),
                'created_by' => $this->session->userdata('tempId'),
                'created_date' => date('Y-m-d H:i:s')
            );

            if ($this->M_Tgl_sewa->createBiayaIzin($data) > 0) {
                echo json_encode(array(
                    "cek" => $cek,
                    "message" => "Data berhasil disimpan!"
                ));
            }
        }
    }

    function getBiaya(){
        $list = $this->M_Tgl_sewa->getBiaya($this->input->post("idWo"));
        echo json_encode(array(
            "data" => $list
        ));
    }

    function save(){
        $list = $this->M_Tgl_sewa->getMasterBiaya();
        $cek = $this->M_Tgl_sewa->cekbiayaizindetail($this->input->post("idWoBiaya"));


        if ($cek == 0) {
            $countFormInput = count($this->input->post('id-biaya'));
            for ($i=0; $i<$countFormInput;$i++){
                $nominal = 0;
                if ($this->input->post("id-biaya")[$i] == 0 || $this->input->post("nominal")[$i] == ""){
                    $nominal = 0;
                }else{
                    $nominal = str_replace(".","",$this->input->post("nominal")[$i]);
                }

                if ($this->input->post("tglmulai") != null && $this->input->post("tglselesai") != null && $this->input->post("id-biaya")[$i] == 2){
                    $data = array(
                        "wo_biaya_izin_id" => $this->input->post("idWoBiaya"),
                        "biaya_id" => $this->input->post("id-biaya")[$i],
                        "nominal" => $nominal,
                        "tgl_mulai" => convertDateGaring($this->input->post("tglmulai")),
                        "tgl_selesai" => convertDateGaring($this->input->post("tglselesai"))
                    );

                    $this->M_Tgl_sewa->createBiayaIzinDetail($data);
                }else{
                    $data = array(
                        "wo_biaya_izin_id" => $this->input->post("idWoBiaya"),
                        "biaya_id" => $this->input->post("id-biaya")[$i],
                        "nominal" => $nominal
                    );

                    $this->M_Tgl_sewa->createBiayaIzinDetail($data);
                }
            }

            $dataWo = array(
                "status" => 4
            );
            $this->M_Tgl_sewa->updateWorkorder($dataWo, $this->input->post("idWo"));

            echo json_encode(array(
                "result" => "true",
                "message" => "Data berhasil disimpan !"
            ));
        }else{
            $oldRecord = $this->M_Tgl_sewa->getBiaya($this->input->post("idWo"));

            foreach ($oldRecord as $row){
                $data = array(
                    "biaya_detil_id" => $row->biaya_detail_id,
                    "biaya_id" => $row->biaya_id,
                    "nominal" => $row->nominal,
                    "tgl_mulai" => $row->mulai != null ? $row->mulai : null,
                    "tgl_selesai" => $row->selesai != null ? $row->selesai : null,
                    "created_by" => $this->session->userdata('tempId'),
                    "created_date" => date('Y-m-d H:i:s')
                );

                $this->M_Tgl_sewa->saveToLog($data);
            }

            $countFormInput = count($this->input->post('id-biaya'));
            for ($i=0; $i<$countFormInput;$i++){
                $nominal = 0;
                if ($this->input->post("id-biaya")[$i] == 0 || $this->input->post("nominal")[$i] == ""){
                    $nominal = 0;
                }else{
                    $nominal = str_replace(".","",$this->input->post("nominal")[$i]);
                }

                $cekDetailBiaya = $this->M_Tgl_sewa->cekDetailBiaya($this->input->post("id-biaya")[$i], $this->input->post("idWoBiaya"));

                if ($cekDetailBiaya == 0){
                    if ($this->input->post("tglmulai") != null && $this->input->post("tglselesai") != null && $this->input->post("id-biaya")[$i] == 2){
                        $data = array(
                            "wo_biaya_izin_id" => $this->input->post("idWoBiaya"),
                            "biaya_id" => $this->input->post("id-biaya")[$i],
                            "nominal" => $nominal,
                            "tgl_mulai" => convertDateGaring($this->input->post("tglmulai")),
                            "tgl_selesai" => convertDateGaring($this->input->post("tglselesai"))
                        );

                        $this->M_Tgl_sewa->createBiayaIzinDetail($data);
                    }else{
                        $data = array(
                            "wo_biaya_izin_id" => $this->input->post("idWoBiaya"),
                            "biaya_id" => $this->input->post("id-biaya")[$i],
                            "nominal" => $nominal
                        );

                        $this->M_Tgl_sewa->createBiayaIzinDetail($data);
                    }
                }else{
                    if ($this->input->post("tglmulai") != null && $this->input->post("tglselesai") != null && $this->input->post("id-biaya")[$i] == 2){
                        $data = array(
                            "wo_biaya_izin_id" => $this->input->post("idWoBiaya"),
                            "biaya_id" => $this->input->post("id-biaya")[$i],
                            "nominal" => $nominal,
                            "tgl_mulai" => convertDateGaring($this->input->post("tglmulai")),
                            "tgl_selesai" => convertDateGaring($this->input->post("tglselesai"))
                        );

                        $where = array(
                            "biaya_id" => $this->input->post("id-biaya")[$i],
                            "wo_biaya_izin_id" => $this->input->post("idWoBiaya")
                        );

                        $this->M_Tgl_sewa->updatebiayadetail($data, $where);
                    }else{
                        $data = array(
                            "wo_biaya_izin_id" => $this->input->post("idWoBiaya"),
                            "biaya_id" => $this->input->post("id-biaya")[$i],
                            "nominal" => $nominal
                        );

                        $where = array(
                            "biaya_id" => $this->input->post("id-biaya")[$i],
                            "wo_biaya_izin_id" => $this->input->post("idWoBiaya")
                        );

                        $this->M_Tgl_sewa->updatebiayadetail($data, $where);
                    }

                }
            }
            echo json_encode(array(
                "result" => "true",
                "message" => "Data berasil diupdate !"
            ));
        }
    }


    function pdf($id){
        $this->ci_minifier->enable_obfuscator();

        $this->load->view(strtolower("preview_biaya_perizinan"), array(
            "css" => $this->assetor->generate("core-style"),
            "fcss" => $this->assetor->generate("form-style"),
            "data" => $this->M_Tgl_sewa->getFormPreviewT1($id),
            "detailBiaya" => $this->M_Tgl_sewa->getFormPreviewBiayaDetail($id),

        ));
    }

}
