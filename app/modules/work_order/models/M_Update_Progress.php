<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Created by vendi 02/13/20
 */

class M_Update_Progress extends CI_Model
{
    var $tbl_workorder = 'workorder';
    var $tbl_workorder_penugasan = 'workorder_penugasan';
    var $tbl_workorder_progress = 'workorder_progress';
    var $tbl_workorder_sku = 'workorder_sku';
    var $tbl_master_mitra = 'master_mitra';
    var $tbl_master_status = 'master_status';
    var $tbl_master_pic = 'master_pic';
    var $tbl_kawasan = "master_kawasan";
    var $tbl_appuser = 'appuser';
    var $tbl_jenis_minta = 'master_jenis_minta';
    var $tbl_kecamatan = 'master_kecamatan';
    var $tbl_kabupaten = 'master_kabupaten';
    var $tbl_kelurahan = 'master_desa';
    var $tbl_master_unit = 'master_unit';

    var $column_order = array(
        null,
        'workorder.sales_no',
        'workorder.tgl_sales',
        'workorder.wo_no',
        'workorder.tgl_wo',
        'workorder.status',
        'appuser.name',
        'master_mitra.nama');
    var $column_search = array(null, 'workorder.sales_no', 'workorder.tgl_sales', 'workorder.wo_no', 'workorder.tgl_wo', null, 'appuser.name', 'master_mitra.nama');
    var $column_search_mask = array(null, 'No SO', 'Tanggal SO', 'No WO', 'Tanggal WO', null, 'PIC', 'Mitra');
    var $order = array('workorder.id' => 'DESC');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    private function _get_datatables_query()
    {
        $this->db->select('
            '.$this->tbl_workorder.'.id,
            '.$this->tbl_workorder.'.sales_no, 
            '.$this->tbl_workorder.'.wo_no, 
            '.$this->tbl_workorder.'.tgl_sales, 
            '.$this->tbl_workorder.'.status as idstatus, 
            '.$this->tbl_workorder.'.tgl_wo,
            '.$this->tbl_workorder_penugasan.'.pic_id,
            '.$this->tbl_jenis_minta.'.kode,
            '.$this->tbl_workorder_sku.'.nama as filesku,
            '.$this->tbl_appuser.'.name,
            '.$this->tbl_appuser.'.userid as creator,
            '.$this->tbl_master_status.'.nama as status,
            (SELECT name FROM appuser where userid = '.$this->tbl_workorder_penugasan.'.pic_id) as createby,
            (SELECT kendala FROM workorder_progress where workorder_id = '.$this->tbl_workorder.'.id ORDER BY id desc LIMIT 1), 
            (SELECT progress FROM workorder_progress where workorder_id = '.$this->tbl_workorder.'.id ORDER BY id desc LIMIT 1) 
            ');

        $this->db->from($this->tbl_workorder);
        $this->db->join($this->tbl_workorder_penugasan, $this->tbl_workorder.'.id = '.$this->tbl_workorder_penugasan.'.workorder_id', 'left');

        $this->db->join($this->tbl_jenis_minta,
            $this->tbl_workorder.'.jenis_perizinan = '.$this->tbl_jenis_minta.'.id',
            'left');

        $this->db->join($this->tbl_workorder_sku,
            $this->tbl_workorder.'.id = '.$this->tbl_workorder_sku.'.workorder_id',
            'left');

        $this->db->join($this->tbl_appuser,
            $this->tbl_appuser.'.userid = '.$this->tbl_workorder_penugasan.'.pic_id',
            'left');

        $this->db->join($this->tbl_master_status,
            $this->tbl_workorder.'.status = '.$this->tbl_master_status.'.id',
            'left');

        $this->db->where($this->tbl_workorder.'.unit_id', $this->session->userdata('tempUnitId'));
        $this->db->where($this->tbl_workorder.'.is_active', 1);
        $this->db->where($this->tbl_workorder_penugasan.'.active', 1);
        $this->db->where($this->tbl_workorder.'.status > 1 and '.$this->tbl_workorder.'.status < 10');

        if ($this->input->post('start_date') != null && $this->input->post('end_date') != null){
            $this->db->where($this->column_search[$this->input->post('column')].' >=', convertDateGaring($this->input->post('start_date')));
            $this->db->where($this->column_search[$this->input->post('column')].' <=', convertDateGaring($this->input->post('end_date')));
            $order = $this->order;
            $this->db->order_by(key($order), reset($order));
        }

        $searchOrder = 0;

        if (isset($_POST["columns"])){
            foreach ($this->column_search as $index => $value) {
                if (isset($value) && isset($_POST["columns"][$index]["search"]) &&
                    !empty($_POST["columns"][$index]["search"]["value"])) {
                    if ($searchOrder === 0) {
                        $this->db->group_start();
                        $this->db->like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    } else {
                        $this->db->or_like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    }

                    $this->db->order_by(key($this->order), reset($this->order));

                    $searchOrder++;
                }
            }
            if ($searchOrder>0) {
                $this->db->group_end();
            }
        }


        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), reset($order));
        }
    }

    function get_datatables()
    {

        $this->_get_datatables_query();

        $order = $this->order;
        $this->db->order_by(key($order), reset($order));
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->_get_datatables_query();

        return $this->db->count_all_results();
    }

    function saveProgress($data){
        $this->db->insert($this->tbl_workorder_progress, $data);

        return $this->db->affected_rows();
    }

    private function _get_datatables_query_detail($id)
    {
        $this->db->select('
           a.wo_no,
           b.*
            ');

        $this->db->from($this->tbl_workorder." as a");
        $this->db->join($this->tbl_workorder_progress." as b", 'a.id = b.workorder_id');
        $this->db->where('a.unit_id', $this->session->userdata('tempUnitId'));
        $this->db->where('a.is_active', 1);
        $this->db->where('a.id', $id);
        $this->db->order_by('b.id', "DESC");

        $i = 0;

        $searchOrder = 0;
        $where = "sales_no";

        if ($_POST["columns"][1]['search']['value'] != null || $_POST["columns"][3]['search']['value'] != ""){
            $searchOrder = 1;
            $where = "sales_no";
        }else{
            $where = "sales_no";
        }

        if ($_POST["columns"][3]['search']['value'] != null || $_POST["columns"][3]['search']['value'] != ""){
            $searchOrder = 3;
            $where = "wo_no";
        }else{
            $where = "sales_no";
        }


        foreach ($this->column_search as $item) {
            if (isset($searchOrder) && isset($where)) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($where, $_POST["columns"][$searchOrder]['search']['value']);
                } else {
                    $this->db->or_like($where, $_POST["columns"][$searchOrder]['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }

            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_detail($id)
    {
        $searchOrder = 0;
        $where = "sales_no";

        if ($_POST["columns"][1]['search']['value'] != null || $_POST["columns"][3]['search']['value'] != ""){
            $searchOrder = 1;
            $where = "sales_no";
        }else{
            $where = "sales_no";
        }

        if ($_POST["columns"][3]['search']['value'] != null || $_POST["columns"][3]['search']['value'] != ""){
            $searchOrder = 3;
            $where = "wo_no";
        }else{
            $where = "sales_no";
        }

        $this->_get_datatables_query_detail($id);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_detail($id)
    {
        $this->_get_datatables_query_detail($id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all_detail($id)
    {
       $this->_get_datatables_query_detail($id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function getWoProgress($id){
        $this->db->where("id", $id);
        $query = $this->db->get($this->tbl_workorder_progress);
        return $query->row();
    }

    function getWo($id){
        $this->db->select('
           a.wo_no,
           (SELECT progress FROM workorder_progress where workorder_id = a.id ORDER BY id desc LIMIT 1),
            ');

        $this->db->from($this->tbl_workorder." as a");
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->row();
    }
}