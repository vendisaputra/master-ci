<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by fajar at 1/29/20
 * Modified by vendi at 2/05/20
 */
class M_Work_order extends CI_Model
{

    var $tbl_workorder = 'workorder';
    var $tbl_workorder_penugasan = 'workorder_penugasan';
    var $tbl_workorder_sku = 'workorder_sku';
    var $tbl_master_mitra = 'master_mitra';
    var $tbl_master_status = 'master_status';
    var $tbl_kawasan = "master_kawasan";
    var $tbl_appuser = 'appuser';
    var $tbl_jenis_minta = 'master_jenis_minta';
    var $tbl_kecamatan = 'master_kecamatan';
    var $tbl_kabupaten = 'master_kabupaten';
    var $tbl_kelurahan = 'master_desa';
    var $tbl_master_unit = 'master_unit';

    var $column_order = array(
        null,
        'sales_no',
        'tgl_sales',
        'wo_no',
        'tgl_wo',
        'status',
        'name',
        'mitra',
        'filesku');

    var $column_search = array(null, 'workorder.sales_no', 'workorder.tgl_sales', 'workorder.wo_no', 'workorder.tgl_wo', null, 'appuser.name', 'master_mitra.nama', 'workorder_sku.nama');

    var $column_search_mask = array(null, 'No SO', 'Tanggal SO', 'No WO', 'Tanggal WO', null, 'PIC', 'Mitra', 'File SKU');

    var $order = array('id' => 'DESC');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    private function _get_datatables_query()
    {
        $this->db->select('
            '.$this->tbl_workorder.'.id,
            '.$this->tbl_workorder.'.sales_no, 
            '.$this->tbl_workorder.'.wo_no, 
            '.$this->tbl_workorder.'.tgl_sales, 
            '.$this->tbl_workorder.'.status as idstatus, 
            '.$this->tbl_workorder.'.tgl_wo,
            '.$this->tbl_workorder.'.created_by as created_id,
            '.$this->tbl_master_mitra.'.nama as mitra,
            split_part('.$this->tbl_workorder_sku.'.nama, \'--\', 2) as filesku,
            '.$this->tbl_appuser.'.name,
            '.$this->tbl_master_status.'.nama as status,
            (select name from '.$this->tbl_appuser.' where userid  = '.$this->tbl_workorder.'.created_by) as created_by
            ');

        $this->db->from($this->tbl_workorder);
        $this->db->join($this->tbl_workorder_penugasan,
            $this->tbl_workorder.'.id = '.$this->tbl_workorder_penugasan.'.workorder_id',
            'left');

        $this->db->join($this->tbl_master_mitra,
            $this->tbl_workorder_penugasan.'.mitra_id = '.$this->tbl_master_mitra.'.id',
            'left');

        $this->db->join($this->tbl_workorder_sku,
            $this->tbl_workorder.'.id = '.$this->tbl_workorder_sku.'.workorder_id',
            'left');

        $this->db->join($this->tbl_appuser,
            $this->tbl_appuser.'.userid = '.$this->tbl_workorder_penugasan.'.pic_id',
            'left');

        $this->db->join($this->tbl_master_status,
            $this->tbl_workorder.'.status = '.$this->tbl_master_status.'.id',
            'left');

        $this->db->where($this->tbl_workorder.'.unit_id',
            $this->session->userdata('tempUnitId'));

        $this->db->where($this->tbl_workorder.'.is_active', 1);
        $this->db->where($this->tbl_workorder.'.status < 11');
        $this->db->where($this->tbl_workorder_penugasan.'.active', 1);

        if ($this->input->post('start_date') != null && $this->input->post('end_date') != null){
            $this->db->where($this->column_search[$this->input->post('column')].' >=', convertDateGaring($this->input->post('start_date')));
            $this->db->where($this->column_search[$this->input->post('column')].' <=', convertDateGaring($this->input->post('end_date')));
            $order = $this->order;
            $this->db->order_by(key($order), reset($order));
        }

        $searchOrder = 0;

        if (isset($_POST["columns"])){
            foreach ($this->column_search as $index => $value) {
                if (isset($value) && isset($_POST["columns"][$index]["search"]) &&
                    !empty($_POST["columns"][$index]["search"]["value"])) {
                    if ($searchOrder === 0) {
                        $this->db->group_start();
                        $this->db->like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    } else {
                        $this->db->or_like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    }

                    $this->db->order_by(key($this->order), reset($this->order));

                    $searchOrder++;
                }
            }
            if ($searchOrder>0) {
                $this->db->group_end();
            }
        }


        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), reset($order));
        }
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->_get_datatables_query();

        return $this->db->count_all_results();
    }

    function get_datatables()
    {

        $this->_get_datatables_query();
        $this->db->order_by(key($this->order), reset($this->order));
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function getDataEdit($idWo){
        $this->db->select('
            a.*,
            b.pic_id as id_pic, 
            c.nama as mitra, 
            c.id as idMitra,
            d.path,
            e.name,
            f.nama as status_text
            ');
        $this->db->from($this->tbl_workorder." as a");
        $this->db->join($this->tbl_workorder_penugasan." as b", 'a.id = b.workorder_id', 'left');
        $this->db->join($this->tbl_master_mitra." as c", 'b.mitra_id = c.id', 'left');
        $this->db->join($this->tbl_workorder_sku." as d", 'a.id = d.workorder_id', 'left');
        $this->db->join($this->tbl_appuser." as e", 'e.userid = b.pic_id', 'left');
        $this->db->join($this->tbl_master_status." as f", 'a.status = f.id', 'left');
        $this->db->where('a.unit_id', $this->session->userdata('tempUnitId'));
        $this->db->where('a.is_active', 1);
        $this->db->where('a.id', $idWo);

        $query = $this->db->get();
        return $query->row();
    }

    function cekNoWo($woNo)
    {

        $this->db->where('wo_no', $woNo);
        $query = $this->db->get($this->tbl_workorder);

        return $query->num_rows();
    }

    function saveworkorder($data)
    {
        $this->db->insert($this->tbl_workorder, $data);

        return $this->db->affected_rows();
    }

    function savesku($data){
        $this->db->insert($this->tbl_workorder_sku, $data);

        return $this->db->affected_rows();
    }

    function updateworkorder($data, $id){
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update($this->tbl_workorder);
        return $this->db->affected_rows();
    }

    function getWoSku($id){
        $this->db->from($this->tbl_workorder_sku);
        $this->db->where('workorder_id', $id);
        $sql = $this->db->get();

        return $sql->row();
    }

    function saveWorkorderPenugasan($data){
        $this->db->insert($this->tbl_workorder_penugasan, $data);

        return $this->db->affected_rows();
    }

    function updateWorkorderPenugasan($data, $id){
        $this->db->set($data);
        $this->db->where('workorder_id', $id);
        $this->db->update($this->tbl_workorder_penugasan);
        return $this->db->affected_rows();
    }

    function updateSku($data, $id){
        $this->db->set($data);
        $this->db->where('workorder_id', $id);
        $this->db->update($this->tbl_workorder_sku);
        return $this->db->affected_rows();
    }

    function cekWorkorder($data){

        $this->db->where($data);
        $query = $this->db->get($this->tbl_workorder);
        return $query->row();
    }

    function cekPenugasan($idWo){
        $this->db->where("workorder_id", $idWo);
        $query = $this->db->get($this->tbl_workorder_penugasan);
        return $query->num_rows();
    }

    function getPic(){

        $this->db->select('*');
        $this->db->from($this->tbl_appuser);
        $this->db->where('roleid', 2);
        $this->db->where('unit_id', $this->session->userdata('tempUnitId'));
        $sql=$this->db->get();

        return $sql->result();
    }

    function getJenisPermintaan(){
        $this->db->where('is_active', 1);
        $this->db->limit(3);
        $sql=$this->db->get($this->tbl_jenis_minta);

        return $sql->result();
    }

    function getKecamatan($kabId){
        $this->db->where('kabupaten_id', $kabId);
        $this->db->order_by('nama', 'ASC');
        $sql=$this->db->get($this->tbl_kecamatan);

        return $sql->result();
    }

    function getKabupaten($unitId){
        $this->db->where('unit_id', $unitId);
        $this->db->where('is_active', 1);
        $this->db->order_by('nama', 'ASC');
        $sql=$this->db->get($this->tbl_kabupaten);
        return $sql->result();
    }

    function getKelurahan($kecId){
        $this->db->where('kecamatan_id', $kecId);
        $this->db->order_by('nama', 'ASC');
        $sql=$this->db->get($this->tbl_kelurahan);

        return $sql->result();
    }

    function getMitra(){
        $this->db->select("*");
        $this->db->from($this->tbl_master_mitra);
        $this->db->where("is_active", 1);
        $sql=$this->db->get();

        return $sql->result();
    }

    function getKawasan($uniId){
        $this->db->where('unit_id', $uniId);
        $sql=$this->db->get($this->tbl_kawasan);

        return $sql->result();
    }

    function getLocationKawasan($idKawasan){
        $this->db->where('id', $idKawasan);
        $sql=$this->db->get($this->tbl_kawasan);

        return $sql->result()[0];
    }

    function getCountWorkorder(){
        $this->db->where(" TO_CHAR(created_date,'YYYYMM')=TO_CHAR(CURRENT_DATE,'YYYYMM')");
        $this->db->where("unit_id", $this->session->userdata('tempUnitId'));
        $sql=$this->db->get($this->tbl_workorder);

        return $sql->num_rows();
    }

    function getMasterUnit($unitId){
        $this->db->where('id', $unitId);
        $sql=$this->db->get($this->tbl_master_unit);

        return $sql->row();
    }
}
