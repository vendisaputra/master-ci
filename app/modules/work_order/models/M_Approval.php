<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by Vendi at 2/14/20
 */
class M_Approval extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
    }

    var $tbl_workorder = 'workorder';
    var $tbl_workorder_penugasan = 'workorder_penugasan';
    var $tbl_workorder_deposit = 'workorder_biaya_deposit';
    var $tbl_workorder_biaya_izin = 'workorder_biaya_izin';
    var $tbl_workoder_biaya_detail = 'workorder_biaya_detail';
    var $tbl_workorder_biaya_deposit = 'workorder_biaya_deposit';
    var $tbl_workorder_progress = 'workorder_progress';
    var $tbl_workorder_approval = 'workorder_approval';
    var $tbl_workorder_sik= 'workorder_sik';
    var $tbl_workorder_sku = 'workorder_sku';
    var $tbl_master_mitra = 'master_mitra';
    var $tbl_master_status = 'master_status';
    var $tbl_master_pic = 'master_pic';
    var $tbl_kawasan = "master_kawasan";
    var $tbl_appuser = 'appuser';
    var $tbl_jenis_minta = 'master_jenis_minta';
    var $tbl_kecamatan = 'master_kecamatan';
    var $tbl_kabupaten = 'master_kabupaten';
    var $tbl_kelurahan = 'master_desa';
    var $tbl_master_unit = 'master_unit';
    var $tbl_master_biaya = 'master_biaya';

    var $column_order = array(
        null,
        'workorder.sales_no',
        'workorder.tgl_sales',
        'workorder.wo_no',
        'workorder.tgl_wo',
        'workorder.status',
        'appuser.name',
        'workorder.tgl_mulai',
        'workorder.tgl_selesai',
        'biaya_izin',
        'workorder_biaya_deposit.biaya_deposit');

    var $column_search = array(null, 'workorder.sales_no', 'workorder.tgl_sales', 'workorder.wo_no', 'workorder.tgl_wo', 'master_jenis_minta.kode', 'appuser.name', 'workorder.tgl_mulai', 'workorder.tgl_selesai', null, null);

    var $column_search_mask = array(null, 'No SO', 'Tanggal SO', 'No WO', 'Tanggal WO', 'Jenis perizinan', 'PIC', 'Tanggal awal perizinan', 'Tanggal akhir perizinan', null, null);

    var $order = array('workorder.id' => 'DESC');

    private function _get_datatables_query()
    {
        $this->db->select('
            '.$this->tbl_workorder.'.id,
            '.$this->tbl_workorder.'.sales_no, 
            '.$this->tbl_workorder.'.wo_no, 
            '.$this->tbl_workorder.'.tgl_sales, 
            '.$this->tbl_workorder.'.status as idstatus, 
            '.$this->tbl_workorder.'.tgl_wo,
            '.$this->tbl_workorder.'.tgl_mulai,
            '.$this->tbl_workorder.'.tgl_selesai,
            '.$this->tbl_master_mitra.'.nama as mitra, 
            '.$this->tbl_appuser.'.name,
            '.$this->tbl_master_status.'.nama as status,
            '.$this->tbl_jenis_minta.'.kode as jenis_perizinan,
            '.$this->tbl_workorder_deposit.'.biaya_deposit,
            (Select SUM(nominal) FROM '.$this->tbl_workoder_biaya_detail.' where wo_biaya_izin_id = '.$this->tbl_workorder_biaya_izin.'.id AND biaya_id NOT IN  (14, 15, 16)) as biaya_izin,
            (select tgl_mulai from '.$this->tbl_workoder_biaya_detail.' where wo_biaya_izin_id = '.$this->tbl_workorder_biaya_izin.'.id AND biaya_id = 2) as detail_tgl_mulai,
            (select tgl_selesai from '.$this->tbl_workoder_biaya_detail.' where wo_biaya_izin_id = '.$this->tbl_workorder_biaya_izin.'.id AND biaya_id = 2) as detail_tgl_selesai');

        $this->db->from($this->tbl_workorder);
        $this->db->join($this->tbl_workorder_penugasan,
            $this->tbl_workorder.'.id = '.$this->tbl_workorder_penugasan.'.workorder_id',
            'left');

        $this->db->join($this->tbl_master_mitra,
            $this->tbl_workorder_penugasan.'.mitra_id = '.$this->tbl_master_mitra.'.id',
            'left');

        $this->db->join($this->tbl_appuser,
            $this->tbl_appuser.'.userid = '.$this->tbl_workorder_penugasan.'.pic_id',
            'left');

        $this->db->join($this->tbl_master_status,
            $this->tbl_workorder.'.status = '.$this->tbl_master_status.'.id',
            'left');

        $this->db->join($this->tbl_jenis_minta,
            $this->tbl_workorder.'.jenis_perizinan = '.$this->tbl_jenis_minta.'.id',
            'left');

        $this->db->join($this->tbl_workorder_deposit,
            $this->tbl_workorder_deposit.'.workorder_id = '.$this->tbl_workorder.'.id AND '. $this->tbl_workorder_deposit.'.is_active = 1',
            'left');

        $this->db->join($this->tbl_workorder_biaya_izin,
            $this->tbl_workorder_biaya_izin.'.workorder_id = '.$this->tbl_workorder.'.id',
            'left');

        $this->db->where($this->tbl_workorder.'.unit_id', $this->session->userdata('tempUnitId'));
        $this->db->where($this->tbl_workorder.'.is_active', 1);
        $this->db->where($this->tbl_workorder_penugasan.'.active', 1);
        $this->db->where($this->tbl_workorder.'.status > 5 AND '.$this->tbl_workorder.'.status < 12');

        if ($this->input->post('start_date') != null && $this->input->post('end_date') != null){
            $this->db->where($this->column_search[$this->input->post('column')].' >=', convertDateGaring($this->input->post('start_date')));
            $this->db->where($this->column_search[$this->input->post('column')].' <=', convertDateGaring($this->input->post('end_date')));
            $order = $this->order;
            $this->db->order_by(key($order), reset($order));
        }

        $searchOrder = 0;

        if (isset($_POST["columns"])){
            foreach ($this->column_search as $index => $value) {
                if (isset($value) && isset($_POST["columns"][$index]["search"]) &&
                    !empty($_POST["columns"][$index]["search"]["value"])) {
                    if ($searchOrder === 0) {
                        $this->db->group_start();
                        $this->db->like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    } else {
                        $this->db->or_like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    }

                    $this->db->order_by(key($this->order), reset($this->order));

                    $searchOrder++;
                }
            }
            if ($searchOrder>0) {
                $this->db->group_end();
            }
        }


        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), reset($order));
        }
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
       $this->_get_datatables_query();

        return $this->db->count_all_results();
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        $this->db->order_by(key($this->order), reset($this->order));
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function biayaDepositQuery()
    {
        $this->db->from($this->tbl_workorder);

        $this->db->join($this->tbl_workorder_deposit,
            $this->tbl_workorder . ".id = " . $this->tbl_workorder_deposit . ".workorder_id");

        $this->db->join($this->tbl_workorder_penugasan,
            $this->tbl_workorder . ".id = " . $this->tbl_workorder_penugasan . ".workorder_id", "left");

        $this->db->join($this->tbl_appuser,
            $this->tbl_appuser . ".userid = " . $this->tbl_workorder_penugasan . ".pic_id", "left");

        $this->db->join($this->tbl_jenis_minta,
            $this->tbl_jenis_minta . ".id = " . $this->tbl_workorder . ".jenis_perizinan", "left");

        $this->db->join($this->tbl_master_status,
            $this->tbl_workorder . ".status = " . $this->tbl_master_status . ".id", "left");

        $this->db->where($this->tbl_workorder . ".is_active", 1);
    }

    public function getBiayaDeposit($id)
    {
        $this->db->select(
            $this->tbl_workorder . ".status, " .
            $this->tbl_workorder_deposit . ".*, "
        );

        $this->biayaDepositQuery();

        $this->db->group_start();
        $this->db->where($this->tbl_workorder_deposit . ".workorder_id", $id);
        $this->db->group_end();

        $sql = $this->db->get();

        return $sql->num_rows() > 0 ? $sql->result()[0] : null;
    }

    public function getHistoryDate($id){
        $this->db->select("
             xx.*,
             (SELECT pic_id from ".$this->tbl_workorder_penugasan." where workorder_id=xx.wo_id limit 1) as ppic,
             (SELECT  
                (SELECT name FROM ".$this->tbl_appuser." WHERE userid=pic_id) FROM ".$this->tbl_workorder_penugasan."
                    WHERE workorder_id=xx.wo_id
                    AND DATE_PART('day', xx.end_date ::timestamp - created_date::timestamp) >= 0
                    ORDER BY DATE_PART('day', xx.end_date ::timestamp - created_date::timestamp) ASC
                LIMIT 1) as nama_pic     
        ");

        $this->db->from('
            (SELECT a.id,
            a.nama ,
            b.id as wo_id,
           CASE WHEN a.id= 1 THEN b.created_date
                 WHEN a.id= 2 THEN b.created_date
                 WHEN a.id= 3 THEN c.created_date
                 WHEN a.id= 4 THEN d.created_date
                 WHEN a.id= 6 THEN e.created_date
                 WHEN a.id= 7 THEN h.created_date
                 WHEN a.id= 8 THEN f.created_date
                 WHEN a.id= 9 THEN g.created_date
                 WHEN a.id= 10 THEN i.created_date
                 WHEN a.id= 14 THEN (SELECT tgl_progress from '.$this->tbl_workorder_progress.' WHERE workorder_id=a.id ORDER BY id DESC limit 1)
           ELSE null END AS start_date,
           CASE WHEN a.id= 1 THEN b.created_date
                 WHEN a.id= 2 THEN c.created_date
                 WHEN a.id= 3 THEN d.created_date
                 WHEN a.id= 4 THEN e.created_date
                 WHEN a.id= 6 THEN h.created_date
                 WHEN a.id= 7 THEN f.created_date
                 WHEN a.id= 8 THEN g.created_date
                 WHEN a.id= 9 THEN i.created_date
                 WHEN a.id= 10 THEN j.created_date
                 WHEN a.id= 14 THEN (SELECT tgl_progress from '.$this->tbl_workorder_progress.' WHERE workorder_id=a."id" ORDER BY id DESC limit 1)
           ELSE null END AS end_date
           
           FROM '.$this->tbl_master_status." as a".'
           LEFT JOIN  '.$this->tbl_workorder." as b".' on b.id= '.$id.'
           LEFT JOIN '.$this->tbl_workorder_penugasan." as c".' on c.workorder_id=b.id and c.active=1
           LEFT JOIN '.$this->tbl_workorder_sku." as d".' on d.workorder_id=b.id and d.is_active=1
           LEFT JOIN '.$this->tbl_workorder_biaya_izin." as e".' on e.workorder_id=b.id
           LEFT JOIN '.$this->tbl_workorder_deposit." as f".' on f.workorder_id=b.id and f.is_active=1
           LEFT JOIN '.$this->tbl_workorder_approval." as g".' on g.id=f.workorder_approval_id
           LEFT JOIN '.$this->tbl_workorder_approval." as h".' on e.workorder_approval_id=h.id
           LEFT JOIN '.$this->tbl_workorder_sik." as i".' on i.workorder_id=b.id and i.is_active=1
           LEFT JOIN '.$this->tbl_workorder_approval." as j".' on b.workorder_approval_id=j.id
           WHERE a.id not in (5,11,12,13)
           ORDER BY a.id ASC 
            ) xx
        ');

        $query = $this->db->get();
        return $query->result();
    }

    function getDataEditWorkorder($idWo){
        $this->db->select('
            a.*,
            b.pic_id as id_pic, 
            c.nama as mitra, 
            c.id as idMitra,
            d.path,
            e.name,
            f.nama as statusWo
            ');
        $this->db->from($this->tbl_workorder." as a");
        $this->db->join($this->tbl_workorder_penugasan." as b", 'a.id = b.workorder_id', 'left');
        $this->db->join($this->tbl_master_mitra." as c", 'b.mitra_id = c.id', 'left');
        $this->db->join($this->tbl_workorder_sku." as d", 'a.id = d.workorder_id', 'left');
        $this->db->join($this->tbl_appuser." as e", 'e.userid = b.pic_id', 'left');
        $this->db->join($this->tbl_master_status." as f", 'a.status = f.id', 'left');
        $this->db->where('a.unit_id', $this->session->userdata('tempUnitId'));
        $this->db->where('a.is_active', 1);
        $this->db->where('a.id', $idWo);

        $query = $this->db->get();
        return $query->row();
    }

    function getDataSik($idWo){
        $this->db->where('workorder_id', $idWo);
        $sql=$this->db->get($this->tbl_workorder_sik);

        return $sql->result();
    }

    function getKecamatan($kabId){
        $this->db->where('kabupaten_id', $kabId);
        $this->db->order_by('nama', 'ASC');
        $sql=$this->db->get($this->tbl_kecamatan);

        return $sql->result();
    }

    function getKabupaten($unitId){
        $this->db->where('unit_id', $unitId);
        $this->db->where('is_active', 1);
        $this->db->order_by('nama', 'ASC');
        $sql=$this->db->get($this->tbl_kabupaten);
        return $sql->result();
    }

    function getKelurahan($kecId){
        $this->db->where('kecamatan_id', $kecId);
        $this->db->order_by('nama', 'ASC');
        $sql=$this->db->get($this->tbl_kelurahan);

        return $sql->result();
    }

    function getMitra(){
        $this->db->select("*");
        $this->db->from($this->tbl_master_mitra);
        $sql=$this->db->get();

        return $sql->result();
    }

    function getKawasan($uniId){
        $this->db->where('unit_id', $uniId);
        $sql=$this->db->get($this->tbl_kawasan);

        return $sql->result();
    }

    function cekPenugasan($idWo){
        $this->db->where("workorder_id", $idWo);
        $query = $this->db->get($this->tbl_workorder_penugasan);
        return $query->num_rows();
    }

    function getPic(){

        $this->db->select('*');
        $this->db->from($this->tbl_appuser);
        $this->db->where('roleid', 2);
        $sql=$this->db->get();

        return $sql->result();
    }

    function getMasterBiaya(){
        $this->db->from($this->tbl_master_biaya);
        $this->db->where("is_active", 1);
        $this->db->where("is_revenue", 0);
        $sql = $this->db->get();
        return $sql->result();
    }

    function getJenisPermintaan(){
        $this->db->where('is_active', 1);
        $this->db->limit(3);
        $sql=$this->db->get($this->tbl_jenis_minta);

        return $sql->result();
    }

    function getCountWorkorder(){
        $this->db->where(" TO_CHAR(created_date,'YYYYMM')=TO_CHAR(CURRENT_DATE,'YYYYMM')");
        $sql=$this->db->get($this->tbl_workorder);

        return $sql->num_rows();
    }

    function getMasterUnit($unitId){
        $this->db->where('id', $unitId);
        $sql=$this->db->get($this->tbl_master_unit);

        return $sql->row();
    }

    function saveSik($data){
        $this->db->insert($this->tbl_workorder_sik, $data);

        return $this->db->affected_rows();
    }

    function saveApproval($data){
        $this->db->insert($this->tbl_workorder_approval, $data);

        return $this->db->affected_rows();
    }

    function updateWorkorder($data, $id){
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update($this->tbl_workorder);
        return $this->db->affected_rows();
    }

    function cekApproval($data){
        $this->db->where($data);
        $query = $this->db->get($this->tbl_workorder_approval);
        return $query->row();
    }

    function getFileDeposit($id){
        $this->db->from($this->tbl_workorder_deposit);
        $this->db->where('id', $id);
        $sql = $this->db->get();

        return $sql->row();
    }

}
