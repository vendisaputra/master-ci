<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by vendi at 7/4/20
 */
class M_Tgl_sewa extends CI_Model
{

    var $tbl_workorder_biaya_izin = 'workorder_biaya_izin';
    var $tbl_workorder = 'workorder';
    var $tbl_workorder_penugasan = 'workorder_penugasan';
    var $tbl_master_mitra = 'master_mitra';
    var $tbl_master_status = 'master_status';
    var $tbl_appuser = 'appuser';
    var $tbl_master_jenis_minta = 'master_jenis_minta';
    var $tbl_master_biaya = 'master_biaya';
    var $tbl_workoder_biaya_detail = 'workorder_biaya_detail';
    var $tbl_workoder_approval = 'workorder_approval';
    var $tbl_log_update_sewa = 'log_update_sewa';


    var $tbl_wbi = "w_b_i";

    var $column_order = array(null, 'workorder.sales_no', 'workorder.wo_no', 'workorder.status', 'workorder_penugasan.nama_pic', 'master_mitra.nama');

    var $column_search = array(null, 'workorder.sales_no', 'workorder.tgl_sales', 'workorder.wo_no', 'workorder.tgl_wo', 'master_jenis_minta.kode', 'appuser.name', null, null, null);

    var $column_search_mask = array(null, 'No SO', 'Tanggal SO', 'No WO', 'Tanggal WO', 'Jenis perizinan', 'PIC', null, null, null);

    var $order = array('workorder.id' => 'desc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        $this->db->select(
            '
            '.$this->tbl_workorder.'.id,
            '.$this->tbl_workorder.'.sales_no, 
            '.$this->tbl_workorder.'.wo_no, 
            '.$this->tbl_workorder.'.tgl_sales, 
            '.$this->tbl_workorder.'.status as idstatus, 
            '.$this->tbl_workorder.'.tgl_wo,
            '.$this->tbl_workorder.'.jenis_perizinan,
            '.$this->tbl_workorder.'.status,
            '.$this->tbl_workorder_biaya_izin.'.approval,
            '.$this->tbl_workorder_penugasan.'.pic_id,
            '.$this->tbl_master_mitra.'.nama as mitra,
            '.$this->tbl_appuser.'.name,
            '.$this->tbl_master_status.'.nama as txt_status,
            '.$this->tbl_master_jenis_minta.'.kode,
            (Select SUM(nominal) FROM '.$this->tbl_workoder_biaya_detail.' where wo_biaya_izin_id = '.$this->tbl_workorder_biaya_izin.'.id AND biaya_id NOT IN  (14, 15, 16)) as biaya_izin,
            (select tgl_mulai from '.$this->tbl_workoder_biaya_detail.' where wo_biaya_izin_id = '.$this->tbl_workorder_biaya_izin.'.id AND biaya_id = 2) as detail_tgl_mulai,
            (select tgl_selesai from '.$this->tbl_workoder_biaya_detail.' where wo_biaya_izin_id = '.$this->tbl_workorder_biaya_izin.'.id AND biaya_id = 2) as detail_tgl_selesai');

        $this->db->from($this->tbl_workorder);

        $this->db->join($this->tbl_workorder_biaya_izin,
            $this->tbl_workorder.'.id = '.$this->tbl_workorder_biaya_izin.'.workorder_id',
            'left');

        $this->db->join($this->tbl_workorder_penugasan,
            $this->tbl_workorder.'.id = '.$this->tbl_workorder_penugasan.'.workorder_id',
            'left');

        $this->db->join($this->tbl_master_mitra,
            $this->tbl_workorder_penugasan.'.mitra_id = '.$this->tbl_master_mitra.'.id',
            'left');

        $this->db->join($this->tbl_appuser,
            $this->tbl_appuser.'.userid = '.$this->tbl_workorder_penugasan.'.pic_id',
            'left');

        $this->db->join($this->tbl_master_status,
            $this->tbl_workorder.'.status = '.$this->tbl_master_status.'.id',
            'left');

        $this->db->join($this->tbl_master_jenis_minta,
            $this->tbl_master_jenis_minta.'.id = '.$this->tbl_workorder.'.jenis_perizinan',
            'left');

        $this->db->where($this->tbl_workorder_penugasan.".active", 1);
        $this->db->where($this->tbl_workorder.".unit_id", $this->session->userdata('tempUnitId'));
        $this->db->where($this->tbl_workorder.".status > 5");
        $this->db->where($this->tbl_workorder.".status NOT IN (12)");

        if ($this->input->post('start_date') != null && $this->input->post('end_date') != null){
            $this->db->where($this->column_search[$this->input->post('column')].' >=', convertDateGaring($this->input->post('start_date')));
            $this->db->where($this->column_search[$this->input->post('column')].' <=', convertDateGaring($this->input->post('end_date')));
            $order = $this->order;
            $this->db->order_by(key($order), reset($order));
        }

        $searchOrder = 0;

        if (isset($_POST["columns"])){
            foreach ($this->column_search as $index => $value) {
                if (isset($value) && isset($_POST["columns"][$index]["search"]) &&
                    !empty($_POST["columns"][$index]["search"]["value"])) {
                    if ($searchOrder === 0) {
                        $this->db->group_start();
                        $this->db->like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    } else {
                        $this->db->or_like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    }

                    $this->db->order_by(key($this->order), reset($this->order));

                    $searchOrder++;
                }
            }
            if ($searchOrder>0) {
                $this->db->group_end();
            }
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();

        $order = $this->order;
        $this->db->order_by(key($order), reset($order));

        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->_get_datatables_query();
        return $this->db->count_all_results();
    }

    private function _get_datatables_query_approval()
    {
        $this->db->select(
            '
            '.$this->tbl_workorder.'.id,
            '.$this->tbl_workorder.'.sales_no, 
            '.$this->tbl_workorder.'.wo_no, 
            '.$this->tbl_workorder.'.tgl_sales, 
            '.$this->tbl_workorder.'.status as idstatus, 
            '.$this->tbl_workorder.'.tgl_wo,
            '.$this->tbl_workorder.'.jenis_perizinan,
            '.$this->tbl_workorder.'.status,
            '.$this->tbl_workorder_biaya_izin.'.approval,
            '.$this->tbl_workorder_biaya_izin.'.revenue,
            '.$this->tbl_workorder_penugasan.'.pic_id,
            '.$this->tbl_master_mitra.'.nama as mitra,
            '.$this->tbl_appuser.'.name,
            '.$this->tbl_master_status.'.nama,
            '.$this->tbl_master_jenis_minta.'.kode,
            (Select SUM(nominal) FROM '.$this->tbl_workoder_biaya_detail.' where wo_biaya_izin_id = '.$this->tbl_workorder_biaya_izin.'.id AND biaya_id NOT IN  (14, 15, 16)) as biaya_izin');

        $this->db->from($this->tbl_workorder);

        $this->db->join($this->tbl_workorder_biaya_izin,
            $this->tbl_workorder.'.id = '.$this->tbl_workorder_biaya_izin.'.workorder_id',
            'left');

        $this->db->join($this->tbl_workorder_penugasan,
            $this->tbl_workorder.'.id = '.$this->tbl_workorder_penugasan.'.workorder_id',
            'left');

        $this->db->join($this->tbl_master_mitra,
            $this->tbl_workorder_penugasan.'.mitra_id = '.$this->tbl_master_mitra.'.id',
            'left');

        $this->db->join($this->tbl_appuser,
            $this->tbl_appuser.'.userid = '.$this->tbl_workorder_penugasan.'.pic_id',
            'left');

        $this->db->join($this->tbl_master_status,
            $this->tbl_workorder.'.status = '.$this->tbl_master_status.'.id',
            'left');

        $this->db->join($this->tbl_master_jenis_minta,
            $this->tbl_master_jenis_minta.'.id = '.$this->tbl_workorder.'.jenis_perizinan',
            'left');

        $this->db->where($this->tbl_workorder_penugasan.".active", 1);
        $this->db->where($this->tbl_workorder.".unit_id", $this->session->userdata('tempUnitId'));
        if ($this->session->userdata('tempRole') == 5){
            $this->db->where($this->tbl_workorder.".status > 4");
            $this->db->where($this->tbl_workorder.".status < 7");
        }else if ($this->session->userdata('tempRole') == 4){
            $this->db->where($this->tbl_workorder.".status > 3");
            $this->db->where($this->tbl_workorder.".status < 6");
        }

        if ($this->input->post('start_date') != null && $this->input->post('end_date') != null){
            $this->db->where($this->column_search[$this->input->post('column')].' >=', convertDateGaring($this->input->post('start_date')));
            $this->db->where($this->column_search[$this->input->post('column')].' <=', convertDateGaring($this->input->post('end_date')));
            $order = $this->order;
            $this->db->order_by(key($order), reset($order));
        }

        $searchOrder = 0;

        if (isset($_POST["columns"])){
            foreach ($this->column_search as $index => $value) {
                if (isset($value) && isset($_POST["columns"][$index]["search"]) &&
                    !empty($_POST["columns"][$index]["search"]["value"])) {
                    if ($searchOrder === 0) {
                        $this->db->group_start();
                        $this->db->like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    } else {
                        $this->db->or_like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    }

                    $this->db->order_by(key($this->order), reset($this->order));

                    $searchOrder++;
                }
            }
            if ($searchOrder>0) {
                $this->db->group_end();
            }
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_approval()
    {
        $this->_get_datatables_query_approval();

        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);

        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_approval()
    {
        $this->_get_datatables_query_approval();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_approval()
    {
        $this->db->from($this->tbl_workorder);
        return $this->db->count_all_results();
    }

    function getMasterBiaya(){
        $this->db->from($this->tbl_master_biaya);
        $this->db->where("is_active", 1);
        $this->db->where("is_revenue", 0);
        $sql = $this->db->get();
        return $sql->result();
    }

    function getRevenue(){
        $this->db->from($this->tbl_master_biaya);
        $this->db->where("is_active", 1);
        $this->db->where("is_revenue", 1);
        $sql = $this->db->get();
        return $sql->result();
    }

    function cekBiayaIzin($id){
        $this->db->where("workorder_id", $id);
        $query = $this->db->get($this->tbl_workorder_biaya_izin);
        return $query->num_rows();
    }

    function cekbiayaizindetail($idBiayaIzin){
        $this->db->where("wo_biaya_izin_id", $idBiayaIzin);
        $query = $this->db->get($this->tbl_workoder_biaya_detail);
        return $query->num_rows();
    }

    function createBiayaIzin($data){
        $this->db->insert($this->tbl_workorder_biaya_izin, $data);

        return $this->db->affected_rows();
    }

    function updateBiayaIzin($data, $id){
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->update($this->tbl_workorder_biaya_izin);
        return $this->db->affected_rows();
    }

    function getBiayaIzin($idWo){
        $this->db->where("workorder_id", $idWo);
        $query = $this->db->get($this->tbl_workorder_biaya_izin);
        return $query->row();
    }

    //Insert into biaya detail
    function createBiayaIzinDetail($data){
        $this->db->insert($this->tbl_workoder_biaya_detail, $data);

        return $this->db->affected_rows();
    }

    function updatebiayadetail($data, $where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($this->tbl_workoder_biaya_detail);
        return $this->db->affected_rows();
    }

    function getBiaya($idWo){
        $this->db->select("
        a.id as id_izin,
        a.approval,
        a.workorder_id,
        b.*,
        c.id as biaya_detail_id,
        c.biaya_id,
        c.nominal,
        c.tgl_selesai as selesai,
        c.tgl_mulai as mulai,
        d.*, 
        e.nama,
        f.nama as layanan
        ");
        $this->db->from($this->tbl_workorder_biaya_izin." as a");
        $this->db->join($this->tbl_workorder." as b", 'b.id = a.workorder_id');
        $this->db->join($this->tbl_workoder_biaya_detail." as c", 'a.id = c.wo_biaya_izin_id', "left");
        $this->db->join($this->tbl_workoder_approval." as d", 'a.workorder_approval_id = d.id', "left");
        $this->db->join($this->tbl_master_biaya." as e", 'c.biaya_id = e.id', "left");
        $this->db->join($this->tbl_master_jenis_minta." as f", 'f.id = b.jenis_perizinan', "left");
        $this->db->where("a.workorder_id", $idWo);
        $sql = $this->db->get();
        return $sql->result();
    }

    function cekWoApproval($idWoIzin){
        $this->db->where("wo_izin_id", $idWoIzin);
        $query = $this->db->get($this->tbl_workoder_approval);
        return $query->num_rows();
    }

    function getDataWoApproval($idWoIzin){
        $this->db->where("wo_izin_id", $idWoIzin);
        $query = $this->db->get($this->tbl_workoder_approval);
        return $query->row();
    }

    function saveToLog($data){
        $this->db->insert($this->tbl_log_update_sewa, $data);

        return $this->db->affected_rows();
    }

    function saveWoApproval($data){
        $this->db->insert($this->tbl_workoder_approval, $data);

        return $this->db->affected_rows();
    }

    function updateWoApproval($data, $where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($this->tbl_workoder_approval);
        return $this->db->affected_rows();
    }

    function updateWorkorder($data,$idWo){
        $this->db->set($data);
        $this->db->where("id", $idWo);
        $this->db->update($this->tbl_workorder);
        return $this->db->affected_rows();
    }

    function getFormPreviewT1($id){
        $this->db->select("a.*, e.nama as layanan, c.nominal as instalasi, d.nominal as sewa");
        $this->db->from($this->tbl_workorder." as a");
        $this->db->join($this->tbl_workorder_biaya_izin." as b", "b.workorder_id=a.id", "inner");
        $this->db->join($this->tbl_workoder_biaya_detail." as c", "c.wo_biaya_izin_id=b.id and c.biaya_id =16", "left" );
        $this->db->join($this->tbl_workoder_biaya_detail." as d", "d.wo_biaya_izin_id=b.id and d.biaya_id =15", "left" );
        $this->db->join($this->tbl_master_jenis_minta." as e", "e.id=a.jenis_perizinan", "left" );
        $this->db->where("a.id", $id);
        $sql = $this->db->get();
        return $sql->row();
    }

    function getFormPreviewBiayaDetail($id){
        $this->db->select(" a.id, c.biaya_id, c.nominal, c.tgl_mulai, c.tgl_selesai, d.nama,");
        $this->db->from($this->tbl_workorder." as a");
        $this->db->join($this->tbl_workorder_biaya_izin." as b", "b.workorder_id=a.id", "inner");
        $this->db->join($this->tbl_workoder_biaya_detail." as c", "c.wo_biaya_izin_id=b.id", "inner");
        $this->db->join($this->tbl_master_biaya." as d", "d.id=c.biaya_id", "inner");
        $this->db->where("a.id", $id);
        $this->db->order_by("c.biaya_id");
        $sql =  $this->db->get();
        return $sql->result();
    }

    function cekDetailBiaya($idBiaya, $idWoBiaya){
        $this->db->where("biaya_id", $idBiaya);
        $this->db->where("wo_biaya_izin_id", $idWoBiaya);
        $query = $this->db->get($this->tbl_workoder_biaya_detail);
        return $query->num_rows();
    }

    // VERSION 2

    function newCreateBiayaIzin($data){
        $this->db->insert($this->tbl_wbi, $data);

        return $this->db->affected_rows();
    }

    function newUpdateBiayaIzin($data, $id){
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->update($this->tbl_wbi);

        return $this->db->affected_rows();
    }

    function newGetBiayaIzin($idWo){
        $this->db->select("
        a.*,
        b.keterangan
        ");
        $this->db->from($this->tbl_wbi." as a");
        $this->db->join($this->tbl_workoder_approval." as b", 'a.id = b.wo_izin_id', "left");
        $this->db->where("a.workorder_id", $idWo);
        $sql = $this->db->get();
        return $sql->result();
    }

    function newCekBiayaIzin($id)
    {
        $this->db->where("workorder_id", $id);
        $query = $this->db->get($this->tbl_wbi);
        return $query->num_rows();
    }


    function newGetFormPreviewT1($id){
        $this->db->select("a.*, b.*, c.nama as layanan");
        $this->db->from($this->tbl_workorder." as a");
        $this->db->join($this->tbl_wbi." as b", "b.workorder_id=a.id", "inner");
        $this->db->join($this->tbl_master_jenis_minta." as c", "c.id=a.jenis_perizinan", "left" );
        $this->db->where("a.id", $id);
        $sql = $this->db->get();
        return $sql->row();
    }


}
