<?php

/**
 * Created by Vendi 3/6/20
 */
class M_Sik extends CI_Model
{

    var $tbl_workorder = 'workorder';
    var $tbl_workorder_penugasan = 'workorder_penugasan';
    var $tbl_workorder_sik = 'workorder_sik';
    var $tbl_master_mitra = 'master_mitra';
    var $tbl_master_status = 'master_status';
    var $tbl_master_pic = 'master_pic';
    var $tbl_kawasan = "master_kawasan";
    var $tbl_appuser = 'appuser';
    var $tbl_jenis_minta = 'master_jenis_minta';
    var $tbl_kecamatan = 'master_kecamatan';
    var $tbl_kabupaten = 'master_kabupaten';
    var $tbl_kelurahan = 'master_desa';
    var $tbl_master_unit = 'master_unit';

    var $column_order = array(
        null,
        'workorder.sales_no',
        'workorder.tgl_sales',
        'workorder.wo_no',
        'workorder.tgl_wo',
        'workorder.status',
        'appuser.name',
        'master_mitra.nama');
    var $column_search = array(null, 'workorder.sales_no', 'workorder.tgl_sales', 'workorder.wo_no', 'workorder.tgl_wo', null, 'appuser.name', 'master_mitra.nama', 'workorder_sik.nama');

    var $column_search_mask = array(null, 'No SO', 'Tanggal SO', 'No WO', 'Tanggal WO', null, 'PIC', 'Mitra', 'File SIK');

    var $order = array('workorder.id' => 'DESC');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    private function _get_datatables_query()
    {
        $this->db->select('
             '.$this->tbl_workorder.'.id,
             '.$this->tbl_workorder.'.sales_no,
             '.$this->tbl_workorder.'.wo_no,
             '.$this->tbl_workorder.'.tgl_sales,
             '.$this->tbl_workorder.'.status as idstatus,
             '.$this->tbl_workorder.'.tgl_wo,
             '.$this->tbl_master_mitra.'.nama as mitra,
             '.$this->tbl_workorder_sik.'.nama as filesku,
             '.$this->tbl_appuser.'.name,
             '.$this->tbl_master_status.'.nama as status
           ');

        $this->db->from($this->tbl_workorder );

        $this->db->join($this->tbl_workorder_penugasan ,
            $this->tbl_workorder.'.id =  '.$this->tbl_workorder_penugasan.'.workorder_id', 'left');

        $this->db->join($this->tbl_master_mitra ,
            $this->tbl_workorder_penugasan.'.mitra_id =  '.$this->tbl_master_mitra.'.id', 'left');

        $this->db->join($this->tbl_workorder_sik ,
            $this->tbl_workorder.'.id =  '.$this->tbl_workorder_sik.'.workorder_id', 'left');

        $this->db->join($this->tbl_appuser ,
            $this->tbl_appuser.'.userid =  '.$this->tbl_workorder_penugasan.'.pic_id', 'left');

        $this->db->join($this->tbl_master_status ,
            $this->tbl_workorder.'.status =  '.$this->tbl_master_status.'.id', 'left');

        $this->db->where( $this->tbl_workorder.'.unit_id', $this->session->userdata('tempUnitId'));
        $this->db->where( $this->tbl_workorder.'.is_active', 1);
        $this->db->where( $this->tbl_workorder.'.status >= 6');
        $this->db->where( $this->tbl_workorder.'.status < 10');
        $this->db->where( $this->tbl_workorder_penugasan.'.active', 1);


        if ($this->input->post('start_date') != null && $this->input->post('end_date') != null){
            $this->db->where($this->column_search[$this->input->post('column')].' >=', convertDateGaring($this->input->post('start_date')));
            $this->db->where($this->column_search[$this->input->post('column')].' <=', convertDateGaring($this->input->post('end_date')));
            $order = $this->order;
            $this->db->order_by(key($order), reset($order));
        }

        $searchOrder = 0;

        if (isset($_POST["columns"])){
            foreach ($this->column_search as $index => $value) {
                if (isset($value) && isset($_POST["columns"][$index]["search"]) &&
                    !empty($_POST["columns"][$index]["search"]["value"])) {
                    if ($searchOrder === 0) {
                        $this->db->group_start();
                        $this->db->like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    } else {
                        $this->db->or_like('LOWER(' . $value . ')', strtolower($_POST["columns"][$index]['search']['value']));
                    }

                    $this->db->order_by(key($this->order), reset($this->order));

                    $searchOrder++;
                }
            }
            if ($searchOrder>0) {
                $this->db->group_end();
            }
        }


        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), reset($order));
        }
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->_get_datatables_query();
        return $this->db->count_all_results();
    }

    function get_datatables()
    {
        $searchOrder = 0;
        $where = "sales_no";

        if ($_POST["columns"][1]['search']['value'] != null || $_POST["columns"][3]['search']['value'] != "") {
            $searchOrder = 1;
            $where = "sales_no";
        } else {
            $where = "sales_no";
        }

        if ($_POST["columns"][3]['search']['value'] != null || $_POST["columns"][3]['search']['value'] != "") {
            $searchOrder = 3;
            $where = "wo_no";
        } else {
            $where = "sales_no";
        }

        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function getWo($id){
        $this->db->where('id', $id);
        $sql=$this->db->get($this->tbl_workorder);

        return $sql->row();
    }

    function getDataSik($idWo){
        $this->db->where('workorder_id', $idWo);
        $sql=$this->db->get($this->tbl_workorder_sik);

        return $sql->result();
    }

    function saveSik($data){
        $this->db->insert($this->tbl_workorder_sik, $data);

        return $this->db->affected_rows();
    }

    function updateWorkorder($data, $id){
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update($this->tbl_workorder);
        return $this->db->affected_rows();
    }

}
