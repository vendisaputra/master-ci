<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by fajar at 1/28/20
 */

/** @var $fcss string is form style */
if (isset($fcss)) {
    echo $fcss;
}
?>

<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header enable-fullscreen">
                    <a class="navbar-brand">Daftar WO Perizinan</a>
                </div>
                <div class="card-body table-responsive table-wrapper">
                    <div class="row float-right ml-2 custome-search" style="margin-right: 14px">
                        <form class="form-inline" id="form-search">
                            <select class="form-control mr-2" name="order" style="width: 150px">
                                <option value="1">NO SO</option>
                                <option value="3">NO WO</option>
                            </select>
                            <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search" aria-label="Search">
                            <div class="input-group input-daterange mr-sm-2" style="display: none;">
                                <input type="text" class="form-control" name="start_date" size="15">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">to</span>
                                </div>
                                <input type="text" class="form-control" name="end_date" size="15">
                            </div>
                            <button class="btn btn-outline-success my-2 my-sm-0 search-data" type="button">Search</button>
                            <button class="btn btn-light ml-2 add-workorder" type="button">Tambah</button>
                        </form>
                    </div>
                    <table id="dt-user" class="table table-bordered nowrap" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Sales Order No</th>
                            <th>Tanggal Sales Order</th>
                            <th>No Wo</th>
                            <th>Tanggal Wo</th>
                            <th>Status</th>
                            <th>Nama PIC</th>
                            <th>Mitra Pelaksana</th>
                            <th>File SKU</th>
                            <th>Dibuat Oleh</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Sales Order No</th>
                            <th>Tanggal Sales Order</th>
                            <th>No Wo</th>
                            <th>Tanggal Wo</th>
                            <th>Status</th>
                            <th>Nama PIC</th>
                            <th>Mitra Pelaksana</th>
                            <th>File SKU</th>
                            <th>Dibuat Oleh</th>
                            <th>Aksi</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>