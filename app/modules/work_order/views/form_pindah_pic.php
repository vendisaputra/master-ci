<?php defined('BASEPATH') OR exit('No direct script access allowed');
echo $css;

?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<link rel="stylesheet" href="https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.css"/>


<div class="content-page">

    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header enable-fullscreen">
                    Work Order - Pergantian PIC Pekerjaan Perijinan

                </div>
                <div class="card-body">
                    <form method="post" id="form_add_workorder">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>No WO Perijinan:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="Nowo" class="form-control" value="Generate System" readonly>
                                    <label for="Nowo" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                                <div class="col-lg-2">
                                    <p>No Sales Order:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="NoSales" value="" class="form-control">
                                    <label for="NoSales" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>Tanggal Perijinan:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="tglPI" data-date-format="dd/mm/yyyy" id="tglPI" value="" class="form-control">
                                    <label for="tglPI" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                                <div class="col-lg-2">
                                    <p>Tangga Sales Order:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="tglSO" data-date-format="dd/mm/yyyy" id="tglSO" class="form-control">
                                    <label for="tglSO" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>Jenis Permintaan</p>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <select type="text" name="jenis_permintaan" class="form-control kawasan">
                                        <option value=''>-- Jenis Permintaan --</option>
                                        <?php foreach ($jenisMinta as $value) {
                                            echo "<option value='".$value->id."'>".$value->nama."</option>";
                                        }
                                        ?>
                                    </select>
                                    <label for="jenis_permintaan" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>Tanggal Permintaan:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="tglPermintaan" data-date-format="dd/mm/yyyy" id="tglPermintaan" class="form-control">
                                    <label for="tglPermintaan" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                                <div class="col-lg-2">
                                    <p>Tanggal Selesai:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="tglSelesai" data-date-format="dd/mm/yyyy" id="tglSelesai" class="form-control">
                                    <label for="tglSelesai" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>Nama Pelanggan:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="Nmplgn" class="form-control">
                                    <label for="Nmplgn" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                                <div class="col-lg-2">
                                    <p>NO IO:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="NoIO" class="form-control">
                                    <label for="NoIO" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>Kawasan:</p>
                                </div>
                                <div class="col-lg-2">
                                    <select type="text" name="kawasan" class="form-control kawasan">
                                        <option value=''>-- Kawasan --</option>
                                        <?php foreach ($kawasan as $value) {
                                            echo "<option value='".$value->id."'>".$value->nama."</option>";
                                        }
                                        ?>
                                    </select>
                                    <label for="kawasan" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                                <div class="col-lg-2">
                                    <p>NO SO:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="noSo" id="noSo" class="form-control">
                                    <label for="noSo" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>Kota:</p>
                                </div>
                                <div class="col-lg-2">
                                    <select type="text" name="kabupaten" class="form-control kabupaten">
                                        <option value=''>-- Kabupaten --</option>
                                        <?php foreach ($kabupaten as $value) {
                                            echo "<option value='".$value->id."'>".$value->nama."</option>";
                                        }
                                        ?>
                                    </select>
                                    <label for="kabupaten" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>Kecamatan:</p>
                                </div>
                                <div class="col-lg-2">
                                    <select type="text" name="kecamatan" class="form-control kecamatan">
                                        <option value=''>-- Kecamatan --</option>
                                    </select>
                                    <label for="kecamatan" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                                <div class="col-lg-2">
                                    <p>Latitude:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="Latitude" class="form-control latitude">
                                    <label for="Latitude" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>Kelurahan:</p>
                                </div>
                                <div class="col-lg-2">
                                    <select type="text" name="kelurahan" class="form-control kelurahan">
                                        <option value=''>-- Kelurahan --</option>
                                    </select>
                                    <label for="kelurahan" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                                <div class="col-lg-2">
                                    <p>Longitude:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" name="Longitude" class="form-control longitude">
                                    <label for="Longitude" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                                <div class="col-lg-2 ">
                                    <input type="image" class="maps" src="<?= base_url(); ?>assets/img/googlemaplogo.png" style="margin-top: -55px;" height="100px" data-toggle="modal"
                                           data-target="#modal-maps">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    Kode Pos
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="kode_pos" class="form-control">
                                    <label for="kode_pos" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    Alamat
                                </div>
                                <div class="col-md-9">
                                    <textarea class="form-control" rows="5" id="alamat" name="alamat"></textarea>
                                    <label for="alamat" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>Nama PIC Lama:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" class="form-control" name="pic_lama" id="" value="PIC LAMA" readonly>
                                </div>
                                <div class="col-lg-2">
                                    <p>Nama PIC Baru:</p>
                                </div>
                                <div class="col-lg-2">
                                    <select type="text" name="nmPIC" class="form-control pic">
                                        <option value=''>-- PIC --</option>
                                        <?php foreach ($pic as $value) {
                                            echo "<option value='".$value->userid."'>".$value->name."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <input type="hidden" name="mitraId" class="mitra form-control">
                                    <input type="hidden" name="generateWo" value="<?=$serialWorkorder;?>" class="form-control">
                                    <input type="hidden" name="id" class="id form-control">
                                    <input type="hidden" class="form-control" placeholder="Nama" name="csrf_izin" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>Mitra Pelaksana:</p>
                                </div>
                                <div class="col-lg-2">
                                    <input type="text" class="form-control" name="mitra" id="" value="PIC LAMA" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    &nbsp;
                                    &nbsp;
                                    <button type="button" class="btn btn-light cancel-btn">Batal </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
