<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by fajar at 1/28/20
 */

/** @var $fcss string is form style */
if (isset($fcss)) {
    echo $fcss;
}
?>

<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header enable-fullscreen">
                    <a class="navbar-brand">History Update Progress</a>
                </div>
                <div class="card-body table-responsive table-wrapper">
                    <div class="row">
                        <div class="col-lg-8">
                            <table id="dt-user" class="table table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No Wo</th>
                                    <th>Tanggal</th>
                                    <th>Progress (%)</th>
                                    <th>Deskripsi</th>
                                    <th>Kendala</th>
                                    <th>File</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>No Wo</th>
                                    <th>Tanggal</th>
                                    <th>Progress (%)</th>
                                    <th>Deskripsi</th>
                                    <th>Kendala</th>
                                    <th>File</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-lg-4" style="margin-top: 70px;">
                            <div class="preview-file border text-center" style="padding: 5px;"><div class="text-center"><img class="img-responsive img-fluid" src="<?=base_url()."/assets/img/no-preview.jpg";?>" alt="" width="300px"></div></div>
                        </div>
                    </div>
                    <div class="col-lg-4" style="margin-top: 10px">
                        <button type="button" class="btn btn-danger kembali">KEMBALI</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>