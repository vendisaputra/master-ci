<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link
        rel="stylesheet"
        href="https://cdn01.boxcdn.net/platform/preview/2.34.0/en-US/preview.css"
/>
<div class="modal fade" id="modal-maps" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-coordinat">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-preview" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="preview-file" style="height: 500px"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<style>
    .doc-preview-file{
        width: 100%;
        height: 100%;
    }
    .img-preview-file{
        width: 100%;
        height: 100%;
    }
</style>