<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($fcss)) {
    echo $fcss;
}
?>
<style>
    .spinner input {
        text-align: right;
    }

    .input-group-btn-vertical {
        position: relative;
        white-space: nowrap;
        width: 5%;
        vertical-align: middle;
        display: table-cell;
    }

    .input-group-btn-vertical > .btn {
        display: block;
        float: none;
        width: 100%;
        max-width: 100%;
        padding: 9px;
        margin-left: -1px;
        position: relative;
        border-radius: 0;
    }

    .input-group-btn-vertical > .btn:first-child {
        border-top-right-radius: 4px;
    }

    .input-group-btn-vertical > .btn:last-child {
        margin-top: -2px;
        border-bottom-right-radius: 4px;
    }

    .input-group-btn-vertical i {
        position: absolute;
        top: 0;
        left: 4px;
    }
</style>
<div class="content-page">

    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <div class="card-header enable-fullscreen">
                    Form - Update Progress Pekerjaan Perizinan
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" id="form-sku">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>No WO Perizinan:</p>
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" name="NoWoPerizinan" class="form-control" value="<?=$data->wo_no != null ? $data->wo_no : "";?>" readonly>
                                    <input type="hidden" name="woId" class="form-control">
                                    <input type="hidden" class="form-control" placeholder="Nama" name="csrf_izin" value="" />
                                    <label for="NoSku" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>Tanggal:</p>
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" name="tgl" class="form-control tgl" data-date-format="dd/mm/yyyy" value="">
                                    <label for="tgl" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>Progress (%):</p>
                                </div>
                                <div class="col-sm-1">
                                    <div class="input-group spinner">
                                        <input type="text" class="form-control" name="progress" value="<?=$data->progress != null ? $data->progress : "1";?>" min="0" max="100">
                                        <div class="input-group-btn-vertical">
                                            <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
                                            <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>Deskripsi Pekerjaan:</p>
                                </div>
                                <div class="col-lg-5">
                                    <textarea class="form-control" rows="5" name="deskripsi" id="deskripsi"></textarea>
                                    <label for="tujuan" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <p> Kendala : </p>
                                </div>
                                <div class="col-lg-5">
                                    <textarea class="form-control" rows="5" name="kendala" id="deskripsi"></textarea>
                                    <label for="deskripsi" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <p> Evidence : </p>
                                </div>
                                <div class="custom-file col-lg-3">
                                    <input type="file" name="evidence" onclick="upload()" class="custom-file-input" id="inputGroupFile01"
                                           aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label ml-3" for="inputGroupFile01">Choose file</label>
                                    <span>( pdf, jpg, png. Masksimal 5 Mb )</span>
                                    <p id="error1" style="display:none; color:#FF0000; margin-top: 5px;">
                                        Format tidak sesuai ! Unggah file Dokumen atau Gambar.
                                    </p>
                                    <p id="error2" style="display:none; color:#FF0000; margin-top: 5px;">
                                        Maksimal ukuran file 5MB.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                </div>
                                <div class="col-lg-3">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 button-action">
                                    <button type="submit" class="btn btn-success">Simpan </button>
                                    &nbsp;
                                    <button type="button" class="btn btn-light cancel-btn">Batal </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- container -->

        </div> <!-- content -->

    </div>
