<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by vendi at 3/6/20
 */

/** @var $fcss string is form style */
if (isset($fcss)) {
    echo $fcss;
}
?>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header enable-fullscreen">
                    Form Upload SIK
                </div>
                <div class="card-body">
                    <form role="form" enctype="multipart/form-data" id="formSIK">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p>No WO Perijinan:</p>
                                </div>
                                <div class="col-md-2">
                                    <input type="hidden" name="id" class="form-control" readonly>
                                    <input type="hidden" name="csrf_izin" class="form-control" value="" id="">
                                    <input type="text" name="Nowo" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-lg-2">
                                    <p>Tanggal:</p>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="tglPI" data-date-format="dd/mm/yyyy" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-lg-2">
                                    <p>No Surat Ijin Kerja:</p>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="NoSKI" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-lg-2">
                                    <p>Tanggal:</p>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="tglSKI" data-date-format="dd/mm/yyyy" class="form-control" id="tglSKI">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-lg-2">
                                    <p>Tanggal Mulai Ijin:</p>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="tglMulai" data-date-format="dd/mm/yyyy" class="form-control" id="tglMulai">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-lg-2">
                                    <p>Tanggal Selesai Ijin:</p>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="tglSelesaiSIK" data-date-format="dd/mm/yyyy" class="form-control" id="tglSelesaiSIK">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-lg-2">
                                    <p>Dikeluarkan Oleh:</p>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="dikeluarkanOleh" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-lg-2">
                                    <p>Deskripsi:</p>
                                </div>
                                <div class="col-md-5">
                                    <textarea name="deskripsi" class="form-control" id="" cols="" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row upload-sik form-group">
                            <div class="col-lg-2">
                                <p>Upload File SIK:</p>
                            </div>
                            <div class="custom-file col-lg-3" style="padding-bottom: 5px;">
                                <input type="file" name="filesik" onclick="upload()" class="custom-file-input" id="inputGroupFile01"
                                       aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label ml-3" for="inputGroupFile01">Choose file</label>
                                <span>( pdf, jpg, png. Masksimal 5 Mb )</span>
                                <label id="error1" style="display:none; color:#ff0000; margin-top: 5px;">
                                    Format tidak sesuai ! Unggah file Dokumen atau Gambar.
                                </label>
                                <label id="error2" style="display:none; color:#FF0000; margin-top: 5px;">
                                    Maksimal ukuran file 5MB.
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg">
                                    <button type="submit" class="btn btn-success btn-simpan">Simpan</button>
                                    <button type="button" class="btn btn-light cancel-btn" style="margin-left: 10px;">Batal </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>