<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by fajar at 1/28/20
 */

/** @var $tjs string is table script */
if (isset($tjs)) {
    echo $tjs;
}

/** @var $fjs string is form script */
if (isset($fjs)) {
    echo $fjs;
}

/** @var $columnSearch array valid column to be search */
/** @var $columnSearchMask array valid column to be search */
?>
<script src="https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.js"></script>
<script lang="js">

    let Pindahpic = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        let $endPath = window.location.pathname.split("/");
        const $tbl = $("#dt-user");
        const $tblbd = $("#dt-user tbody");
        const $base = "<?php echo base_url()?>work_order/pindah_pic";
        const $url = "<?php echo base_url()?>work_order/pindah_pic/getdata";
        const $urledit = "<?php echo base_url()?>work_order/pindah_pic/edit";
        let $column_search = <?php echo json_encode($columnSearch); ?>;
        let $column_search_mask = <?php echo json_encode($columnSearchMask); ?>;

        initial = function (start_date=null, end_date=null, column = null) {
            $("select[name=order] option").remove();

            $("select[name=order]").append($('<option>', {
                value: "",
                text: "-- Pilih --"
            }));


            $column_search.forEach(function (value, index) {

                if (value != null){

                    $("select[name=order]").append($('<option>', {
                        value: index,
                        text: $column_search_mask[index]
                    }));
                }
            });

            $("select[name=order]").append($('<option>', {
                value: "reset",
                text: "-- Reset filter --"
            }));

            $tbl.DataTable({
                'iDisplayLength': 10,
                "responsive": true,
                "processing": true,
                "serverSide": true,
                ajax: {
                    url: $url,
                    type: "post",
                    data: function (d) {
                        d.csrf_izin = $token;
                        d.start_date = start_date;
                        d.end_date = end_date;
                        d.column = column;
                    },
                    dataSrc: function (dt) {
                        $("input[name='csrf_izin']").val($token);

                        return dt.data;
                    }
                },
                buttons: [{
                    text: '<i class="fal fa-user-plus mr-1"></i> Add User',
                    name: 'add',
                    className: 'btn-primary btn-md mr-1 mt-n1 btnadduser'
                }],
                dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'l><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'Bf>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                columns: [{
                    data: 0,
                    "orderable": false,
                    render: $.fn.dataTable.render.text(),
                },{
                    data: 1,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 2,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 3,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 4,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 5,
                    "orderable": true,
                    render: function (x, y, z) {
                        let sts = null, ket = null;
                        if (z[9] == 10) {
                            sts = 'badge-success';
                            ket = 'Selesai';
                            return '<label class="badge '+sts+'">'+ket+'</label>';
                        } else if (z[9] < 2) {
                            sts = 'badge-dark';
                            ket = 'Workorder Baru';
                            return '<label class="badge '+sts+'">'+ket+'</label>';
                        }else if (z[9] > 1 && z[9] <10){
                            sts = 'badge-warning';
                            ket = 'Proses';
                            return '<label class="badge '+sts+'">'+ket+' '+ x +'</label>';
                        };
                    }
                }, {
                    data: 6,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 7,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 9,
                    "orderable": false,
                    'class': 'text-center',
                    render:function (x, y, z) {
                        return '<a href="' + $urledit + '/' + z[10] + '" class="btn btn-sm btn-icon btn-outline-primary rounded-circle mr-1 editWo" title="Edit Workorder">' +
                            '<i class="fa fa-user"></i></a>';

                    }
                }],
                "rowCallback": function (row, data, iDisplayIndex) {
                    var pg = $tbl.DataTable().page.info();
                    var index = iDisplayIndex + 1;
                    $('td:eq(0)', row).html(index + pg.start);
                    return row;
                }
            });
        };

        searchdata = function () {
            var order = $("select[name=order]");
            order.on('change', function() {
                $("select[name=order] option[value='']").remove();

                if (order.find(":selected").text().toLowerCase().match(/tanggal/g)){
                    $(".search-data").unbind( "click" );
                    $(".input-daterange").css('display', '');
                    $("input[name=search]").css('display', 'none');

                    [".input-daterange input"]
                        .forEach(function (key) {
                            $(key).datepicker({dateFormat: 'dd/mm/yy'});

                            if (!$(key).val()) {
                                $(key).datepicker("setDate", new Date());
                            }
                        });

                    $(".search-data").on("click", function () {

                        $tbl.DataTable().clear().destroy();

                        var order = $("select[name=order]").val();

                        initial($("input[name=start_date]").val(), $("input[name=end_date]").val(), $("select[name=order]").val());

                        $("select[name=order]").val(order);

                        $(".dataTables_filter").css("display", "none");

                        $("select[name=order] option[value='']").remove();

                    });

                }else{
                    if (order.find(":selected").text().toLowerCase().match(/reset/g)){
                        $tbl.DataTable().clear().destroy();

                        initial();

                        $("select[name=order]").val(selected);

                        $(".dataTables_filter").css("display", "none");

                        $("input[name=search]").css('display', '').val('');

                        $(".input-daterange").css('display', 'none');


                    }else {
                        var selected = order.find(":selected").val();

                        $(".search-data").unbind("click");

                        $(".input-daterange").css('display', 'none');

                        $("input[name=search]").css('display', '');

                        $(".search-data").on("click", function () {
                            $tbl.DataTable().clear().destroy();

                            initial();

                            $(".dataTables_filter").css("display", "none");

                            $tbl.DataTable()
                                .columns(selected)
                                .search($("input[name=search]").val()).draw();

                            $("select[name=order]").val(selected);

                            $("select[name=order] option[value='']").remove();

                        });
                    }
                }
            });
        };

        return {
            init: function () {
                initial(), searchdata();
            }
        }
    }();

    $(document).ready(function () {
        Pindahpic.init();
        $(".dataTables_filter").css("display", "none");
    });
</script>