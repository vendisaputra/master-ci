<?php
if (isset($tjs)) {
    echo $tjs;
}
if (isset($fjs)) {
    echo $fjs;
}
?>
<script>

    let UpdateProgressForm = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        let $endPath = window.location.pathname.split("/");
        const $base = "<?php echo base_url()?>work_order/update_progress";
        const $urlsaveprogress = "<?php echo base_url()?>work_order/update_progress/saveProgress";

        initial = function(){
          $("input[name=woId]").val($endPath.pop());
            $("input[name=csrf_izin]").val($token);
        };

        spinnerInput = function(){
            $('.spinner .btn:first-of-type').on('click', function() {
                var btn = $(this);
                var input = btn.closest('.spinner').find('input');
                if (input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
                    input.val(parseInt(input.val(), 10) + 1);
                } else {
                    btn.next("disabled", true);
                }
            });
            $('.spinner .btn:last-of-type').on('click', function() {
                var btn = $(this);
                var input = btn.closest('.spinner').find('input');
                if (input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
                    input.val(parseInt(input.val(), 10) - 1);
                } else {
                    btn.prev("disabled", true);
                }
            });
        };

        upload = function() {

            $('input[type="submit"]').prop("disabled", true);

            var a=0;

            $('input[name=evidence]').bind('change', function() {
                if ($('input:submit').attr('disabled',false)){
                    $('input:submit').attr('disabled',true);
                }

                var ext = $('input[name=evidence]').val().split('.').pop().toLowerCase();

                if ($.inArray(ext, ['gif','png','jpg','jpeg', 'pdf']) == -1){

                    $('#error1').slideDown("slow");
                    $('#error2').slideUp("slow");

                    a=0;
                }else{
                    var picsize = (this.files[0].size);

                    if (picsize > 5000000){
                        $('#error2').slideDown("slow");

                        a=0;
                    }else{
                        a=1;

                        $('#error2').slideUp("slow");
                    }
                    $('#error1').slideUp("slow");

                    if (a==1){
                        $('input:submit').attr('disabled',false);
                    }
                }
            });
        };

        saveProgress = function () {
            let forms=$("#form-sku");
            forms.submit(function(e) {
                e.preventDefault();
            }).validate({
                rules: {
                    NoSku:{
                        required: true,
                        minlength: 2
                    },
                    tgl:{
                        required: true,
                        minlength: 2
                    },
                    tujuan:{
                        required: true,
                        minlength: 2
                    },
                    deskripsi:{
                        required: true,
                    },
                    filesku:{
                        required: true
                    }
                },
                submitHandler: function(forms) {
                    $.ajax({
                        url:$urlsaveprogress,
                        type:"post",
                        data:new FormData(forms),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(result) {
                            result = $.parseJSON(result);
                            if(result.result == 'true'){
                                swalSuccess(result.message, function (x) {
                                    if (x){
                                        document.location.href= $base;
                                    }
                                });
                            }else{
                                swalAlert(result.message);
                            }

                        }
                    });
                }
            });
        };

        cancelBtn = function () {
            $(".cancel-btn").on("click",function(e){
                e.preventDefault();
                if(e.handler != true){
                    document.location.href= $base;
                    e.handler = true;
                }
            });
        };

        return {
            init: function () {
                initial(), saveProgress(), upload(), spinnerInput(), cancelBtn();
            }
        }
    }();



    $(document).ready(function () {

        bsCustomFileInput.init();
        UpdateProgressForm.init();


    });

</script>

<script type="text/javascript">

    $(document).ready(function () {
        [".tgl"]
            .forEach(function (key) {
                $(key).datepicker({dateFormat: 'dd/mm/yy'});

                if (!$(key).val()) {
                    $(key).datepicker("setDate", new Date());
                }
            });

    });



</script>
</script>
