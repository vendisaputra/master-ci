<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($tjs)) {
    echo $tjs;
}
if (isset($fjs)) {
    echo $fjs;
}

/** @var $columnSearch array valid column to be search */
/** @var $columnSearchMask array valid column to be search */
?>

<script>
    let updateProgressList = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        let $userid = "<?php echo $this->session->userdata('tempId'); ?>";
        const $tbl = $("#dt-user");
        const $tblbd = $("#dt-user tbody");
        const $url = "<?php echo base_url()?>work_order/update_progress/getdata";
        const $urladdworkorder = "<?php echo base_url()?>work_order/add";
        const $urlgetsku = "<?php echo base_url()?>work_order/getsku";
        const $urledit = "<?php echo base_url()?>work_order/update_progress/addProgress";
        const $urldetail = "<?php echo base_url()?>work_order/update_progress/detailProgress";
        let $column_search = <?php echo json_encode($columnSearch); ?>;
        let $column_search_mask = <?php echo json_encode($columnSearchMask); ?>;

        initial = function (start_date=null, end_date=null, column = null) {
            $("select[name=order] option").remove();

            $("select[name=order]").append($('<option>', {
                value: "",
                text: "-- Pilih --"
            }));


            $column_search.forEach(function (value, index) {

                if (value != null){

                    $("select[name=order]").append($('<option>', {
                        value: index,
                        text: $column_search_mask[index]
                    }));
                }
            });

            $("select[name=order]").append($('<option>', {
                value: "reset",
                text: "-- Reset filter --"
            }));

            $tbl.DataTable({
                'iDisplayLength': 10,
                "responsive": true,
                "processing": true,
                "serverSide": true,
                ajax: {
                    url: $url,
                    type: "post",
                    data: function (d) {
                        d.csrf_izin = $token;
                        d.start_date = start_date;
                        d.end_date = end_date;
                        d.column = column;
                    },
                    dataSrc: function (dt) {
                        $("input[name='csrf_izin']").val($token);

                        return dt.data;
                    }
                },
                buttons: [{
                    text: '<i class="fal fa-user-plus mr-1"></i> Add User',
                    name: 'add',
                    className: 'btn-primary btn-md mr-1 mt-n1 btnadduser'
                }],
                dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'l><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'Bf>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                columns: [{
                    data: 0,
                    "orderable": false,
                    render: $.fn.dataTable.render.text(),
                },{
                    data: 1,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 2,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 3,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 4,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 5,
                    "orderable": true,
                    render: function (x, y, z) {
                        let sts = null, ket = null;
                        if (z[9] == 10) {
                            sts = 'badge-success';
                            ket = 'Selesai';
                            return '<label class="badge '+sts+'">'+ ket +'</label>';
                        } else if (z[9] < 2) {
                            sts = 'badge-dark';
                            ket = 'Workorder Baru';
                            return '<label class="badge '+sts+'">'+ ket +'</label>';
                        }else if (z[9] > 1 && z[9] <10){
                            sts = 'badge-warning';
                            ket = 'Proses';
                            return '<label class="badge '+sts+'">'+ket + ' ' + x +'</label>';
                        };

                    }
                }, {
                    data: 6,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 7,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 8,
                    "orderable": false,
                    'class': 'text-center',
                    render:function (x, y, z) {
                        if(z[11] == z[13]){
                            return '<a href="' + $urledit + '/' + z[10] + '" class="btn btn-sm btn-icon btn-outline-primary rounded-circle mr-1 editWo" title="Update Progress">' +
                                    '<i class="fa fa-pencil-square-o"></i></a><a href="' + $urldetail + '/' + z[10] + '" class="btn btn-sm btn-icon btn-outline-primary rounded-circle mr-1 editWo" title="Detail Progress">' +
                                '<i class="fa fa-search"></i></a>';
                        }else{
                            return '<a href="' + $urldetail + '/' + z[10] + '" class="btn btn-sm btn-icon btn-outline-primary rounded-circle mr-1 editWo" title="Detail Progress">' +
                                '<i class="fa fa-search"></i></a>';
                        }

                    }
                }],
                "rowCallback": function (row, data, iDisplayIndex) {
                    var pg = $tbl.DataTable().page.info();
                    var index = iDisplayIndex + 1;
                    $('td:eq(0)', row).html(index + pg.start);
                    return row;
                }
            });
        };

        addWo = function(){
            $(".add-workorder").on("click",function(){
                document.location.href= $urladdworkorder;
            });
        };

        preview = function ($id) {
            $.post($urlgetsku, {
                idWo: $id,
                csrf_izin: $token
            }, function (xy) {
                xy = $.parseJSON(xy);
                if (xy.file.type == "application/pdf" || xy.file.type == "image/jpeg" || xy.file.type == "image/png"){
                    createPreviewFile([{
                        type: xy.file.type,
                        path: xy.file.path
                    }], ".preview-file", false);
                }else{
                    $("#modal-preview .modal-body").html('<div class="text-center"><img class="img-responsive img-fluid" src="<?=base_url()."/assets/img/no-preview.jpg";?>" alt="" width="300px"></div>');
                    $("#modal-preview .modal-footer").append('<a href="'+xy.file.path+'" class="btn btn-primary">Download</a>');
                }


            });

        };

        searchdata = function () {
            var order = $("select[name=order]");
            order.on('change', function() {
                $("select[name=order] option[value='']").remove();

                if (order.find(":selected").text().toLowerCase().match(/tanggal/g)){
                    $(".search-data").unbind( "click" );
                    $(".input-daterange").css('display', '');
                    $("input[name=search]").css('display', 'none');

                    [".input-daterange input"]
                        .forEach(function (key) {
                            $(key).datepicker({dateFormat: 'dd/mm/yy'});

                            if (!$(key).val()) {
                                $(key).datepicker("setDate", new Date());
                            }
                        });

                    $(".search-data").on("click", function () {

                        $tbl.DataTable().clear().destroy();

                        var order = $("select[name=order]").val();

                        initial($("input[name=start_date]").val(), $("input[name=end_date]").val(), $("select[name=order]").val());

                        $("select[name=order]").val(order);

                        $(".dataTables_filter").css("display", "none");

                        $("select[name=order] option[value='']").remove();

                    });

                }else{
                    if (order.find(":selected").text().toLowerCase().match(/reset/g)){
                        $tbl.DataTable().clear().destroy();

                        initial();

                        $("select[name=order]").val(selected);

                        $(".dataTables_filter").css("display", "none");

                        $("input[name=search]").css('display', '').val('');

                        $(".input-daterange").css('display', 'none');


                    }else {
                        var selected = order.find(":selected").val();

                        $(".search-data").unbind("click");

                        $(".input-daterange").css('display', 'none');

                        $("input[name=search]").css('display', '');

                        $(".search-data").on("click", function () {
                            $tbl.DataTable().clear().destroy();

                            initial();

                            $(".dataTables_filter").css("display", "none");

                            $tbl.DataTable()
                                .columns(selected)
                                .search($("input[name=search]").val()).draw();

                            $("select[name=order]").val(selected);

                            $("select[name=order] option[value='']").remove();

                        });
                    }
                }
            });
        };


        return {
            init: function () {
                initial(), addWo(), searchdata();
            }
        }
    }();



    $(document).ready(function () {

        bsCustomFileInput.init();
        updateProgressList.init();
        $(".dataTables_filter").css("display", "none");


    });

</script>