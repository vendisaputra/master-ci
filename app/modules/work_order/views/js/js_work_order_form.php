<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($tjs)) {
    echo $tjs;
}
if (isset($fjs)) {
    echo $fjs;
}
?>
<script src="https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.js"></script>

<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>

<script src="https://cdn01.boxcdn.net/platform/preview/2.34.0/en-US/preview.js"></script>
<script>
    let WorkorderForm = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        let $idlogin = "<?php echo $this->session->userdata('tempId'); ?>";
        let $endPath = window.location.pathname.split("/");
        let $defaultLat, $defaultLng;
        const $base = "<?php echo base_url()?>work_order";
        const $urlkecamatan = "<?php echo base_url()?>work_order/getKecamatan";
        const $urlkelurahan = "<?php echo base_url()?>work_order/getKelurahan";
        const $urlkawasan = "<?php echo base_url()?>work_order/getKawasan";
        const $urlsave = "<?php echo base_url()?>work_order/save";
        const $urlgetdata = "<?php echo base_url()?>work_order/getDataEdit";
        let $status;


        initial = function () {
            $("input[name='csrf_izin']").val($token);
            if ($endPath[$endPath.length-2]=="edit") {
                $.post($urlgetdata, {
                    idWo: $endPath[$endPath.length-1],
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);
                    $token = xy.token;

                    localStorage.setItem("kota", xy.result.kota);
                    localStorage.setItem("kecamatan", xy.result.kecamatan);
                    localStorage.setItem("pic", xy.result.id_pic);


                    var callbackplace = function () {
                        $(".kecamatan").val(xy.result.kecamatan);
                        $(".kelurahan").val(xy.result.kelurahan);
                        $(".pic").val(xy.result.id_pic);
                        $("select[name=mitra]").val(xy.result.idMitra);
                    };
                    getkecamatan(callbackplace);
                    getkelurahan(callbackplace);

                    $("input[name='id']").val(xy.result.id);
                    $("input[name='Nowo']").val(xy.result.wo_no);
                    $("input[name='NoSales']").val(xy.result.sales_no);
                    $("input[name='tglPI']").val(formateDate(xy.result.tgl_wo));
                    $("input[name='tglSO']").val(formateDate(xy.result.tgl_sales));
                    $("input[name='tglPermintaan']").val(formateDate(xy.result.tgl_mulai));
                    $("input[name='tglSelesai']").val(formateDate(xy.result.tgl_selesai));
                    $("input[name='Nmplgn']").val(xy.result.nama_pelanggan);
                    $("input[name='NoIO']").val(xy.result.no_io);
                    $("input[name='generateWo']").val(xy.result.wo_no);
                    $("input[name='Latitude']").val(xy.result.latitude);
                    $("input[name='Longitude']").val(xy.result.longitude);
                    $("input[name='kode_pos']").val(xy.result.kodepos);
                    $("textarea[name='alamat']").val(xy.result.alamat);
                    $("select[name='kawasan']").val(xy.result.kawasan);
                    $(".kabupaten").val(xy.result.kota);

                    $("select[name=jenis_permintaan]").val(xy.result.jenis_perizinan);

                    $('input[name=status]').val(xy.result.status);

                    if (xy.result.status > 1){
                        $('.pic').prop('disabled', true);
                        $('select[name=mitra]').prop('disabled', true);
                    }
                    if(xy.result.status > 3 || xy.result.created_by != $idlogin){
                        $('#form_add_workorder input, select, textarea').prop('disabled', true);
                        $('#form_add_workorder button[type=submit]').prop('disabled', true);
                        $('#form_add_workorder button[type=submit]').remove();
                        $('#form_add_workorder button[type=button]').html("Kembali");
                        $('#form_add_workorder button[type=button]').removeClass("btn-light");
                        $('#form_add_workorder button[type=button]').addClass("btn-danger");

                    }
                });
            }
        };

        getkecamatan = function (callback) {
            var kabupaten = "";
            if (callback) {
                $.post($urlkecamatan, {
                    kabupatenId: localStorage.getItem("kota"),
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);

                    $("select.kecamatan").empty();
                    $("select.kecamatan").append($('<option>', {
                        value: "",
                        text: "-- Kecamatan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kecamatan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                    callback();
                });
            }
            $("select.kabupaten").change(function () {
                kabupaten = $(this).children("option:selected").val();
                $.post($urlkecamatan, {
                    kabupatenId: kabupaten,
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);
                    $("select.kecamatan").empty();
                    $("select.kecamatan").append($('<option>', {
                        value: "",
                        text: "-- Kecamatan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kecamatan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                });
            });
        };

        integerInput = function(){
            (function($) {
                $.fn.inputFilter = function(inputFilter) {
                    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                        if (inputFilter(this.value)) {
                            this.oldValue = this.value;
                            this.oldSelectionStart = this.selectionStart;
                            this.oldSelectionEnd = this.selectionEnd;
                        } else if (this.hasOwnProperty("oldValue")) {
                            this.value = this.oldValue;
                            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                        } else {
                            this.value = "";
                        }
                    });
                };
            }(jQuery));


            $("input[name=kode_pos]").inputFilter(function(value) {
                return /^-?\d*$/.test(value); });
        };

        getkelurahan = function (callback) {
            var kecamatan = "";
            if (callback) {
                $.post($urlkelurahan, {
                    kecamatanId: localStorage.getItem("kecamatan"),
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);

                    $("select.kelurahan").empty();
                    $("select.kelurahan").append($('<option>', {
                        value: "",
                        text: "-- Kelurahan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kelurahan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                    callback();
                });
            }
            $("select.kecamatan").change(function () {
                kecamatan = $(this).children("option:selected").val();
                $.post($urlkelurahan, {
                    kecamatanId: kecamatan,
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);
                    $("select.kelurahan").empty();
                    $("select.kelurahan").append($('<option>', {
                        value: "",
                        text: "-- Kelurahan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kelurahan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                });
            });
        };

        save = function () {
            let form=$("#form_add_workorder");

            form.submit(function(e) {
                e.preventDefault();

                if ( $("select[name=mitra]").val() != "" && $("select[name=nmPIC]").val() == ""){
                    $("select[name=nmPIC]").prop('required',true);
                    return false;
                }

                if ($("select[name=nmPIC]").val() != "" && $("select[name=mitra]").val() == ""){
                    $("select[name=mitra]").prop('required',true);
                    return false;
                }

            }).validate({
                errorClass: "text-danger",
                rules: {
                    NoSales:{
                        required: true,
                    },
                    tglPI:{
                        required: true,
                    },
                    tglSO:{
                        required: true,
                    },
                    jenis_permintaan:{
                        required: true,
                    },
                    tglPermintaan:{
                        required: true,
                    },
                    tglSelesai:{
                        required: true,
                    },
                    Nmplgn:{
                        required: true,
                    },
                    NoIO:{
                        required: true,
                    },
                    kawasan:{
                        required: true,
                    },
                    kabupaten:{
                        required: true,
                    },
                    kecamatan:{
                        required: true,
                    },
                    Latitude:{
                        required: true,},
                    kelurahan:{
                        required: true,
                    },
                    Longitude:{
                        required: true,
                    },
                    alamat:{
                        required: true,
                    },
                },
                errorPlacement: function(error,element) {
                  return true;
                },
                submitHandler: function(form) {
                    $.ajax({
                        url: $urlsave,
                        type: 'POST',
                        data: $(form).serialize(),
                        dataType: 'json',
                        success: function(result) {
                            if(result.result == 'true'){
                                swalSuccess(result.message, function (x) {
                                    if (x){
                                        document.location.href= $base;
                                    }
                                });
                            }else{
                                swalAlert(result.message);
                            }
                        }
                    });
                }
            });
        };

        getmaps = function(){
            $(".maps").on("click",function(e){
                e.preventDefault();
                if(e.handler != true){
                    $("#modal-maps").modal("show");
                    e.handler = true;
                }
            });
        };

        cancelBtn = function () {
            $(".cancel-btn").on("click",function(e){
                e.preventDefault();
                if(e.handler != true){
                    document.location.href= $base;
                    e.handler = true;
                }
            });
        };

        formateDate = function($date) {
            var d = new Date($date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [day, month, year].join('/');
        };

        openmap = function(){
            $('#modal-maps').on('shown.bs.modal', function (e) {
                if ($('select[name=kawasan]').children("option:selected").val() != ""){
                    $.post($urlkawasan, {
                        id: $('select[name=kawasan]').children("option:selected").val(),
                        csrf_izin: $token
                    }, function (xy) {

                        xy = $.parseJSON(xy);

                        if ($("input[name=Latitude]").val() != "" && $("input[name=Longitude]").val() != null){
                            mapform($("input[name=Latitude]").val(), $("input[name=Longitude]").val(), true);
                        }else {

                            if (xy.result.latitude != null && xy.result.longitude != null) {
                                mapform(xy.result.latitude, xy.result.longitude);
                            } else {
                                navigator.geolocation.getCurrentPosition(function (position) {
                                    mapform(position.coords.latitude, position.coords.longitude);
                                }, function (error) {
                                    if (error.code == error.PERMISSION_DENIED) {
                                        mapform(-6.2472172, 106.8176994);
                                    } else {
                                        mapform(-6.2472172, 106.8176994);
                                    }
                                });
                            }
                        }

                    });
                }else{
                    navigator.geolocation.getCurrentPosition(function(position){
                        mapform(position.coords.latitude, position.coords.longitude);
                    }, function(error){
                        if (error.code == error.PERMISSION_DENIED) {
                            mapform(-6.2472172, 106.8176994);
                        }else{
                            mapform(-6.2472172, 106.8176994);
                        }
                    });
                }
            });
        };


        mapform = function ($lat, $lng, $hasLocation = false){
            $("#modal-maps .modal-body").html('<div id="osm-map" style="width: 100%; height: 500px;"></div>');
            var lat, long;
            var osmUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
                osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> coffee',
                osm = L.tileLayer(osmUrl, {
                    maxZoom: 18,
                    attribution: osmAttrib
                });

            var map = L.map('osm-map').setView([$lat, $lng], 12).addLayer(osm);

            var marker = null;      // create initial marker here(for edit if needed)

            if ($hasLocation) {
                marker = L.marker([$lat, $lng], {
                    draggable: true,
                    title: "Resource location",
                    alt: "Resource Location",
                    riseOnHover: true
                }).addTo(map).bindPopup([$lat, $lng].toString()).openPopup();
            } else {
                marker = null;
            }

            function onMapClick(e) {
                if (marker != null) {
                    map.removeLayer(marker);
                }
                marker = L.marker(e.latlng, {
                    draggable: true,
                    title: "Resource location",
                    alt: "Resource Location",
                    riseOnHover: true
                }).addTo(map).bindPopup(e.latlng.toString()).openPopup();

                $defaultLat = e.latlng.lat;
                $defaultLng = e.latlng.lng;

                marker.on("dragend", function (ev) {
                    var chagedPos = ev.target.getLatLng();
                    this.bindPopup(chagedPos.toString()).openPopup();
                    document.getElementById('coordinat').value = ev.target.getLatLng().lat + ',' + ev.target.getLatLng().lng;

                });
            }
            map.on('click', onMapClick);
        };

        savelocation = function () {
            $(".save-coordinat").on("click",function(e){
                e.preventDefault();
                if(e.handler != true){
                    $(".longitude").val($defaultLng);
                    $(".latitude").val($defaultLat);
                    $("#modal-maps").modal("hide");
                    e.handler = true;
                }
            });
        };

        return {
            init: function () {
                initial(), getkecamatan(), getkelurahan(), openmap(), save(), getmaps(), cancelBtn(), savelocation(), integerInput();
            }
        }
    }();



    $(document).ready(function () {

        bsCustomFileInput.init();
        WorkorderForm.init();

        ["#tglPermintaan, #tglPI, #tglSO, #tglPermintaan, #tglSelesai, .tgl"]
            .forEach(function (key) {
                $(key).datepicker({dateFormat: 'dd/mm/yy', minDate:new Date()});

                if (!$(key).val()) {
                    $(key).datepicker("setDate", new Date());
                }
            });
    });

</script>
