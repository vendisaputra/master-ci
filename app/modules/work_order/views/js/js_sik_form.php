<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($tjs)) {
    echo $tjs;
}
if (isset($fjs)) {
    echo $fjs;
}
?>
<script>
    let sikForm = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        const $base = "<?php echo base_url()?>work_order/sik";
        const $baseurl = "<?php echo base_url()?>";
        const $urlgetsik = "<?php echo base_url()?>work_order/sik/getsik";
        const $urlsave = "<?php echo base_url()?>work_order/sik/save";
        const $urlgetdata = "<?php echo base_url()?>work_order/sik/getwo";
        let $endPath = window.location.pathname.split("/").pop();

        initial = function () {
            $("input[name='csrf_izin']").val($token);

            ["#tglSKI, #tglMulai, #tglSelesaiSIK"]
                .forEach(function (key) {
                    $(key).datepicker({dateFormat: 'dd/mm/yy'});

                    if (!$(key).val()) {
                        $(key).datepicker("setDate", new Date());
                    }
                });

            $.post($urlgetdata, {
                idWo: $endPath,
                csrf_izin: $token
            }, function (xy) {
                xy = $.parseJSON(xy);

                $("input[name='id']").val(xy.data.id);
                $("input[name='Nowo']").val(xy.data.wo_no);
                $("input[name='tglPI']").val(formateDate(xy.data.tgl_wo));

                if (xy.data.status == 9){
                    dataSIK(xy.data.id);

                    $('.btn-simpan').remove();

                    $('.cancel-btn').html('Kembali');
                    $('.cancel-btn').css('margin-left', '0px');
                    $('.cancel-btn').removeClass('btn-light');
                    $('.cancel-btn').addClass('btn-danger');

                }
            });
        };

        upload = function() {

            $('input[type="submit"]').prop("disabled", true);

            var a=0;

            $('input[name=filesik]').bind('change', function() {
                if ($('input:submit').attr('disabled',false)){
                    $('input:submit').attr('disabled',true);
                }

                var ext = $('input[name=filesik]').val().split('.').pop().toLowerCase();

                if ($.inArray(ext, ['gif','png','jpg','jpeg', 'doc', 'docx', 'xls', 'xlsx', 'pdf']) == -1){

                    $('#error1').slideDown("slow");
                    $('#error2').slideUp("slow");

                    a=0;
                }else{
                    var picsize = (this.files[0].size);

                    if (picsize > 5000000){
                        $('#error2').slideDown("slow");

                        a=0;
                    }else{
                        a=1;

                        $('#error2').slideUp("slow");
                    }
                    $('#error1').slideUp("slow");

                    if (a==1){
                        $('input:submit').attr('disabled',false);
                    }
                }
            });
        };

        formateDate = function($date) {
            var d = new Date($date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [day, month, year].join('/');
        };

        dataSIK = function ($id) {
            $.post($urlgetsik, {
                idWo: $id,
                csrf_izin: $token
            }, function (xy) {
                xy = $.parseJSON(xy);

                $("input[name='csrf_izin']").val($token);

                $.each(xy.result, function (i, item) {
                    if (xy.result != null) {
                        $("#formSIK input, textarea").prop("disabled", true);
                    }
                    $("input[name=NoSKI]").val(item.no_sik);
                    $("input[name=tglSKI]").val(item.tgl_sik);
                    $("input[name=tglMulai]").val(item.tgl_mulai);
                    $("input[name=tglSelesaiSIK]").val(item.tgl_selesai);
                    $("input[name=dikeluarkanOleh]").val(item.out_by);
                    $("textarea[name=deskripsi]").val(item.deskripsi);

                    $(".upload-sik .custom-file").remove();

                    $(".upload-sik").append('<div class="col-lg"><a href="#modal-preview" data-toggle="modal">'+item.nama.split("--").pop()+'</a></div>');

                    if (item.file_type == "application/pdf" || item.file_type == "image/jpeg" || item.file_type == "image/png"){
                        createPreviewFile([{
                            type: item.file_type,
                            path: $baseurl+item.path_sik+"/"+item.nama
                        }], ".preview-file", false);
                    }else{
                        $("#modal-preview .modal-body").html('<div class="text-center"><img class="img-responsive img-fluid" src="<?=base_url()."/assets/img/no-preview.jpg";?>" alt="" width="300px"></div>');
                        $("#modal-preview .modal-footer").append('<a href="'+item.path_sik+'" class="btn btn-primary">Download</a>');
                    }
                });
            });
        };

        cancelBtn = function () {
            $(".cancel-btn").on("click",function(e){
                e.preventDefault();
                if(e.handler != true){
                    document.location.href= $base;
                    e.handler = true;
                }
            });
        };

        saveSik = function () {
            let forms=$("#formSIK");
            forms.submit(function(e) {
                e.preventDefault();
            }).validate({
                errorClass: "text-danger",
                rules: {
                    NoSKI:{
                        required: true
                    },
                    tglSKI:{
                        required: true
                    },
                    tglMulai:{
                        required: true
                    },
                    tglSelesaiSIK:{
                        required: true
                    },
                    dikeluarkanOleh:{
                        required: true
                    },
                    deskripsi:{
                        required: true
                    },
                    filesik:{
                        required: true
                    }
                },
                submitHandler: function(forms) {
                    $.ajax({
                        url:$urlsave,
                        type:"post",
                        data:new FormData(forms),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(result) {
                            result = $.parseJSON(result);
                            if(result.result == 'true'){
                                swalSuccess(result.message, function (x) {
                                    if (x){
                                        document.location.href= $base;
                                    }
                                });
                            }else{
                                swalAlert(result.message);
                            }

                        }
                    });
                }
            });
        };

        return {
            init: function () {
                initial(), upload(), cancelBtn(), saveSik();
            }
        }
    }();



    $(document).ready(function () {

        bsCustomFileInput.init();
        sikForm.init();

    });

</script>