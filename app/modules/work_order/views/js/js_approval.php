<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($tjs)) {
    echo $tjs;
}
if (isset($fjs)) {
    echo $fjs;
}

/** @var $columnSearch array valid column to be search */
/** @var $columnSearchMask array valid column to be search */
?>
<script src="https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.js"></script>
<script>
    let approvalList = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        const $tbl = $("#dt-user");
        const $tblbd = $("#dt-user tbody");
        const $url = "<?php echo base_url()?>work_order/approval/getdata";
        const $urladdworkorder = "<?php echo base_url()?>work_order/approval/add";
        const $urlgetsku = "<?php echo base_url()?>work_order/approval/getsku";
        const $urledit = "<?php echo base_url()?>work_order/approval/closing";
        const $urlsku = "<?php echo base_url()?>work_order/approval/addsku";
        let $column_search = <?php echo json_encode($columnSearch); ?>;
        let $column_search_mask = <?php echo json_encode($columnSearchMask); ?>;


        initial = function (start_date=null, end_date=null, column = null) {
            $("select[name=order] option").remove();

            $("select[name=order]").append($('<option>', {
                value: "",
                text: "-- Pilih --"
            }));


            $column_search.forEach(function (value, index) {

                if (value != null){

                    $("select[name=order]").append($('<option>', {
                        value: index,
                        text: $column_search_mask[index]
                    }));
                }
            });

            $("select[name=order]").append($('<option>', {
                value: "reset",
                text: "-- Reset filter --"
            }));

            $tbl.DataTable({
                'iDisplayLength': 10,
                "responsive": true,
                "processing": true,
                "serverSide": true,
                ajax: {
                    url: $url,
                    type: "post",
                    data: function (d) {
                        d.csrf_izin = $token;
                        d.start_date = start_date;
                        d.end_date = end_date;
                        d.column = column;
                    },
                    dataSrc: function (dt) {
                        $("input[name='csrf_izin']").val($token);

                        return dt.data;
                    }
                },
                buttons: [{
                    text: '<i class="fal fa-user-plus mr-1"></i> Add User',
                    name: 'add',
                    className: 'btn-primary btn-md mr-1 mt-n1 btnadduser'
                }],
                dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'l><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'Bf>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                columns: [{
                    data: 0,
                    "orderable": false,
                    render: $.fn.dataTable.render.text(),
                },{
                    data: 1,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 2,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 3,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 4,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 5,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 6,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 14,
                    "orderable": false,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 15,
                    "orderable": false,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 13,
                    "orderable": false,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 10,
                    "orderable": true,
                    render: $.fn.dataTable.render.text(),
                }, {
                    data: 11,
                    "orderable": false,
                    'class': 'text-center',
                    render:function (x, y, z) {
                        if (z[11] < 10){
                            return '<a href="' + $urledit + '/' + z[12] + '" class="btn btn-sm btn-icon btn-outline-primary rounded-circle mr-1 editWo" title="Approval Workorder">' +
                                '<i class="fa fa-pencil-square-o"></i></a>';
                        }else if(z[11] == 10){
                            return '<a href="' + $urledit + '/' + z[12] + '" class="btn btn-sm btn-icon btn-outline-success rounded-circle mr-1 editWo" title="Lihat Permohonan">' +
                                '<i class="fa fa-search"></i></a>';
                        }else if(z[11] == 11){
                            return '<a href="' + $urledit + '/' + z[12] + '" class="btn btn-sm btn-icon btn-outline-danger rounded-circle mr-1 editWo" title="Lihat Permohonan">' +
                                '<i class="fa fa-search"></i></a>';
                        }
                    }
                }],
                "rowCallback": function (row, data, iDisplayIndex) {
                    var pg = $tbl.DataTable().page.info();
                    var index = iDisplayIndex + 1;
                    $('td:eq(0)', row).html(index + pg.start);
                    return row;
                }
            });
        };

        searchdata = function () {
            var order = $("select[name=order]");
            order.on('change', function() {
                $("select[name=order] option[value='']").remove();

                if (order.find(":selected").text().toLowerCase().match(/tanggal/g)){
                    $(".search-data").unbind( "click" );
                    $(".input-daterange").css('display', '');
                    $("input[name=search]").css('display', 'none');

                    [".input-daterange input"]
                        .forEach(function (key) {
                            $(key).datepicker({dateFormat: 'dd/mm/yy'});

                            if (!$(key).val()) {
                                $(key).datepicker("setDate", new Date());
                            }
                        });

                    $(".search-data").on("click", function () {

                        $tbl.DataTable().clear().destroy();

                        var order = $("select[name=order]").val();

                        initial($("input[name=start_date]").val(), $("input[name=end_date]").val(), $("select[name=order]").val());

                        $("select[name=order]").val(order);

                        $(".dataTables_filter").css("display", "none");

                    });

                }else{
                    if (order.find(":selected").text().toLowerCase().match(/reset/g)){
                        $tbl.DataTable().clear().destroy();

                        initial();

                        $("select[name=order]").val(selected);

                        $(".dataTables_filter").css("display", "none");

                        $("input[name=search]").css('display', '').val('');

                        $(".input-daterange").css('display', 'none');


                    }else {
                        var selected = order.find(":selected").val();

                        $(".search-data").unbind("click");

                        $(".input-daterange").css('display', 'none');

                        $("input[name=search]").css('display', '');

                        $(".search-data").on("click", function () {
                            $tbl.DataTable().clear().destroy();

                            initial();

                            $(".dataTables_filter").css("display", "none");

                            $tbl.DataTable()
                                .columns(selected)
                                .search($("input[name=search]").val()).draw();

                            $("select[name=order]").val(selected);

                        });
                    }
                }
            });
        };


        return {
            init: function () {
                initial(),  searchdata();
            }
        }
    }();

    $(document).ready(function () {

        bsCustomFileInput.init();
        approvalList.init();
        $(".dataTables_filter").css("display", "none");


    });

</script>