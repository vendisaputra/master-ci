<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($tjs)) {
    echo $tjs;
}
if (isset($fjs)) {
    echo $fjs;
}
?>
<script src="https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.js"></script>

<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>

<script src="https://cdn01.boxcdn.net/platform/preview/2.34.0/en-US/preview.js"></script>
<script>
    let ApprovalForm = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        let $endPath = window.location.pathname.split("/").pop();
        const $base = "<?php echo base_url()?>work_order/approval";
        const $baseurl = "<?php echo base_url()?>";
        const $urlgetbiaya = "<?php echo base_url()?>biaya_perizinan/getBiaya";
        const $urlkecamatan = "<?php echo base_url()?>work_order/approval/getKecamatan";
        const $urlkelurahan = "<?php echo base_url()?>work_order/approval/getKelurahan";
        const $urlgetsik = "<?php echo base_url()?>work_order/approval/getSik";
        const $urlsave = "<?php echo base_url()?>work_order/approval/saveClosing";
        const $urlgetdata = "<?php echo base_url()?>work_order/approval/getDataEditWorkorder";
        const $urlGetDepositFile = "<?php echo base_url()?>work_order/approval/getFileDeposit";
        let $listFormInput = new Array();
        let $listFormInput2 = new Array();


        workorder = function () {
            $("input[name='csrf_izin']").val($token);

            $.post($urlgetdata, {
                idWo: $endPath,
                csrf_izin: $token
            }, function (xy) {
                xy = $.parseJSON(xy);
                $token = xy.token;

                localStorage.setItem("kota", xy.result.kota);
                localStorage.setItem("kecamatan", xy.result.kecamatan);
                localStorage.setItem("pic", xy.result.id_pic);


                var callbackplace = function () {
                    $(".kecamatan").val(xy.result.kecamatan);
                    $(".kelurahan").val(xy.result.kelurahan);
                    $(".pic").val(xy.result.id_pic);
                    $("select[name=mitra]").val(xy.result.idMitra);
                };
                getkecamatan(callbackplace);
                getkelurahan(callbackplace);

                $("input[name='id']").val(xy.result.id);
                $("input[name='Nowo']").val(xy.result.wo_no);
                $("input[name='NoSales']").val(xy.result.sales_no);
                $("input[name='tglPI']").val(formateDate(xy.result.tgl_wo));
                $("input[name='tglSO']").val(formateDate(xy.result.tgl_sales));
                $("input[name='tglPermintaan']").val(formateDate(xy.result.tgl_mulai));
                $("input[name='tglSelesai']").val(formateDate(xy.result.tgl_selesai));
                $("input[name='Nmplgn']").val(xy.result.nama_pelanggan);
                $("input[name='NoIO']").val(xy.result.no_io);
                $("input[name='noSo']").val(xy.result.no_so);
                $("input[name='generateWo']").val(xy.result.wo_no);
                $("input[name='Latitude']").val(xy.result.latitude);
                $("input[name='Longitude']").val(xy.result.longitude);
                $("input[name='kode_pos']").val(xy.result.kodepos);
                $("textarea[name='alamat']").val(xy.result.alamat);
                $("select[name='kawasan']").val(xy.result.kawasan);
                $(".kabupaten").val(xy.result.kota);

                $("select[name=jenis_permintaan]").val(xy.result.jenis_perizinan);

                if (xy.result.status > 9 && xy.result.status < 12){
                    $("#formSIK input, textarea").prop("disabled", true);
                    if (xy.result.status == 10){
                        $("input[name=persetujuan][value=1]").prop("checked",true);
                    }else if(xy.result.status == 11){
                        $("input[name=persetujuan][value=0]").prop("checked",true);
                    }

                    $("#form_add_workorder :input").prop("disabled", true);
                    $(".btn-simpan").remove();
                    $(".cancel-btn").css("margin-left", -15);
                    $(".cancel-btn").removeClass("btn-light");
                    $(".cancel-btn").addClass("btn-danger");
                    $(".cancel-btn").html("Kembali");
                    $(".cancel-btn").prop("disabled", false);
                }
            });

        };

        biaya = function () {
            $.post($urlgetbiaya, {
                idWo: $endPath,
                csrf_izin: $token
            }, function (xy) {
                xy = $.parseJSON(xy);

                $("input[name='csrf_izin']").val($token);

                $.each(xy.data, function (i, item) {

                    $(".approval-manager").css("display", "");
                    $("select[name=form-biaya]").css("display", "none");
                    $('#form-biaya-perizinan input').prop("disabled", true);
                    $('#form-biaya-perizinan input[name=persetujuan]').prop("disabled", false);
                    $("input[name='csrf_izin']").prop("disabled", false);
                    $("input[name='idWo']").prop("disabled", false);
                    $("input[name='idWoBiaya']").prop("disabled", false);


                    $("input[name=idWo]").val($endPath);
                    $("input[name=idWoBiaya]").val(item.id_izin);
                    $("input[name=idWoApproval]").val(item.id);
                    $(".keterangan").val(item.keterangan);

                    var selectinput = ' <div class="form-group">' +
                        '                            <div class="row">' +
                        '                                <div class="col-lg"' +
                        '                                    <label>' + item.nama + '</label>' +
                        '                                    <div class="row">' +
                        '                                       <div class="col-10">' +
                        '                                           <input type="hidden" name="id-biaya[]" value="' + item.biaya_id + '" class="form-control " value="Generate System" readonly>' +
                        '                                           <input type="text" name="nominal[]" value="' + item.nominal + '" class="form-control '+item.nama.replace(/ /g,"-")+"-val"+'" placeholder="">' +
                        '                                       </div>' +
                        '                                   </div>' +
                        '                            </div>' +
                        '                        </div>';

                    if (item.biaya_id == 2) {
                        selectinput = selectinput + '<br><div class="form-group">' +
                            '                            <div class="row">' +
                            '                                <div class="col-lg-6">' +
                            '                                      <div class="input-group mb-3">' +
                            '                                           <div class="input-group-append">' +
                            '                                               <span class="input-group-text" id="basic-addon2">Tgl Mulai</span>' +
                            '                                            </div>' +
                            '                                            <input type="text" name="tglmulai" data-date-format="dd/mm/yyyy" id="tglmulai" value="' + formateDate(item.mulai) + '" class="form-control" placeholder="Tanggal 1">' +
                            '                                      </div>' +
                            '                                </div>' +
                            '                                <div class="col-lg-6">' +
                            '                                      <div class="input-group mb-3">' +
                            '                                           <div class="input-group-append">' +
                            '                                               <span class="input-group-text" id="basic-addon2">Tgl Selesai</span>' +
                            '                                            </div>' +
                            '                                            <input type="text" name="tglselesai" data-date-format="dd/mm/yyyy" id="tglselesai" value="' + formateDate(item.selesai) + '" class="form-control" placeholder="Tanggal 1">' +
                            '                                      </div>' +
                            '                                </div>' +
                            '                            </div>';

                    }
                    if (item.nominal != null && item.biaya_id != 16 && item.biaya_id != 15) {
                        $("select[name=form-biaya] option[value='" + item.nama.replace(/ /g, "-") + "-" + item.biaya_id + "']").remove();
                        $listFormInput.push($(selectinput));
                        $listFormInput2 = $listFormInput;
                        sortlistInput();
                    } else if (item.biaya_id == 16) {
                        $(".id-" + item.biaya_id).val(item.biaya_id);
                        $("." + item.nama.replace(/ /g,"-")).val(item.nominal);
                    } else if (item.biaya_id == 15){
                        $(".id-" + item.biaya_id).val(item.biaya_id);
                        $("." + item.nama.replace(/ /g,"-")).val(item.nominal);
                    }

                    $(".nama-pelanggan").html(item.nama_pelanggan);
                    $(".no-so").html(item.sales_no);
                    $(".layanan").html(item.layanan);
                    $(".origanating").html("-");
                    $(".terminating").html(item.alamat);
                });
            });
        };

        tunai_transfer = function(){
            $('input[type=radio][name=jenis_serah_deposit]').change(function() {
                if (this.value == 2) {
                    $("input[name=no_bukti_terima_deposit], input[name=tgl_terima_deposit]").prop('disabled', false);
                    $(".tunai").css("display", "");
                    $("input[name=filebukti]").prop('disabled', true);
                    $(".transfer").css("display", "none");
                }else{
                    $("input[name=no_bukti_terima_deposit], input[name=tgl_terima_deposit]").prop('disabled', true);
                    $(".tunai").css("display", "none");
                    $("input[name=filebukti]").prop('disabled', false);
                    $(".transfer").css("display", "");
                }
            });

            if ($('input[type=radio][name=jenis_serah_deposit]:checked').val() == 1){
                $(".transfer").css("display", "");
                $(".custom-file").css("display", "none");
                $(".transfer>a").css("display", "");
                $("input[name=no_bukti_terima_deposit], input[name=tgl_terima_deposit]").prop('disabled', true);
                $(".tunai").css("display", "none");
            }
        };

        dataSIK = function () {
            $.post($urlgetsik, {
                idWo: $endPath,
                csrf_izin: $token
            }, function (xy) {
                xy = $.parseJSON(xy);

                $("input[name='csrf_izin']").val($token);

                $.each(xy.result, function (i, item) {

                    if (xy.result != null) {
                       $("#formSIK input, textarea").prop("disabled", true);
                    }

                    $("input[name=NoSKI]").val(item.no_sik);
                    $("input[name=tglSKI]").val(item.tgl_sik);
                    $("input[name=tglMulai]").val(item.tgl_mulai);
                    $("input[name=tglSelesaiSIK]").val(item.tgl_selesai);
                    $("input[name=dikeluarkanOleh]").val(item.out_by);
                    $("textarea[name=deskripsi]").val(item.deskripsi);

                    $(".upload-sik .custom-file").remove();

                    $(".upload-sik").append('<div class="col-lg"><a href="#modal-preview" data-toggle="modal">'+item.nama.split("--").pop()+'</a></div>');

                    if (item.file_type == "application/pdf" || item.file_type == "image/jpeg" || item.file_type == "image/png"){
                        createPreviewFile([{
                            type: item.file_type,
                            path: $baseurl+item.path_sik+"/"+item.nama
                        }], ".preview-file", false);
                    }else{
                        $("#modal-preview .modal-body").html('<div class="text-center"><img class="img-responsive img-fluid" src="<?=base_url()."/assets/img/no-preview.jpg";?>" alt="" width="300px"></div>');
                        $("#modal-preview .modal-footer").append('<a href="'+item.path_sik+'" class="btn btn-primary">Download</a>');
                    }
                });
            });
        };

        bindVal = function (context) {
            $(context).attr("value", $(context).val());
        };

        showInput = function () {
            let $serialNumberId = 0;
            $('select[name=form-biaya]').on('change', function () {
                var selected = this.value,
                    title = $('select[name=form-biaya] option:selected').html(),
                    idBiaya = selected.split("-").pop();
                var selectinput = ' <div class="form-group ' + selected + '">' +
                    '                            <div class="row">' +
                    '                                <div class="col-lg"' +
                    '                                    <label>' + title + '</label>' +
                    '                                    <div class="row">' +
                    '                                       <div class="col-10">' +
                    '                                           <input type="hidden" name="id-biaya[]" value="' + idBiaya + '" class="form-control" value="Generate System" readonly>' +
                    '                                           <input type="text" name="nominal[]" class="form-control '+title.replace(/ /g, "-")+'-val" placeholder="' + title + '" oninput="bindVal(this)" onchange="bindVal(this)" value="">' +
                    '                                       </div>' +
                    '                                       <div class="col-1">' +
                    '                                           <button type="button" class="btn btn-danger" onclick="deleteInput(' + "'" + selected + "', " + $listFormInput.length + ');" style="margin-left:0px;"><i class="fa fa-trash"></i></button>' +
                    '                                       </div>' +
                    '                                   </div>' +
                    '                            </div>' +
                    '                        </div>';

                if (title == "Biaya Sewa Pertahun") {
                    selectinput = selectinput + '<br><div class="form-group ' + selected + '">' +
                        '                            <div class="row">' +
                        '                                <div class="col-lg-6">' +
                        '                                      <div class="input-group mb-3">' +
                        '                                           <div class="input-group-append">' +
                        '                                               <span class="input-group-text" style="font-size:10px" id="basic-addon2">Tgl Mulai</span>' +
                        '                                            </div>' +
                        '                                            <input type="text" name="tglmulai" data-date-format="dd/mm/yyyy" id="tglmulai" value="" class="form-control" placeholder="Tanggal 1"  oninput="bindVal(this)" onchange="bindVal(this)">' +
                        '                                      </div>' +
                        '                                </div>' +
                        '                                <div class="col-lg-6">' +
                        '                                      <div class="input-group mb-3">' +
                        '                                           <div class="input-group-append">' +
                        '                                               <span class="input-group-text" style="font-size:10px" id="basic-addon2">Tgl Selesai</span>' +
                        '                                            </div>' +
                        '                                            <input type="text" name="tglselesai" data-date-format="dd/mm/yyyy" id="tglselesai" value="" class="form-control" placeholder="Tanggal 1"  oninput="bindVal(this)" onchange="bindVal(this)">' +
                        '                                      </div>' +
                        '                                </div>' +
                        '                            </div>';

                }

                $listFormInput.push($(selectinput));
                $listFormInput2 = $listFormInput;
                sortlistInput();

                $("select[name=form-biaya] option[value='" + this.value + "']").remove();


            });
        };

        sortlistInput = function () {
            var count = 0,
                col;
            count = $listFormInput.length;
            $listFormInput.forEach(function (item, index) {
                if (index < 6) {
                    col = 1;
                    $(".for-col-" + col).empty();
                    $(".for-col-1").empty();
                    $(".for-col-2").empty();
                    $(".for-col-3").empty();
                    $(".for-col-" + col).append($listFormInput.slice(0, index + 1));
                } else if (index > 5 && index < 12) {
                    col = 2;
                    $(".for-col-" + col).empty();
                    $(".for-col-" + col).append($listFormInput.slice(6, index + 1));
                } else if (count > 11) {
                    col = 3;
                    $(".for-col-" + col).empty();
                    $(".for-col-" + col).append($listFormInput.slice(12, index + 1));
                }
            });
            ["#tglSKI, #tglMulai, #tglSelesaiSIK"]
                .forEach(function (key) {
                    $(key).datepicker({dateFormat: 'dd/mm/yy', minDate: new Date()});

                });

            $("input[name='nominal[]']").mask('000.000.000', {reverse: true});
        };

        deleteInput = function ($name, $index) {
            var forDelete = [];
            forDelete.push($listFormInput2[$index]);
            $("." + $name).remove();
            $('select[name=form-biaya]').append($('<option>', {
                value: $name,
                text: $name.substring(0, $name.length - 2).replace("-", " ")
            }));
            sortingBiaya();
            $listFormInput = $listFormInput.filter(item => !forDelete.includes(item));
            forDelete = [];
            sortlistInput();
        };

        sortingBiaya = function () {
            var options = $("select[name=form-biaya] option");
            options.detach().sort(function (a, b) {
                var at = $(a).text();
                var bt = $(b).text();
                return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
            });
            options.appendTo("select[name=form-biaya]");

            $('select[name=form-biaya]').val("");
        };

        getkecamatan = function (callback) {
            var kabupaten = "";
            if (callback) {
                $.post($urlkecamatan, {
                    kabupatenId: localStorage.getItem("kota"),
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);

                    $("select.kecamatan").empty();
                    $("select.kecamatan").append($('<option>', {
                        value: "",
                        text: "-- Kecamatan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kecamatan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                    callback();
                });
            }
            $("select.kabupaten").change(function () {
                kabupaten = $(this).children("option:selected").val();
                $.post($urlkecamatan, {
                    kabupatenId: kabupaten,
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);
                    $("select.kecamatan").empty();
                    $("select.kecamatan").append($('<option>', {
                        value: "",
                        text: "-- Kecamatan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kecamatan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                });
            });
        };

        getkelurahan = function (callback) {
            var kecamatan = "";
            if (callback) {
                $.post($urlkelurahan, {
                    kecamatanId: localStorage.getItem("kecamatan"),
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);

                    $("select.kelurahan").empty();
                    $("select.kelurahan").append($('<option>', {
                        value: "",
                        text: "-- Kelurahan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kelurahan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                    callback();
                });
            }
            $("select.kecamatan").change(function () {
                kecamatan = $(this).children("option:selected").val();
                $.post($urlkelurahan, {
                    kecamatanId: kecamatan,
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);
                    $("select.kelurahan").empty();
                    $("select.kelurahan").append($('<option>', {
                        value: "",
                        text: "-- Kelurahan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kelurahan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                });
            });
        };

        save = function () {
            let forms=$("#form_add_workorder");
            var data;
            $("input[name=woId]").val($endPath[$endPath.length-1]);
            forms.submit(function(e) {
                e.preventDefault();

                var form = $('#form_add_workorder')[0];
                var form2 = $('#formSIK')[0];

                // Create an FormData object
                data = new FormData(form);
                data2 = new FormData(form2);
                data2.forEach((value, key) => {
                   data.append(key, value);
                });
            }).validate({
                rules: {
                    NoSku:{
                        required: true,
                        minlength: 2
                    },
                    tgl:{
                        required: true,
                        minlength: 2
                    },
                    persetujuan:{
                        required: true,
                    }
                },
                submitHandler: function(forms) {
                    $.ajax({
                        url:$urlsave,
                        type:"post",
                        enctype: 'multipart/form-data',
                        data:data,
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(result) {
                            result = $.parseJSON(result);
                            if(result.result == 'true'){
                                swalSuccess(result.message, function (x) {
                                    document.location.href= $base;
                                });
                            }else{
                                swalAlert(result.message);
                            }

                        }
                    });
                }
            });
        };

        validation = function(){
            $("#form_add_workorder").submit();
        };

        cancelBtn = function () {
            $(".cancel-btn").on("click",function(e){
                e.preventDefault();
                if(e.handler != true){
                    document.location.href= $base;
                    e.handler = true;
                }
            });
        };

        formateDate = function($date) {
            var d = new Date($date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [day, month, year].join('/');
        };

        upload = function() {

            $('input[type="submit"]').prop("disabled", true);

            var a=0;

            $('input[name=filesik]').bind('change', function() {
                if ($('input:submit').attr('disabled',false)){
                    $('input:submit').attr('disabled',true);
                }

                var ext = $('input[name=filesik]').val().split('.').pop().toLowerCase();

                if ($.inArray(ext, ['gif','png','jpg','jpeg', 'doc', 'docx', 'xls', 'xlsx', 'pdf']) == -1){

                    $('#error1').slideDown("slow");
                    $('#error2').slideUp("slow");

                    a=0;
                }else{
                    var picsize = (this.files[0].size);

                    if (picsize > 5000000){
                        $('#error2').slideDown("slow");

                        a=0;
                    }else{
                        a=1;

                        $('#error2').slideUp("slow");
                    }
                    $('#error1').slideUp("slow");

                    if (a==1){
                        $('input:submit').attr('disabled',false);
                    }
                }
            });
        };

        previewfile = function($id) {
            $.post($urlGetDepositFile, {
                idWo: $id,
                csrf_izin: $token
            }, function (xy) {
                xy = $.parseJSON(xy);
                if (xy.file.type == "application/pdf" || xy.file.type == "image/jpeg" || xy.file.type == "image/png"){
                    createPreviewFile([{
                        type: xy.file.type,
                        path: xy.file.path
                    }], ".preview-file", false);
                }else{
                    $("#modal-preview .modal-body").html('<div class="text-center"><img class="img-responsive img-fluid" src="<?=base_url()."/assets/img/no-preview.jpg";?>" alt="" width="300px"></div>');
                    $("#modal-preview .modal-footer").append('<a href="'+xy.file.path+'" class="btn btn-primary">Download</a>');
                }


            });
        };


        return {
            init: function () {
                workorder(), biaya(), dataSIK(), getkecamatan(), getkelurahan(), save(), cancelBtn(), upload(), tunai_transfer();
            }
        }
    }();



    $(document).ready(function () {

        bsCustomFileInput.init();
        ApprovalForm.init();


    });

</script>
