<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by fajar at 1/28/20
 */

/** @var $tjs string is table script */
if (isset($tjs)) {
    echo $tjs;
}

/** @var $fjs string is form script */
if (isset($fjs)) {
    echo $fjs;
}
?>
<script src="https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.js"></script>
<script lang="js">

    let PindahPicForm = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        let $endPath = window.location.pathname.split("/");
        const $base = "<?php echo base_url()?>work_order/pindah_pic";
        const $urlkecamatan = "<?php echo base_url()?>work_order/pindah_pic/getKecamatan";
        const $urlkelurahan = "<?php echo base_url()?>work_order/pindah_pic/getKelurahan";
        const $urlkawasan = "<?php echo base_url()?>work_order/pindah_pic/getKawasan";
        const $urlsave = "<?php echo base_url()?>work_order/pindah_pic/save";
        const $urlgetdata = "<?php echo base_url()?>work_order/pindah_pic/getDataEdit";

        initial = function () {
            if ($endPath[$endPath.length-2]=="edit") {
                $.post($urlgetdata, {
                    idWo: $endPath[$endPath.length-1],
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);

                    localStorage.setItem("kota", xy.result.kota);
                    localStorage.setItem("kecamatan", xy.result.kecamatan);
                    localStorage.setItem("pic", xy.result.id_pic);


                    var callbackplace = function () {
                        $(".kecamatan").val(xy.result.kecamatan);
                        $(".kelurahan").val(xy.result.kelurahan);
                        $("input[name='csrf_izin']").val($token);
                    };
                    getkecamatan(callbackplace);
                    getkelurahan(callbackplace);


                    $("input[name='id']").val(xy.result.id);
                    $("input[name='Nowo']").val(xy.result.wo_no);
                    $("input[name='NoSales']").val(xy.result.sales_no);
                    $("input[name='tglPI']").val(formateDate(xy.result.tgl_wo));
                    $("input[name='tglSO']").val(formateDate(xy.result.tgl_sales));
                    $("input[name='tglPermintaan']").val(formateDate(xy.result.tgl_mulai));
                    $("input[name='tglSelesai']").val(formateDate(xy.result.tgl_selesai));
                    $("input[name='Nmplgn']").val(xy.result.nama_pelanggan);
                    $("input[name='NoIO']").val(xy.result.no_io);
                    $("input[name='noSo']").val(xy.result.no_so);
                    $("input[name='generateWo']").val(xy.result.wo_no);
                    $("input[name='Latitude']").val(xy.result.latitude);
                    $("input[name='Longitude']").val(xy.result.longitude);
                    $("input[name='kode_pos']").val(xy.result.kodepos);
                    $("input[name='pic_lama']").val(xy.result.name);
                    $("input[name='mitra']").val(xy.result.mitra);
                    $("textarea[name='alamat']").val(xy.result.alamat);
                    $("select[name='kawasan']").val(xy.result.kawasan);
                    $("input[name=mitraId]").val(xy.result.idMitra);
                    $(".kabupaten").val(xy.result.kota);

                    $("select[name=jenis_permintaan]").val(xy.result.jenis_perizinan);

                    $('#form_add_workorder input, select, textarea').prop('disabled', true);
                    $('#form_add_workorder input[name=id],input[name=mitraId],input[name=csrf_izin], select[name=nmPIC]').prop('disabled', false);
                });
            }
        };

        getkecamatan = function (callback) {
            var kabupaten = "";
            if (callback) {
                $.post($urlkecamatan, {
                    kabupatenId: localStorage.getItem("kota"),
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);

                    $("select.kecamatan").empty();
                    $("select.kecamatan").append($('<option>', {
                        value: "",
                        text: "-- Kecamatan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kecamatan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                    callback();
                });
            }
            $("select.kabupaten").change(function () {
                kabupaten = $(this).children("option:selected").val();
                $.post($urlkecamatan, {
                    kabupatenId: kabupaten,
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);
                    $("select.kecamatan").empty();
                    $("select.kecamatan").append($('<option>', {
                        value: "",
                        text: "-- Kecamatan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kecamatan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                });
            });
        };

        getkelurahan = function (callback) {
            var kecamatan = "";
            if (callback) {
                $.post($urlkelurahan, {
                    kecamatanId: localStorage.getItem("kecamatan"),
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);

                    $("select.kelurahan").empty();
                    $("select.kelurahan").append($('<option>', {
                        value: "",
                        text: "-- Kelurahan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kelurahan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                    callback();
                });
            }
            $("select.kecamatan").change(function () {
                kecamatan = $(this).children("option:selected").val();
                $.post($urlkelurahan, {
                    kecamatanId: kecamatan,
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);
                    $("select.kelurahan").empty();
                    $("select.kelurahan").append($('<option>', {
                        value: "",
                        text: "-- Kelurahan --"
                    }));
                    $.each(xy.result, function (i, item) {
                        $("select.kelurahan").append($('<option>', {
                            value: item.id,
                            text: item.nama
                        }));
                    });
                });
            });
        };


        getmaps = function(){
            $(".maps").on("click",function(e){
                e.preventDefault();
                if(e.handler != true){
                    $("#modal-maps").modal("show");
                    e.handler = true;
                }
            });
        };

        cancelBtn = function () {
            $(".cancel-btn").on("click",function(e){
                e.preventDefault();
                if(e.handler != true){
                    document.location.href= $base;
                    e.handler = true;
                }
            });
        };

        formateDate = function($date) {
            var d = new Date($date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [day, month, year].join('/');
        };

        save = function () {
            let form=$("#form_add_workorder");
            form.submit(function(e) {
                e.preventDefault();
            }).validate({
                rules: {
                    NoSales:{
                        required: true,
                        minlength: 2
                    },
                    tglPI:{
                        required: true,
                        minlength: 2
                    },
                    tglSO:{
                        required: true,
                        minlength: 2
                    },
                    jenis_permintaan:{
                        required: true,
                    },
                    tglPermintaan:{
                        required: true,
                        minlength: 2
                    },
                    tglSelesai:{
                        required: true,
                        minlength: 2
                    },
                    Nmplgn:{
                        required: true,
                        minlength: 2
                    },
                    NoIO:{
                        required: true,
                        minlength: 2
                    },
                    kawasan:{
                        required: true,
                    },
                    noSo:{
                        required: true,
                        minlength: 2
                    },
                    kabupaten:{
                        required: true,
                    },
                    kecamatan:{
                        required: true,
                    },
                    Latitude:{
                        required: true,
                        minlength: 2
                    },
                    kelurahan:{
                        required: true,
                    },
                    Longitude:{
                        required: true,
                        minlength: 2
                    },
                    kode_pos:{
                        required: true,
                        minlength: 2
                    },
                    alamat:{
                        required: true,
                        minlength: 2
                    },
                },
                submitHandler: function(form) {
                    $.ajax({
                        url: $urlsave,
                        type: 'POST',
                        data: $(form).serialize(),
                        dataType: 'json',
                        success: function(result) {
                            if(result.result == 'true'){
                                swalSuccess(result.message, function (x) {
                                    if (x){
                                        document.location.href = $base;
                                    }
                                });
                            }else{
                                swalAlert(result.message);
                            }
                        }
                    });
                }
            });
        };

        getLocation = function () {
            $("select.kawasan").change(function(){
                kawasanId = $(this).children("option:selected").val();
                $.post($urlkawasan, {
                    id: kawasanId,
                    csrf_izin: $token
                }, function (xy) {
                    xy = $.parseJSON(xy);
                    $.each(xy.result, function (i, item) {
                        if (item.latitude != null || item.longitude != null){
                            mapform(item.latitude, item.longitude)

                        }else {
                            navigator.geolocation.getCurrentPosition(function(location) {
                                $defaultLat = location.coords.latitude;
                                $defaultLng = location.coords.longitude;
                                mapform(location.coords.latitude, location.coords.longitude)
                            });
                        }
                    });
                });
            });
        };

        mapform = function ($lat, $lng){
            $('#modal-maps').find('.modal-body').html('<div id="osm-map" style="width: 100%; height: 500px;"></div>');
            var lat, long;
            var osmUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
                osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> coffee',
                osm = L.tileLayer(osmUrl, {
                    maxZoom: 18,
                    attribution: osmAttrib
                });

            var map = L.map('osm-map').setView([$lat, $lng], 12).addLayer(osm);

            var marker = null;      // create initial marker here(for edit if needed)
            function onMapClick(e) {
                if (marker != null) {
                    map.removeLayer(marker);
                }
                marker = L.marker(e.latlng, {
                    draggable: true,
                    title: "Resource location",
                    alt: "Resource Location",
                    riseOnHover: true
                }).addTo(map).bindPopup(e.latlng.toString()).openPopup();

                $defaultLat = e.latlng.lat;
                $defaultLng = e.latlng.lng;

                marker.on("dragend", function (ev) {
                    var chagedPos = ev.target.getLatLng();
                    this.bindPopup(chagedPos.toString()).openPopup();
                    document.getElementById('coordinat').value = ev.target.getLatLng().lat + ',' + ev.target.getLatLng().lng;

                });
            }
            map.on('click', onMapClick);
        };

        savelocation = function () {
            $(".save-coordinat").on("click",function(e){
                e.preventDefault();
                if(e.handler != true){
                    $(".longitude").val($defaultLng);
                    $(".latitude").val($defaultLat);
                    $("#modal-maps").modal("hide");
                    e.handler = true;
                }
            });
        };

        return {
            init: function () {
                initial(), getkecamatan(), getkelurahan(), getmaps(), cancelBtn(), save(), savelocation(), getLocation(), mapform(-6.2472172, 106.8176994);
            }
        }
    }();

    $(document).ready(function () {
        PindahPicForm.init();


        $(".dataTables_filter").css("display", "none");

        $('#tglPermintaan, #tglPI, #tglSO, #tglPermintaan, #tglSelesai, .tgl').datepicker({
            weekStart: 1,
            daysOfWeekHighlighted: "6,0",
            autoclose: true,
            todayHighlight: true,
        });

        $('#tglPermintaan, #tglPI, #tglSO, #tglPermintaan, #tglSelesai, .tgl').datepicker("setDate", new Date());
    });
</script>