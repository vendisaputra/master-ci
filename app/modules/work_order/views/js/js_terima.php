<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by fajar at 1/29/20
 */

/** @var $tjs string is table script */
if (isset($tjs)) {
    echo $tjs;
}

/** @var $fjs string is form script */
if (isset($fjs)) {
    echo $fjs;
}
?>

<script lang="js">
    $(document).ready(function () {
    });
</script>