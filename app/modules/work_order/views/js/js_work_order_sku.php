<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($tjs)) {
    echo $tjs;

}
if (isset($fjs)) {
    echo $fjs;

}
?>
<script src="https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.js"></script>

<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>

<script src="https://cdn01.boxcdn.net/platform/preview/2.34.0/en-US/preview.js"></script>
<script>
    let skuForm = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        let $endPath = window.location.pathname.split("/");
        const $base = "<?php echo base_url()?>work_order";
        const $urlsavesku = "<?php echo base_url()?>work_order/saveSku";

        saveSku = function () {
            let forms=$("#form-sku");
            $("input[name=woId]").val($endPath[$endPath.length-1]);
            forms.submit(function(e) {
                e.preventDefault();
            }).validate({
                rules: {
                    NoSku:{
                        required: true,
                        minlength: 2
                    },
                    tgl:{
                        required: true,
                        minlength: 2
                    },
                    tujuan:{
                        required: true,
                        minlength: 2
                    },
                    deskripsi:{
                        required: true,
                    },
                    filesku:{
                        required: true
                    }
                },
                submitHandler: function(forms) {
                    $.ajax({
                        url:$urlsavesku,
                        type:"post",
                        data:new FormData(forms),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(result) {
                            result = $.parseJSON(result);
                            if(result.result == 'true'){
                                swalSuccess(result.message, function (x) {
                                    if (x){
                                        document.location.href= $base;
                                    }
                                });

                            }else{
                                swalAlert(result.message);
                            }

                        }
                    });
                }
            });
        };

        cancelBtn = function () {
            $(".cancel-btn").on("click",function(e){
                e.preventDefault();
                if(e.handler != true){
                    document.location.href= $base;
                    e.handler = true;
                }
            });
        };

        upload = function() {

            $('input[type="submit"]').prop("disabled", true);

            var a=0;

            $('input[name=filesku]').bind('change', function() {
                if ($('input:submit').attr('disabled',false)){
                    $('input:submit').attr('disabled',true);
                }

                var ext = $('input[name=filesku]').val().split('.').pop().toLowerCase();

                var str = $(".custom-file-label").text();

                if (str.length > 45){
                    $(".custom-file-label").text(str.substring(0,40));
                }

                if ($.inArray(ext, ['gif','png','jpg','jpeg', 'doc', 'docx', 'xls', 'xlsx', 'pdf']) == -1){

                    $('#error1').slideDown("slow");
                    $('#error2').slideUp("slow");

                    a=0;
                }else{
                    var picsize = (this.files[0].size);

                    if (picsize > 5000000){
                        $('#error2').slideDown("slow");

                        a=0;
                    }else{
                        a=1;

                        $('#error2').slideUp("slow");
                    }
                    $('#error1').slideUp("slow");

                    if (a==1){
                        $('input:submit').attr('disabled',false);
                    }
                }
            });
        };

        return {
            init: function () {
                saveSku(), upload(), cancelBtn();
            }
        }
    }();


    $(document).ready(function () {

        $(document).ready(function () {
            [".tgl"]
                .forEach(function (key) {
                    $(key).datepicker({dateFormat: 'dd/mm/yy'});

                    if (!$(key).val()) {
                        $(key).datepicker("setDate", new Date());
                    }
                });

        });

        bsCustomFileInput.init();
        skuForm.init();


    });

</script>
