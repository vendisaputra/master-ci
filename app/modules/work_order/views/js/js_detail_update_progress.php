<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($tjs)) {
    echo $tjs;

}
?>
<script src="https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.js"></script>

<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>

<script src="https://cdn01.boxcdn.net/platform/preview/2.34.0/en-US/preview.js"></script>
<script>
    let detailUpdateProgress = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        const $tbl = $("#dt-user");
        const $tblbd = $("#dt-user tbody");
        let $endPath = window.location.pathname.split("/");
        const $url = "<?php echo base_url()?>work_order/update_progress/getDetailUpdateProgress";
        const $base = "<?php echo base_url()?>work_order/update_progress";
        const $urlgetprogress = "<?php echo base_url()?>work_order/update_progress/getProgress";

        initial = function () {
            $("input[name='csrf_izin']").val($token);

            $tbl.DataTable({
                'iDisplayLength': 10,
                "responsive": true,
                "processing": true,
                "serverSide": true,
                ajax: {
                    url: $url,
                    type: "post",
                    data: function (d) {
                        d.id = $endPath.pop();
                        d.csrf_izin = $token;
                        $("input[name='csrf_izin']").val($token);
                    },
                    dataSrc: function (dt) {
                        $token = dt.token;
                        $("input[name='csrf_izin']").val(dt.token);

                        return dt.data;
                    }
                },
                buttons: [{
                    text: '<i class="fal fa-user-plus mr-1"></i> Add User',
                    name: 'add',
                    className: 'btn-primary btn-md mr-1 mt-n1 btnadduser'
                }],
                dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'l><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'Bf>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                columns: [{
                    data: 0,
                    "orderable": false,
                },{
                    data: 1,
                    "orderable": true,
                }, {
                    data: 2,
                    "orderable": true
                }, {
                    data: 3,
                    "orderable": true,
                }, {
                    data: 4,
                    "orderable": true,
                }, {
                    data: 5,
                    "orderable": true
                }, {
                    data: 6,
                    "orderable": true,
                    render:  function (x, y, z) {
                        return '<a href="'+z[8]+'" download>' + x +'</a>';
                    }
                }],
                "rowCallback": function (row, data, iDisplayIndex) {
                    var pg = $tbl.DataTable().page.info();
                    var index = iDisplayIndex + 1;
                    $('td:eq(0)', row).html(index + pg.start);
                    return row;
                }
            });
        };

        preview = function ($id) {
            $.post($urlgetprogress, {
                id: $id,
                csrf_izin: $token
            }, function (xy) {
                xy = $.parseJSON(xy);
                if (xy.file.type == "application/pdf" || xy.file.type == "image/jpeg" || xy.file.type == "image/png"){
                    createPreviewFile([{
                        type: xy.file.type,
                        path: xy.file.path
                    }], ".preview-file", false);
                }else{
                    $(".preview-file").html('<img class="img-responsive img-fluid" src="<?=base_url()."/assets/img/no-preview.jpg";?>" alt="" width="300px">');
                }
            });
        };

        actionPreview = function () {
            var table = $('#dt-user').DataTable();

            $('#dt-user tbody').on('click', 'tr', function () {
                var data =  table.row( this ).data();
                preview(data[7]);
            });
        };

        back = function () {
            $(".kembali").on("click", function () {
                window.location.href = $base;
            })
        };


        return {
            init: function () {
                initial(), actionPreview(), back();
            }
        }
    }();



    $(document).ready(function () {

        bsCustomFileInput.init();
        detailUpdateProgress.init();
        $(".dataTables_filter").css("display", "none");



    });

</script>