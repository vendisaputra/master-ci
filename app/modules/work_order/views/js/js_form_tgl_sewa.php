<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($fjs)) {
    echo $fjs;
}
if (isset($tjs)) {
    echo $tjs;
}
?>
<script>
    let WorkorderList = function () {
        let $token = "<?php echo $this->security->get_csrf_hash(); ?>";
        let $endpath = window.location.pathname.split("/").pop();
        const $base = "<?php echo base_url()?>work_order/tgl_sewa";
        const $urlgetbiaya = "<?php echo base_url()?>work_order/tgl_sewa/getBiaya";
        const $urlsave = "<?php echo base_url()?>work_order/tgl_sewa/save";
        let $listFormInput = new Array();
        let $listFormInput2 = new Array();
        var dateEnd = null, startDate = null;


        initial = function () {
            $.post($urlgetbiaya, {
                idWo: $endpath,
                csrf_izin: $token
            }, function (xy) {
                xy = $.parseJSON(xy);

                $("input[name='csrf_izin']").val($token);

                $("form :input").each(function(){
                    $(this).attr("autocomplete", "off");
                });

                $.each(xy.data, function (i, item) {

                    $("input[name=idWo]").val($endpath);
                    $("input[name=idWoBiaya]").val(item.id_izin);
                    $("input[name=idWoApproval]").val(item.id);
                    $("input[name=idDetailBiaya]").val(item.biaya_detail_id);
                    $(".keterangan").val(item.keterangan);

                    var selectinput = ' <div class="form-group">' +
                        '                            <div class="row">' +
                        '                                <div class="col-lg"' +
                        '                                    <label>' + item.nama + '</label>' +
                        '                                    <div class="row">' +
                        '                                       <div class="col-10">' +
                        '                                           <input type="hidden" name="id-biaya[]" value="' + item.biaya_id + '" class="form-control " value="Generate System" readonly>' +
                        '                                           <input type="text" name="nominal[]" value="' + item.nominal + '" class="form-control '+item.nama.replace(/ /g,"-")+"-val"+'" placeholder="">' +
                        '                                       </div>' +
                        '                                   </div>' +
                        '                            </div>' +
                        '                        </div>';

                    if (item.biaya_id == 2) {
                            selectinput = selectinput + '<br><div class="form-group">' +
                            '                            <div class="row">' +
                            '                                <div class="col-lg-6">' +
                            '                                      <div class="input-group mb-3">' +
                            '                                           <div class="input-group-append">' +
                            '                                               <span class="input-group-text" style="font-size:10px" id="basic-addon2">Tgl Mulai</span>' +
                            '                                            </div>' +
                            '                                            <input type="text" name="tglmulai" data-date-format="dd/mm/yyyy" id="tglmulai" value="'+ formateDate(item.mulai) +'" class="form-control" placeholder="Tanggal Mulai"  oninput="bindVal(this)" onchange="bindVal(this)">' +
                            '                                      </div>' +
                            '                                </div>' +
                            '                                <div class="col-lg-6">' +
                            '                                      <div class="input-group mb-3">' +
                            '                                           <div class="input-group-append">' +
                            '                                               <span class="input-group-text" style="font-size:10px" id="basic-addon2">Tgl Selesai</span>' +
                            '                                            </div>' +
                            '                                            <input type="text" name="tglselesai" id="tglselesai" value="' + monthDiff(formateDate(item.mulai), formateDate(item.selesai)) + '" class="form-control" placeholder="Tanggal Selesai"  oninput="bindVal(this)" onchange="bindVal(this)" readonly>' +
                            '                                      </div>' +
                            '                                <div>' +
                            '                            </div>';

                        dateEnd = item.selesai;
                        startDate = item.mulai;

                    }

                    if (item.nominal != null && item.biaya_id != 16 && item.biaya_id != 15) {
                        $("select[name=form-biaya] option[value='" + item.nama.replace(/ /g, "-") + "-" + item.biaya_id + "']").remove();
                        $listFormInput.push($(selectinput));
                        $listFormInput2 = $listFormInput;
                        sortingArray($listFormInput2);
                        sortlistInput();

                        $("input[name='nominal[]']").prop('readonly', true);

                        $("input#tglmulai").on("change", function () {
                            $("input#tglselesai").val(monthDiff(formateDate(startDate), formateDate(dateEnd), $("input#tglmulai").val()));
                        });
                    } else if (item.biaya_id == 16) {
                        $(".id-" + item.biaya_id).val(item.biaya_id);
                        $("." + item.nama.replace(/ /g,"-")).val(item.nominal);
                    } else if (item.biaya_id == 15){
                        $(".id-" + item.biaya_id).val(item.biaya_id);
                        $("." + item.nama.replace(/ /g,"-")).val(item.nominal);
                    }

                    $(".nama-pelanggan").html(item.nama_pelanggan);
                    $(".no-so").html(item.sales_no);
                    $(".layanan").html(item.layanan);
                    $(".origanating").html("-");
                    $(".terminating").html(item.alamat);

                    $("input#tglmulai").on("click", function () {
                    });

                });
                ['#tglmulai']
                    .forEach(function (key) {
                        $(key).datepicker({dateFormat: 'dd/mm/yy'});

                    });
            });


        };

        formatNumber = function(number){
            return parseInt(number).toLocaleString()
        };

        save = function () {
            let forms = $("#form-biaya-perizinan");
            forms.submit(function (e) {
                e.preventDefault();
            }).validate({
                rules: {
                    idWoBiaya: {
                        required: true,
                        minlength: 1
                    }
                },
                submitHandler: function (forms) {
                    $.ajax({
                        url: $urlsave,
                        type: "post",
                        data: new FormData(forms),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (result) {
                            result = $.parseJSON(result);
                            if (result.result == 'true') {
                                swalSuccess(result.message, function (x) {
                                    if (x){
                                        document.location.href= $base;
                                    }
                                });
                            } else {
                                swalAlert(result.message);
                            }

                        }
                    });
                }
            });
        };

        bindVal = function (context) {
            $(context).attr("value", $(context).val());
        };

        showInput = function () {
            let $serialNumberId = 0;
            $('select[name=form-biaya]').on('change', function () {
                var selected = this.value,
                    title = $('select[name=form-biaya] option:selected').html(),
                    idBiaya = selected.split("-").pop();
                var selectinput = ' <div class="form-group ' + selected + '">' +
                    '                            <div class="row">' +
                    '                                <div class="col-lg"' +
                    '                                    <label>' + title + '</label>' +
                    '                                    <div class="row">' +
                    '                                       <div class="col-10">' +
                    '                                           <input type="hidden" name="id-biaya[]" value="' + idBiaya + '" class="form-control" value="Generate System" readonly>' +
                    '                                           <input type="text" name="nominal[]" class="form-control '+title.replace(/ /g, "-")+'-val" placeholder="' + title + '" oninput="bindVal(this)" onchange="bindVal(this)" value="">' +
                    '                                       </div>' +
                    '                                       <div class="col-1">' +
                    '                                           <button type="button" class="btn btn-danger" onclick="deleteInput(' + "'" + selected + "', " + $listFormInput.length + ');" style="margin-left:0px;"><i class="fa fa-trash"></i></button>' +
                    '                                       </div>' +
                    '                                   </div>' +
                    '                            </div>' +
                    '                        </div>';

                if (title == "Biaya Sewa Pertahun") {
                    selectinput = selectinput + '<br><div class="form-group ' + selected + '">' +
                        '                            <div class="row">' +
                        '                                <div class="col-lg-6">' +
                        '                                      <div class="input-group mb-3">' +
                        '                                           <div class="input-group-append">' +
                        '                                               <span class="input-group-text" style="font-size:10px" id="basic-addon2">Tgl Mulai</span>' +
                        '                                            </div>' +
                        '                                            <input type="text" name="tglmulai" data-date-format="dd/mm/yyyy" id="tglmulai" value="" class="form-control" placeholder="Tanggal Mulai"  oninput="bindVal(this)" onchange="bindVal(this)">' +
                        '                                      </div>' +
                        '                                </div>' +
                        '                                <div class="col-lg-6">' +
                        '                                      <div class="input-group mb-3">' +
                        '                                           <div class="input-group-append">' +
                        '                                               <span class="input-group-text" style="font-size:10px" id="basic-addon2">Tgl Selesai</span>' +
                        '                                            </div>' +
                        '                                            <input type="text" name="tglselesai" data-date-format="dd/mm/yyyy" id="tglselesai" value="" class="form-control" placeholder="Tanggal Selesai"  oninput="bindVal(this)" onchange="bindVal(this)">' +
                        '                                      </div>' +
                        '                                </div>' +
                        '                            </div>';

                }

                $listFormInput.push($(selectinput));
                $listFormInput2 = $listFormInput;
                sortingArray($listFormInput2);
                sortlistInput();

                $("select[name=form-biaya] option[value='" + this.value + "']").remove();


            });
        };

        sortingArray = function($array){
            $array.sort(function(a, b){
                // ASC  -> a.length - b.length
                // DESC -> b.length - a.length
                return a.length - b.length;
            });
        };

        sortlistInput = function () {
            var count = 0,
                col;
            count = $listFormInput.length;
            $listFormInput.forEach(function (item, index) {
                if (index < 6) {
                    col = 1;
                    $(".for-col-" + col).empty();
                    $(".for-col-1").empty();
                    $(".for-col-2").empty();
                    $(".for-col-3").empty();
                    $(".for-col-" + col).append($listFormInput.slice(0, index + 1));
                } else if (index > 5 && index < 12) {
                    col = 2;
                    $(".for-col-" + col).empty();
                    $(".for-col-" + col).append($listFormInput.slice(6, index + 1));
                } else if (count > 11) {
                    col = 3;
                    $(".for-col-" + col).empty();
                    $(".for-col-" + col).append($listFormInput.slice(12, index + 1));
                }

            });


            $("input[name='nominal[]']").mask('000.000.000', {reverse: true});
        };

        monthDiff = function (dateFrom, dateTo, newDate = null) {

            dateFrom = new Date(dateFrom.split("/").reverse().join("-"));
            dateTo = new Date(dateTo.split("/").reverse().join("-"));

            b = Math.floor((Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate()) - Date.UTC(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate()) ) /(1000 * 60 * 60 * 24));

            if (newDate != null) {
                newDate = new Date(newDate.split("/").reverse().join("-"));

                newDate.setDate(newDate.getDate() + b);

                console.log(formateDate(new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate())));
                return formateDate(new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate()));
            }else{
                newDate = dateFrom;

                dateFrom.setDate(dateFrom.getDate() + b);

                console.log(formateDate(new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate())));
                return formateDate(new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate()));
            }
        };

        deleteInput = function ($name, $index) {
            var forDelete = [];
            forDelete.push($listFormInput2[$index]);
            $("." + $name).remove();
            $('select[name=form-biaya]').append($('<option>', {
                value: $name,
                text: $name.substring(0, $name.length - 2).replace("-", " ")
            }));
            sortingBiaya();
            $listFormInput = $listFormInput.filter(item => !forDelete.includes(item));
            forDelete = [];
            sortlistInput();
        };

        sortingBiaya = function () {
            var options = $("select[name=form-biaya] option");
            options.detach().sort(function (a, b) {
                var at = $(a).text();
                var bt = $(b).text();
                return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
            });
            options.appendTo("select[name=form-biaya]");

            $('select[name=form-biaya]').val("");
        };

        cancel = function () {
            $(".btn-cancel").on("click", function () {
                document.location.href = $base;
            })
        };

        formateDate = function ($date) {
            var d = new Date($date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [day, month, year].join('/');
        };

        return {
            init: function () {
                initial(), cancel(), showInput(), sortingBiaya();
            }
        }
    }();

    $(document).ready(function () {
        WorkorderList.init();
    });
</script>
