<?php
if (isset($fcss)) {
echo $fcss;
}
?>

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header enable-fullscreen">
                    Closing Cancel WO
                </div>
                <div class="card-body ">
                    <ul class="nav nav-pills navtab-bg nav-justified ">
                        <li class="nav-item">
                            <a href="#ClosingCancel" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                Closing/Cancel
                            </a>
                        </li>
                        <li class="nav-item border-right border-left">
                            <a href="#BiayaSewa" data-toggle="tab" aria-expanded="false" class="nav-link">
                                Biaya Sewa
                            </a>
                        </li>
                        <li class="nav-item border-right border-left">
                            <a href="#BiayaDeposit" data-toggle="tab" aria-expanded="false" class="nav-link">
                                Biaya Deposit
                            </a>
                        </li>
                        <li class="nav-item border-right border-left">
                            <a href="#SuratIjinKerjasama" id="sik-tab" data-toggle="tab" aria-expanded="false" class="nav-link">
                                Surat Kerja sama
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#History" data-toggle="tab" aria-expanded="false" class="nav-link">
                                Histori
                            </a>
                        </li>
                    </ul>

                    <div class="border-bottom" style="margin-top: 10px;"></div>

                    <div class="tab-content ">
                        <div class="tab-pane fade show active" id="ClosingCancel">
                            <div>
                               <div>
                                   <form method="post" id="form_add_workorder">
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>No WO Perijinan:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="text" name="Nowo" class="form-control" value="Generate System" readonly>
                                                   <label for="Nowo" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                               <div class="col-lg-2">
                                                   <p>No Sales Order:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="text" name="NoSales" value="" class="form-control" disabled>
                                                   <label for="NoSales" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Tanggal Perijinan:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="text" name="tglPI" data-date-format="dd/mm/yyyy" id="tglPI" value="" class="form-control" disabled>
                                                   <label for="tglPI" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                               <div class="col-lg-2">
                                                   <p>Tangga Sales Order:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="text" name="tglSO" data-date-format="dd/mm/yyyy" id="tglSO" class="form-control" disabled>
                                                   <label for="tglSO" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Jenis Permintaan:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <select type="text" name="jenis_permintaan" class="form-control kawasan" disabled>
                                                       <option value=''>-- Jenis Permintaan --</option>
                                                       <?php foreach ($jenisMinta as $value) {
                                                           echo "<option value='".$value->id."'>".$value->nama."</option>";
                                                       }
                                                       ?>
                                                   </select>
                                                   <label for="jenis_permintaan" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Tanggal Permintaan:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="text" name="tglPermintaan" data-date-format="dd/mm/yyyy" id="tglPermintaan" class="form-control" disabled>
                                                   <label for="tglPermintaan" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                               <div class="col-lg-2">
                                                   <p>Tanggal Selesai:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="text" name="tglSelesai" data-date-format="dd/mm/yyyy" id="tglSelesai" class="form-control" disabled>
                                                   <label for="tglSelesai" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Nama Pelanggan:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="text" name="Nmplgn" class="form-control" disabled>
                                                   <label for="Nmplgn" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                               <div class="col-lg-2">
                                                   <p>NO IO:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="text" name="NoIO" class="form-control" disabled>
                                                   <label for="NoIO" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Kawasan:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <select type="text" name="kawasan" class="form-control kawasan" disabled>
                                                       <option value=''>-- Kawasan --</option>
                                                       <?php foreach ($kawasan as $value) {
                                                           echo "<option value='".$value->id."'>".$value->nama."</option>";
                                                       }
                                                       ?>
                                                   </select>
                                                   <label for="kawasan" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Kota:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <select type="text" name="kabupaten" class="form-control kabupaten" disabled>
                                                       <option value=''>-- Kabupaten --</option>
                                                       <?php foreach ($kabupaten as $value) {
                                                           echo "<option value='".$value->id."'>".$value->nama."</option>";
                                                       }
                                                       ?>
                                                   </select>
                                                   <label for="kabupaten" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Kecamatan:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <select type="text" name="kecamatan" class="form-control kecamatan" disabled>
                                                       <option value=''>-- Kecamatan --</option>
                                                   </select>
                                                   <label for="kecamatan" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                               <div class="col-lg-2">
                                                   <p>Latitude:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="text" name="Latitude" class="form-control latitude" disabled>
                                                   <label for="Latitude" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Kelurahan:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <select type="text" name="kelurahan" class="form-control kelurahan" disabled>
                                                       <option value=''>-- Kelurahan --</option>
                                                   </select>
                                                   <label for="kelurahan" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                               <div class="col-lg-2">
                                                   <p>Longitude:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="text" name="Longitude" class="form-control longitude" disabled>
                                                   <label for="Longitude" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                               <div class="col-lg-2 ">
                                                   <img class="maps" src="<?= base_url(); ?>assets/img/googlemaplogo.png" style="margin-top: -55px;" height="100px" data-toggle="modal"
                                                          data-target="">
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   Kode Pos
                                               </div>
                                               <div class="col-md-2">
                                                   <input type="text" name="kode_pos" class="form-control" disabled>
                                                   <label for="kode_pos" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   Alamat
                                               </div>
                                               <div class="col-md-9">
                                                   <textarea class="form-control" rows="5" id="alamat" name="alamat" disabled></textarea>
                                                   <label for="alamat" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Nama PIC:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <select type="text" name="nmPIC" class="form-control pic" disabled>
                                                       <option value=''>-- PIC --</option>
                                                       <?php foreach ($pic as $value) {
                                                           echo "<option value='".$value->userid."'>".$value->name."</option>";
                                                       }
                                                       ?>
                                                   </select>
                                               </div>
                                               <div class="col-lg-2">
                                                   <input type="hidden" name="generateWo" value="<?=$serialWorkorder;?>" class="form-control">
                                                   <input type="hidden" name="id" class="id form-control">
                                                   <input type="hidden" class="form-control" placeholder="Nama" name="csrf_izin" value="" />
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Mitra:</p>
                                               </div>
                                               <div class="col-lg-2">
                                                   <select type="text" name="mitra" class="form-control pic" disabled>
                                                       <option value=''>-- Mitra --</option>
                                                       <?php foreach ($mitra as $value) {
                                                           echo "<option value='".$value->id."'>".$value->nama."</option>";
                                                       }
                                                       ?>
                                                   </select>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-2">
                                                   <p>Persetujuan:</p>
                                               </div>
                                               <div class="col-lg-6">
                                                   <div class="radio">
                                                       <input type="radio" name="persetujuan" id="radio1" value="1">
                                                       <label for="radio1">
                                                           Pekerjaan Selesai
                                                       </label>
                                                       <br>
                                                       <input type="radio" name="persetujuan" id="radio2" value="0" >
                                                       <label for="radio2">
                                                           Pekerjaan Batal
                                                       </label>
                                                   </div>
                                                   <label for="persetujuan" class="error text-danger" style="display:none;">Please choose one.</label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="row">
                                               <div class="col-lg-6">
                                                   <button type="button" onclick="validation();" class="btn btn-success btn-simpan">Simpan</button>
                                                   &nbsp;
                                                   &nbsp;
                                                   <button type="button" class="btn btn-light cancel-btn">Batal </button>
                                               </div>
                                           </div>
                                       </div>


                                   </form>
                               </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="BiayaSewa">
                            <div>
                                <div>
                                    <form role="form" method="post" enctype="multipart/form-data" id="form-biaya-perizinan">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select name="form-biaya" class="form-control">
                                                    <option value="">-- Biaya --</option>
                                                    <?php
                                                    foreach ($masterBiaya as $field){
                                                        ?>
                                                        <option value="<?=str_replace(" ","-",$field->nama)."-".$field->id;?>"><?=$field->nama;?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="hidden" name="idWoBiaya" class="form-control" value="" id="">
                                                <input type="hidden" name="idWo" class="form-control" value="" id="">
                                                <input type="hidden" name="idWoApproval" class="form-control" value="" id="">
                                                <input type="hidden" name="csrf_izin" class="form-control" value="" id="">
                                            </div>
                                        </div>
                                        <div class="form-inputan">
                                            <div class="row">
                                                <div class="col-md-4 for-col-1"></div>
                                                <div class="col-md-4 for-col-2"></div>
                                                <div class="col-md-4 for-col-3"></div>
                                            </div>
                                        </div>
                                        <?php
                                        if(isset($revenue)){
                                            ?>
                                            <div class="border col-lg-8" style="padding-top: 10px;">
                                                <div class="row">
                                                    <?php
                                                    foreach ($revenue as $item){
                                                        ?>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1"><?=$item->nama;?></label>
                                                                <input type="hidden" class="form-control id-<?=$item->id;?> numbers" name="id-biaya[]" value="<?=$item->id;?>" id="exampleInputEmail1" placeholder="<?=$item->nama;?>">
                                                                <input type="text" class="form-control <?=str_replace(' ' ,'-',$item->nama);?> numbers" name="nominal[]" id="exampleInputEmail1" placeholder="<?=$item->nama;?>">
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <br>
                                            <?php
                                        }
                                            ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="BiayaDeposit">
                            <div>
                                <div>
                                    <form id="dataform" role="form">
                                        <?php $dateFormat = "dd-mm-yyyy"; ?>
                                        <input type="hidden" name="id" value="<?php echo $biayaDeposit->id ?? ''; ?>" disabled>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group ">
                                                    <label>Biaya Deposit</label>
                                                    <input type="text" class="form-control currency" name="biaya_deposit"
                                                           value="<?php echo $biayaDeposit->biaya_deposit ?? ''; ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Cara Penyerahan Deposit</label>
                                                    <div class="d-flex">
                                                        <?php $jenisSerah = ($biayaDeposit->jenis_serah_deposit ?? '') == 1 ? 1 : 2; ?>
                                                        <div class="radio">
                                                            <input type="radio" name="jenis_serah_deposit" value="1"
                                                                <?php echo $jenisSerah == 1 ? 'checked' : '' ?> disabled>
                                                            <label>Transfer</label>
                                                        </div>
                                                        <div class="radio ml-2">
                                                            <input type="radio" name="jenis_serah_deposit" value="2"
                                                                <?php echo $jenisSerah == 2 ? 'checked' : '' ?> disabled>
                                                            <label>Tunai</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Jenis Deposit</label>
                                                    <div class="d-flex">
                                                        <?php $jenisDeposit = ($biayaDeposit->jenis_deposit ?? '') == 1 ? 1 : 2; ?>
                                                        <div class="radio">
                                                            <input type="radio" name="jenis_deposit" value="1"
                                                                <?php echo $jenisDeposit == 1 ? 'checked' : '' ?> disabled>
                                                            <label>Sewa</label>
                                                        </div>
                                                        <div class="radio ml-2">
                                                            <input type="radio" name="jenis_deposit" value="2"
                                                                <?php echo $jenisDeposit == 2 ? 'checked' : '' ?> disabled>
                                                            <label>Pekerjaan</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group transfer" style="display: none;">
                                                    <label> Upload Bukti* : </label>
                                                    <?php $filename = explode("--", $biayaDeposit->bukti_tf ?? ''); ?>
                                                    <br /><a href="#modal-preview" data-toggle="modal" style="display: none;" onclick="previewfile(<?php echo $biayaDeposit->id ?? ''; ?>)"><?=end($filename) ?? ''; ?></a>
                                                    <div class="custom-file">
                                                        <input type="file" name="filebukti" onclick="upload()" class="custom-file-input" id="inputGroupFile01"
                                                               aria-describedby="inputGroupFileAddon01">
                                                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                        <span>( pdf, jpg, png. Masksimal 5 Mb )</span>
                                                        <p id="error1" style="display:none; color:#FF0000; margin-top: 5px;">
                                                            Format tidak sesuai ! Unggah file Dokumen atau Gambar.
                                                        </p>
                                                        <p id="error2" style="display:none; color:#FF0000; margin-top: 5px;">
                                                            Maksimal ukuran file 5MB.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="form-group tunai">
                                                    <label>No Bukti Tanda Terima</label>
                                                    <input type="text" class="form-control" name="no_bukti_terima_deposit"
                                                           value="<?php echo $biayaDeposit->no_bukti_terima_deposit ?? ''; ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>No MI Permohonan Jaminan </label>
                                                    <input type="text" class="form-control" name="no_mi_permohonan_jaminan"
                                                           value="<?php echo $biayaDeposit->no_mi_permohonan_jaminan ?? ''; ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group tunai">
                                                    <label>Tanggal Tanda Terima Deposit</label>
                                                    <input type="text" class="form-control" name="tgl_terima_deposit"
                                                           data-date-format="<?php echo $dateFormat; ?>"
                                                           value="<?php echo $biayaDeposit->tgl_terima_deposit ?? ''; ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Tanggal MI Permohonan Jaminan</label>
                                                    <input type="text" class="form-control" name="tgl_mi_permohonan_jaminan"
                                                           data-date-format="<?php echo $dateFormat; ?>"
                                                           value="<?php echo $biayaDeposit->tgl_mi_permohonan_jaminan ?? ''; ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <?php $picSerah = $biayaDeposit->pic_serah ?? ''; ?>
                                                <div class="form-group">
                                                    <label>PIC Penyerahan Deposit</label>
                                                    <select name="pic_serah" class="form-control" disabled>
                                                        <option value='' disabled
                                                            <?php echo $picSerah == '' ? 'selected' : '' ?>>-- PIC --
                                                        </option>
                                                        <?php foreach ($pic as $pics) {
                                                            echo '<option value="' . $pics->userid . '" '
                                                                . ($picSerah == $pics->userid ? 'selected' : '') . '>' . $pics->name . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Tanggal Penyerahan Jaminan</label>
                                                    <input type="text" class="form-control" name="tgl_serah_jaminan"
                                                           data-date-format="<?php echo $dateFormat; ?>"
                                                           value="<?php echo $biayaDeposit->tgl_serah_jaminan ?? ''; ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <?php $picTerima = $biayaDeposit->pic_terima ?? ''; ?>
                                                <div class="form-group">
                                                    <label>PIC Penerimaan Deposit</label>
                                                    <select name="pic_terima" class="form-control" disabled>
                                                        <option value='' disabled
                                                            <?php echo $picTerima == '' ? 'selected' : '' ?>>-- PIC --
                                                        </option>
                                                        <?php foreach ($pic as $pics) {
                                                            echo '<option value="' . $pics->userid . '" '
                                                                . ($picTerima == $pics->userid ? 'selected' : '') . '>' . $pics->name . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>No Surat Penarikan Jaminan</label>
                                                    <input type="text" class="form-control" name="no_tarik_jaminan"
                                                           value="<?php echo $biayaDeposit->no_tarik_jaminan ?? ''; ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <div class="d-flex">
                                                        <?php $status = $biayaDeposit->status ?? 0; ?>
                                                        <div class="radio">
                                                            <input type="radio" name="status" value="1"
                                                                <?php echo $status == 1 ? 'checked' : '' ?> disabled>
                                                            <label>Close</label>
                                                        </div>
                                                        <div class="radio ml-2">
                                                            <input type="radio" name="status" value="0"
                                                                <?php echo $status == 0 ? 'checked' : '' ?> disabled>
                                                            <label>Open</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Tanggal Surat Penarikan Jaminan</label>
                                                    <input type="text" class="form-control" name="tgl_tarik_jaminan"
                                                           data-date-format="<?php echo $dateFormat; ?>"
                                                           value="<?php echo $biayaDeposit->tgl_tarik_jaminan ?? ''; ?>" disabled>
                                                </div>
                                            </div>
                                        </div>

                                        <?php if (isset($isApproval) && $isApproval) { ?>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Persetujuan</label>
                                                        <div class="d-flex">
                                                            <?php $approval = ($biayaDeposit->approval ?? '') == 1 ? 1 : 0; ?>
                                                            <div class="radio">
                                                                <input type="radio" name="approval" value="1"
                                                                    <?php echo $approval == 1 ? 'checked' : '' ?> disabled>
                                                                <label>Setuju</label>
                                                            </div>
                                                            <div class="radio ml-2">
                                                                <input type="radio" name="approval" value="0"
                                                                    <?php echo $approval == 0 ? 'checked' : '' ?> disabled>
                                                                <label>Tidak Setuju</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Keterangan</label>
                                                        <textarea class="form-control" name="keterangan" rows="3" disabled>
                                        <?php echo htmlspecialchars($biayaDeposit->keterangan ?? ''); ?>
                                    </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="SuratIjinKerjasama">
                            <div>
                                <div>
                                    <form role="form" enctype="multipart/form-data" id="formSIK">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p>No WO Perijinan:</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="Nowo" class="form-control" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">

                                                <div class="col-lg-2">
                                                    <p>Tanggal:</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="tglPI" data-date-format="dd/mm/yyyy" class="form-control" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">

                                                <div class="col-lg-2">
                                                    <p>No Surat Ijin Kerja:</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="NoSKI" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">

                                                <div class="col-lg-2">
                                                    <p>Tanggal:</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="tglSKI" data-date-format="dd/mm/yyyy" class="form-control" id="tglSKI">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">

                                                <div class="col-lg-2">
                                                    <p>Tanggal Mulai Ijin:</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="tglMulai" data-date-format="dd/mm/yyyy" class="form-control" id="tglMulai">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">

                                                <div class="col-lg-2">
                                                    <p>Tanggal Selesai Ijin:</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="tglSelesaiSIK" data-date-format="dd/mm/yyyy" class="form-control" id="tglSelesaiSIK">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">

                                                <div class="col-lg-2">
                                                    <p>Dikeluarkan Oleh:</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="dikeluarkanOleh" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">

                                                <div class="col-lg-2">
                                                    <p>Deskripsi:</p>
                                                </div>
                                                <div class="col-md-5">
                                                    <textarea name="deskripsi" class="form-control" id="" cols="" rows="5"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row upload-sik form-group">
                                            <div class="col-lg-2">
                                                <p>Upload File SIK:</p>
                                            </div>
                                            <div class="custom-file col-lg-3" style="padding-bottom: 5px;">
                                                <input type="file" name="filesik" onclick="upload()" class="custom-file-input" id="inputGroupFile01"
                                                       aria-describedby="inputGroupFileAddon01">
                                                <label class="custom-file-label ml-3" for="inputGroupFile01">Choose file</label>
                                                <label id="error1" style="display:none; color:#ff0000; margin-top: 5px;">
                                                    Format tidak sesuai ! Unggah file Dokumen atau Gambar.
                                                </label>
                                                <label id="error2" style="display:none; color:#FF0000; margin-top: 5px;">
                                                    Maksimal ukuran file 5MB.
                                                </label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="History">
                            <table id="datatable" class="table table-bordered nowrap" width="100%">
                                <tr>
                                    <th>No </th>
                                    <th>Aktivitas</th>
                                    <th>Tanggal Jam Awal</th>
                                    <th>Tanggal Jam Akhir</th>
                                    <th>PIC</th>
                                </tr>
                                <?php
                                $no = 0;
                                $this->load->helper('convertdate_helper');
                                foreach ($historyDate as $result){
                                    $no++;
                                ?>
                                <tr>
                                    <td><?=$no;?></td>
                                    <td><?=$result->nama;?></td>
                                    <td><?=date_indo_detik($result->start_date);?></td>
                                    <td><?=date_indo_detik($result->end_date);?></td>
                                    <td><?=$result->nama_pic;?></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

    <footer class="footer text-right">
        2019 © Icon+.
    </footer>

</div>