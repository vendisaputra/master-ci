<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($fcss)) {
    echo $fcss;
}
?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid bg-light text-dark ">
            <div class="card">
                <div class="card-header center enable-fullscreen">
                    Update Tanggal Sewa
                </div>
                <div class="card-body">
                    <form role="form" method="post" id="form-biaya-perizinan">
                        <div class="row">
                            <div class="col-md-4">
                                <input type="hidden" name="idWoBiaya" class="form-control" value="" id="">
                                <input type="hidden" name="idDetailBiaya" class="form-control" value="" id="">
                                <input type="hidden" name="idWo" class="form-control" value="" id="">
                                <input type="hidden" name="idWoApproval" class="form-control" value="" id="">
                                <input type="hidden" name="csrf_izin" class="form-control" value="" id="">
                            </div>
                        </div>
                        <div class="form-inputan">
                            <div class="row">
                                <div class="col-md-4 for-col-1"></div>
                                <div class="col-md-4 for-col-2"></div>
                                <div class="col-md-4 for-col-3"></div>
                            </div>
                        </div>
                        <?php
                        if(isset($revenue)){
                            ?>
                            <div class="border col-lg-8" style="padding-top: 10px;">
                                <div class="row">
                                    <?php
                                    foreach ($revenue as $item){
                                        ?>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?=$item->nama;?></label>
                                                <input type="hidden" class="form-control id-<?=$item->id;?> numbers" name="id-biaya[]" value="<?=$item->id;?>" id="exampleInputEmail1" placeholder="<?=$item->nama;?>">
                                                <input type="text" class="form-control <?=str_replace(' ' ,'-',$item->nama);?> numbers" name="nominal[]" id="exampleInputEmail1" placeholder="<?=$item->nama;?>">
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <br>
                            <?php
                        }
                        if ($this->session->userdata('tempRole') == 5){
                            ?>
                            <div class="approval-manager" style="display: none;">
                                <div class="col-lg-8 " >
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="radio">Persetujuan</label>
                                            <div class="d-flex">
                                                <div class="radio">
                                                    <input type="radio" name="persetujuan" id="radio1" value="1" />
                                                    <label for="radio1">
                                                        Setuju
                                                    </label>
                                                </div>
                                                <div class="radio ml-2">
                                                    <input type="radio" name="persetujuan" id="radio2" value="0" />
                                                    <label for="radio2">
                                                        Tidak Setuju
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="keterangan">Keterangan</label>
                                            <textarea class="form-control keterangan" name="keterangan" rows="5" cols="100"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                        ?>
                        <div class="row">
                            <?php
                            if (isset($aksi)){
                                ?>
                                <div class="col-md-4">
                                    <button type="submit" onclick="approval()" class="btn btn-primary">SIMPAN</button>
                                    &nbsp;
                                    <button type="button" class="btn btn-light previewfile"  onclick="validationSewa();" >PREVIEW</button>
                                    &nbsp;
                                    <button type="button" class="btn btn-danger btn-cancel">BATAL</button>
                                </div>
                                <?php
                            }else{
                                ?>
                                <div class="col-md-4">
                                    <button type="submit" onclick="save()" class="btn btn-primary">SIMPAN</button>
                                    &nbsp;
                                    <button type="reset" class="btn btn-danger btn-cancel">BATAL</button>

                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </form>
                </div>

            </div> <!-- container -->
        </div>

    </div> <!-- content -->

    <footer class="footer text-right">
        2019 © Icon+.
    </footer>

