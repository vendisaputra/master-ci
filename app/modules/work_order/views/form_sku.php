<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($fcss)) {
    echo $fcss;
}
?>
<div class="content-page">

    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <div class="card-header enable-fullscreen">
                    Form - Upload File SKU
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" id="form-sku">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>No SKU Perijinan:</p>
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" name="NoSku" class="form-control" value="<?php echo isset($data) ? $data->no_sku : '' ?>">
                                    <input type="hidden" name="woId" class="form-control">
                                    <input type="hidden" class="form-control" placeholder="Nama" name="csrf_izin" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                                    <label for="NoSku" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>Tanggal:</p>
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" name="tgl" class="form-control tgl" data-date-format="dd/mm/yyyy" value="<?php echo isset($data) ? $data->tgl_sku : '' ?>">
                                    <label for="tgl" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>Tujuan:</p>
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" name="tujuan" class="form-control" value="<?php echo isset($data) ? $data->tujuan : '' ?>">
                                    <label for="tujuan" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">Deskripsi</div>
                                <div class="col-md-5">
                                    <textarea class="form-control" rows="5" name="deskripsi" id="deskripsi"><?php echo isset($data) ? $data->deskripsi : '' ?></textarea>
                                    <label for="deskripsi" class="error text-danger" style="display:none;">Please choose one.</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <p> Upload Sku File : </p>
                                </div>
                                <div class="custom-file col-lg-3">
                                    <input type="file" name="filesku" onclick="upload()" class="custom-file-input" id="inputGroupFile01"
                                           aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label ml-3" for="inputGroupFile01">Choose file</label>
                                    <span>( pdf, jpg, png. Masksimal 5 Mb )</span>
                                    <p id="error1" style="display:none; color:#FF0000; margin-top: 5px;">
                                        Format tidak sesuai ! Unggah file Dokumen atau Gambar.
                                    </p>
                                    <p id="error2" style="display:none; color:#FF0000; margin-top: 5px;">
                                        Maksimal ukuran file 5MB.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                </div>
                                <div class="col-lg-3">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 button-action">
                                    <button type="submit" class="btn btn-success">Simpan </button>
                                    &nbsp;
                                    <button type="button" class="btn btn-light cancel-btn">Batal </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

    </div>
