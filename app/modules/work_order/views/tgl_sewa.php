<?php
if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * Created by vendi at 4/7/20
 */

/** @var $fcss string is form style */
if (isset($fcss)) {
    echo $fcss;
}
?>
<div class="content-page">

    <div class="content">
        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header enable-fullscreen">
                    Table Update Tanggal Sewa
                </div>
                <div class="card-body table-responsive table-wrapper">
                    <div class="row float-right ml-2" style="margin-right: 14px">
                        <form class="form-inline" id="form-search">
                            <select class="form-control mr-2" name="order">
                                <option value="1">NO SO</option>
                                <option value="3">NO WO</option>
                            </select>
                            <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search" aria-label="Search">
                            <div class="input-group input-daterange mr-sm-2" style="display: none;">
                                <input type="text" class="form-control" name="start_date">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">to</span>
                                </div>
                                <input type="text" class="form-control" name="end_date">
                            </div>
                            <button class="btn btn-outline-success my-2 my-sm-0 search-data" type="button">Search</button>
                        </form>
                    </div>
                    <table id="dt-TglSewa" class="table table-bordered nowrap" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>No. SO</th>
                            <th>Tanggal SO</th>
                            <th>No WO</th>
                            <th>Tanggal WO</th>
                            <th>Jenis Perizinan</th>
                            <th>PIC</th>
                            <th>Status Perizinan</th>
                            <th>Biaya Perizinan</th>
                            <th>Periode</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>No. SO</th>
                            <th>Tanggal SO</th>
                            <th>No WO</th>
                            <th>Tanggal WO</th>
                            <th>Jenis Perizinan</th>
                            <th>PIC</th>
                            <th>Status Perizinan</th>
                            <th>Biaya Perizinan</th>
                            <th>Periode</th>
                            <th>Aksi</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div> <!-- container -->

    </div>

</div>
