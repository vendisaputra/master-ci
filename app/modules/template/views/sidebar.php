<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
/**
 * Modifier by fajar at 1/27/20
 */
?>
<style>
    .enlarged .slimscroll-menu .nav-second-level {
        height: auto !important;
        max-height: 50vh !important;
        overflow-y: auto !important;
    }
</style>
<div id="wrapper">
    <div class="topbar">
        <div class="topbar-left">
            <a href="<?php echo base_url(); ?>dashboard" class="logo">
                <span>Aplikasi Perizinan</span>
                <i><img src="<?php echo base_url(); ?>assets/img/icon+.png" alt="icon+" height="28"></i>
            </a>
        </div>
        <nav class="navbar-custom">
            <ul class="list-unstyled topbar-right-menu float-right mb-0">
                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user"
                       data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
                        <img src="<?php echo base_url(); ?>assets/img/avatar/avatar.png"
                             alt="<?php echo $this->controller->session->userdata("tempNama"); ?>"
                             class="rounded-circle">
                        <span class="ml-1">
                            <?php echo $this->controller->session->userdata("tempNama"); ?>
                            <i class="mdi mdi-chevron-down"></i>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <a href="<?php echo base_url() ?>auth/logout" class="dropdown-item notify-item">
                            <i class="fi-power"></i> <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left waves-light waves-effect">
                        <i class="dripicons-menu"></i>
                    </button>
                </li>
            </ul>
        </nav>
    </div>
    <div class="left side-menu">
        <div class="slimscroll-menu" id="remove-scroll">
            <div id="sidebar-menu">
                <ul class="metismenu" id="side-menu">
                    <li class="menu-title">Navigasi</li>
                    <?php
                    $this->load->helper("menu");
                    echo parseMenu($menu);
                    ?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>