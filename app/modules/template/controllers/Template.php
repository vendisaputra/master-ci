<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Modifier by fajar at 1/27/20
 */
class Template extends CI_Controller
{

    public function index()
    {
        $this->assetor->clear();

        $this->groupStyle();
        $this->groupScript();

        $this->session->set_flashdata("alert", "Generate template berhasil !");

        redirect(base_url(), "auto");
    }

    public function groupStyle()
    {
        $this->assetor->load("main.css", "core-style");
        $this->assetor->load("sweetalert2.bundle.css", "core-style");
        $this->assetor->load("fixedHeader.bootstrap.min.css", "core-style");
        $this->assetor->load("responsive.bootstrap.min.css", "core-style");
        $this->assetor->load("bootstrap-datepicker.min.css", "core-style");
        $this->assetor->load("tooltipster.bundle.min.css", "form-style");
        $this->assetor->load("tooltipster-sideTip-punk.min.css", "form-style");
        $this->assetor->load("../lib/datepicker/css/1.12.1/jquery-ui.css", "form-style");

        $this->assetor->merge(true);
        $this->assetor->minify(true);
    }

    public function groupScript()
    {
        $this->assetor->load("modernizr.min.js", "core-script");
        $this->assetor->load("jquery.min.js", "core-script");
        $this->assetor->load("popper.min.js", "core-script");
        $this->assetor->load("bootstrap.min.js", "core-script");
        $this->assetor->load("metisMenu.min.js", "core-script");
        $this->assetor->load("waves.js", "core-script");
        $this->assetor->load("jquery.slimscroll.js", "core-script");

        $this->assetor->load("sweetalert2.bundle.js", "core-script");
        $this->assetor->load("jquery.core.js", "core-script");
        $this->assetor->load("jquery.app.js", "core-script");
        $this->assetor->load("custom.js", "core-script");
        $this->assetor->load("bootstrap-datepicker.min.js", "core-script");
        $this->assetor->load("jquery.validate.min.js", "core-script");
        $this->assetor->load("bs-custom-file-input.min.js", "core-script");

        $this->assetor->load("jquery.dataTables.min.js", "data-tables-script");
        $this->assetor->load("dataTables.bootstrap4.min.js", "data-tables-script");
        $this->assetor->load("dataTables.fixedHeader.min.js", "data-tables-script");
        $this->assetor->load("dataTables.responsive.min.js", "data-tables-script");
        $this->assetor->load("responsive.bootstrap.min.js", "data-tables-script");

        $this->assetor->load('tooltipster.bundle.min.js', 'form-script');
        $this->assetor->load('jquery.validate.min.js', 'form-script');
        $this->assetor->load('additional-methods.min.js', 'form-script');
        $this->assetor->load('messages_id.min.js', 'form-script');
        $this->assetor->load('jquery.form.js', 'form-script');
        $this->assetor->load('jquery.mask.js', 'form-script');
        $this->assetor->load("../lib/datepicker/js/1.12.1/jquery-ui.js", "form-script");

        $this->assetor->load('Chart.min.js', 'chart-script');


        $this->assetor->merge(true);
    }

}
