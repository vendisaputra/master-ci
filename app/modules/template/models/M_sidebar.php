<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sidebar extends CI_Model
{

    var $tbl_permission = "permission";
    var $tbl_menu = "menu";

    function getmenu($role)
    {
        $this->db->where("roleid", $role);
        $this->db->where("menu.isactive", 1);
        $this->db->where("permission.isactive", 1);
        $this->db->order_by('permission.menuid', 'asc');
        $this->db->select('permission.menuid,menu.menuname,menu.parentid,menu.path,menu.icon');
        $this->db->from($this->tbl_permission);
        $this->db->join($this->tbl_menu, 'permission.menuid = menu.menuid');
        $query = $this->db->get();

        return $query->result();
    }

}

/* End of file M_Sidebar.php */
/* Location: .//C/xampp/htdocs/cbc-lite/cbc_app/modules/template/models/M_Sidebar.php */