<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php echo $js; ?>
<?php echo $fjs; ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/min/slidercaptcha.min.js?v=1.8"></script>
<script>
    /**
     * Modifier by fajar at 1/27/20
     */
    var Login = function () {
        const form = $("form#loginform");
        const forminput = $('#loginform input');

        initial = function () {
            forminput.tooltipster({
                theme: 'tooltipster-punk'
            });

            <?php if (($this->session->flashdata("alert"))) { ?>
            var msg = '<?php echo $this->session->flashdata("alert"); ?>';

            if (msg.toLowerCase().includes("berhasil")) {
                swalSuccess(msg);
            } else {
                swalAlert(msg);
            }
            <?php } ?>
        };
        send = function () {
            form.validate({
                rules: {
                    username: {
                        required: true,
                        minlength: 2
                    },
                    password: {
                        required: true,
                        minlength: 2,
                        maxlength: 20
                    }
                },
                message: {
                    username: {
                        required: 'Kolom nama pengguna wajib diisi'
                    },
                    password: {
                        required: 'Kolom kata sandi wajib diisi'
                    }
                },
                submitHandler: function (form) {
                    var captcha = $("#loginform [data-content=captcha]");

                    if (captcha) {
                        if (captcha.data("success")) {
                            form.submit();
                        } else if (captcha.data("show")) {
                            captcha.data("show")();
                        }
                    }
                },
                errorPlacement: function (error, element) {
                    if ($(error).text()) {
                        $(element).tooltipster('content', $(error).text());
                        $(element).tooltipster('show');
                    }
                },
                success: function (label, element) {
                    $(element).tooltipster('close');
                }
            });
        };
        return {
            init: function () {
                initial();
                send();
            }
        }
    }();
    $(document).ready(function () {
        Login.init();
    });
</script>