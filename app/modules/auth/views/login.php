<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo $fcss; ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/min/slidercaptcha.min.css"/>
<?php
/**
 * Modifier by fajar at 1/27/20
 */
?>
<body class="bg-accpunt-pages">
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="wrapper-page">
                    <div class="account-pages">
                        <div class="account-box" style="max-width:360px">
                            <div class="account-logo-box">
                                <h5 class="text-center">
                                    <a href="<?php echo base_url(); ?>" class="text-dark">
                                        <span>Aplikasi Perizinan</span>
                                    </a>
                                </h5>
                            </div>
                            <div class="account-content">
                                <form id="loginform" class="form-horizontal" novalidate=""
                                      action="<?php echo base_url(); ?>auth/login" method="post">
                                    <div class="form-group m-b-20 row">
                                        <div class="col-12">
                                            <label for="username">Nama Pengguna</label>
                                            <input class="form-control" type="text" id="username" name="username"
                                                   placeholder="Masukkan Nama Pengguna"
                                                   required autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-20">
                                        <div class="col-12">
                                            <a href="<?php echo base_url(); ?>auth/recovery"
                                               class="text-muted pull-right" style="display:none;">
                                                <small>Lupa Kata Sandi?</small>
                                            </a>
                                            <label for="password">Kata Sandi</label>
                                            <input class="form-control" type="password" id="password" name="password"
                                                   placeholder="Masukkan Kata Sandi"
                                                   required autocomplete="off">
                                            <input type="hidden"
                                                   name="<?php echo $this->config->item('csrf_token_name'); ?>"
                                                   value="<?php echo $this->security->get_csrf_hash(); ?>"/>
                                        </div>
                                    </div>
                                    <div data-content="captcha"
                                         data-url="<?php echo base_url(); ?>assets/img/captcha/"
                                         data-target="#loginform"></div>
                                    <div class="form-group row text-center m-t-10">
                                        <div class="col-12">
                                            <button class="btn btn-block btn-primary waves-effect waves-light"
                                                    type="submit">LOGIN
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>