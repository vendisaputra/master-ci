<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo $fcss; ?>

<body>
    <div class="page-wrapper">
        <div class="page-inner bg-brand-gradient">
            <div class="page-content-wrapper bg-transparent m-0">
                <div class="flex-1">
                    <div class="container py-4 py-lg-5 my-lg-5 px-4 px-sm-0">
                        <div class="row">
                            <div class="col col-md-6 col-lg-7 hidden-sm-down">

                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4 ml-auto">

                                <h1 class="text-center fw-500 mb-3 d-sm-block d-md-block ptitle">
                                    <img src="<?php echo base_url('assets/img/logo-test.png'); ?>" alt="CBC WebApp" aria-roledescription="logo" style="width:75px;!important;"> Call Back Center
                                </h1>
                                <div class="card p-4 rounded-plus bg-faded lgn-card">
                                    <form>
                                    <?php if(($this->session->flashdata('alert'))){ 
                                                echo '<div class="alert border-danger bg-transparent text-secondary fade show" role="alert">'.
                                                    '<div class="d-flex align-items-center">'.
                                                        '<div class="alert-icon">'.
                                                            '<span class="icon-stack icon-stack-md">'.
                                                                '<i class="base-7 icon-stack-3x color-danger-900"></i>'.
                                                                '<i class="fal fa-times icon-stack-1x text-white"></i>'.
                                                            '</span>'.
                                                        '</div>'.
                                                        '<div class="flex-1">'.
                                                            '<span class="h6" style="color:white !important">'.$this->session->flashdata("alert").'</span>'.
                                                        '</div>'.
                                                    '</div>'.
                                                '</div>';
                                            } ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="position-absolute pos-bottom pos-left pos-right p-3 text-center text-white">
                            <span class="hidden-md-down fw-700">2019 © CBC Powered by&nbsp;<a href='http://www.iconpln.co.id/' class='text-primary fw-500' title='http://www.iconpln.co.id/' target='_blank'>http://www.iconpln.co.id/</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>