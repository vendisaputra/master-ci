<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Auth extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->model('M_auth');
    }

    function index()
    {
        $header['css'] = $this->assetor->generate('core-style');
        $data['fcss'] = $this->assetor->generate('form-style');
        $js['js'] = $this->assetor->generate('core-script');
        $js['fjs'] = $this->assetor->generate('form-script');

        $this->ci_minifier->enable_obfuscator();

        $this->load->view('template/header', $header);
        $this->load->view('login', $data);
        $this->load->view('js/login', $js);
    }

    function login()
    {
        if ($_POST) {
            $u = $this->input->post("username");
            $p = $this->input->post("password");

            $ret = $this->M_auth->login($u, $p);
            if ($ret['success'] == 'true') {
                redirect('/dashboard', 'refresh');
            } else {
                $this->session->set_flashdata('alert', $ret['message']);
                redirect(base_url(), 'refresh');
            }
        }
    }

    public function logout()
    {
        if ($this->M_auth->setnologin($this->session->userdata('tempId')) > 0) {

            $this->session->sess_destroy();
            redirect(base_url(), 'refresh');
        }
    }
}
