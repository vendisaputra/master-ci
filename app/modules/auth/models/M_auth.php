<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Modifier by fajar at 1/22/20
 */
class M_auth extends CI_Model
{

    var $tbl_appuser = "appuser";
    var $msg = null;
    var $success = null;

    function login($user, $pass, $phone = null)
    {

        $pwd = $this->chiper->encode($pass);

        $this->db->select('userid, username, name, roleid, islogin, password, unit_id, isactive');
        $this->db->where("username", $user);
      //  $this->db->where("isactive", 1);
        $sql = $this->db->get($this->tbl_appuser);

        $numrow = $sql->num_rows();

        if ($numrow == 1) {
            /** @noinspection PhpLanguageLevelInspection */
            $row = $sql->result()[0];

            if ($row->isactive == 0){
                $this->success = 'false';
                $this->msg = "User tidak aktif !";

            }else{
                if ($row->password == $pwd) {
                    /*if ($row->islogin == '0' || ($row->islogin == '1' && $row->roleid == '0')) {*/
                    $newdata = array(
                        'success' => true,
                        'tempId' => $row->userid,
                        'tempNama' => $row->username,
                        'tempFullNama' => $row->name,
                        'tempRole' => $row->roleid,
                        'tempStatusLogin' => $row->islogin,
                        'tempUnitId' => $row->unit_id,
                        'tempCall' => $phone
                    );

                    $this->setislogin($row->userid);
                    $this->session->set_userdata($newdata);
                    $this->success = 'true';
                    $this->msg = "User tidak ditemukan !";
                    /*} else {
                        $this->success = 'false';
                        $this->msg = "User sudah melakukan login !";
                    }*/
                } else {
                    $this->success = 'false';
                    $this->msg = "User tidak ditemukan !";
                }
            }
        } else {
            $this->success = 'false';
            $this->msg = "User tidak ditemukan !";
        }

        return array(
            "success" => $this->success,
            "message" => $this->msg
        );
    }

    function setislogin($id)
    {
        $this->db->set('islogin', '1');
        $this->db->set('izin_session', $this->session->session_id);           // set last session
        $this->db->where('userid', $id);
        $this->db->update($this->tbl_appuser);
    }

    function setnologin($id)
    {
        $this->db->set('islogin', '0');
        $this->db->set('izin_session', null);           // set last session null
        $this->db->where('userid', $id);
        $this->db->update($this->tbl_appuser);

        return $this->db->affected_rows();
    }

    function setnologinToSyscca($username)
    {
        $this->db->set('islogin', '0');
        $this->db->set('izin_session', null);           // set last session null
        $this->db->where('username', $username);
        $this->db->update($this->tbl_appuser);

        return $this->db->affected_rows();
    }

    function cekIsLogin($username)
    {
        $this->db->where('islogin', '1');
        $this->db->where('username', $username);
        $query = $this->db->get($this->tbl_appuser);

        return $query->num_rows() > 0;
    }

    function findBySession($session)
    {
        $this->db->where('izin_session', $session);
        $query = $this->db->get($this->tbl_appuser);

        /** @noinspection PhpLanguageLevelInspection */
        return $query->num_rows() > 0 ? $query->result()[0] : null;
    }

}

/* End of file M_Aauth.php */
/* Location: .//C/xampp/htdocs/cbc-lite/cbc_app/modules/auth/models/M_Aauth.php */