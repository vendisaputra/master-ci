<?php

/**
 * Created by fajar at 1/22/20
 */

/**
 * Check if current menu path is equal url
 *
 * @param $path string
 * @param $childCount int
 * @param $childParentActive boolean
 * @return boolean
 */
function isMenuActive($path, $childCount = null, $childParentActive = false)
{
    $path = strtolower($path);
    $currentPath = preg_replace("/\?.*/", "", strtolower(current_url()));

    if ($path != 'none' && $childCount == null) {
        return strtolower((base_url() . $path)) == $currentPath;
    } else if ($path == 'none' && $childCount != null && $childCount > 0) {
        return $childParentActive;
    }

    return strtolower((base_url() . "dashboard")) == $currentPath;
}

/**
 * Parsing menu to html link
 *
 * @param $menu array
 * @return string
 */
function parseMenu($menu)
{
    $result = "";

    foreach ($menu as $value) {
        if ($value->parentid == null && $value->path == 'None') {
            $parentNote = '<i aria-hidden="true" class="' . $value->icon . '"></i>';
            $parentNote .= '<span data-i18n="' . $value->menuname . '">' . $value->menuname . '</span>';

            $child = "";
            $childCount = 0;
            $childParentActive = false;

            foreach ($menu as $v) {
                if ($value->menuid == $v->parentid) {
                    $childCount++;

                    $childActive = isMenuActive($v->path);
                    $childParentActive = $childParentActive || $childActive;

                    $child .= '<li class="' . ($childActive ? 'active' : '') . '">';
                    $child .= '<a data-filter-tags="' . $value->menuname . '"';
                    $child .= ' style="cursor:pointer" class="nav-link"';
                    $child .= 'href="' . base_url() . strtolower($v->path) . '">';
                    $child .= '<span data-i18n="' . $v->menuname . '">' . $v->menuname . '</span>';
                    $child .= '</a>';
                    $child .= '</li>';
                }
            }

            $parentActive = isMenuActive($value->path, $childCount, $childParentActive);

            $parent = '<li class="' . ($parentActive ? 'active' : '') . '">';
            $parent .= '<a data-filter-tags="' . $value->menuname . '" ';
            $parent .= 'class="nav-link" ';
            $parent .= 'style="cursor:pointer;' . ($parentActive ? 'color: #313a46;' : '') . '"';

            if ($childCount > 0) {
                $parentNote .= '<span class="menu-arrow"></span>' . '</a>';
                $parentNote .= '<ul class="nav-second-level" aria-expanded="false">';
                $parentNote .= $child . '</ul></li>';
            } else {
                $parent .= 'href="' . base_url() . '"';
                $parentNote .= '</a></li>';
            }

            $result .= $parent . '>' . $parentNote;
        }
    }

    return $result;
}