<?php

/**
 * Created by fajar at 2/4/20
 */
if (!class_exists("QueryBuilder")) {
    /**
     * Active class to trigger fetch on data access
     */
    class ActiveRecord
    {

        private $fetchClass = array();

        private $resultClass = array();

        public function addFetchClass($key, $value, $isEager)
        {
            $this->fetchClass[$key] = $value;

            if ($isEager) {
                $this->$key = $this->__get($key);
            }
        }

        public function __get($property)
        {
            if (!isset($this->resultClass[$property]) && isset($this->fetchClass[$property])) {
                $this->resultClass[$property] = $this->fetchClass[$property]->get()->result();
            }

            return isset($this->resultClass[$property]) ? $this->resultClass[$property] : null;
        }

    }

    /**
     * Class to generate select and join with dynamic field
     * with proxies method to format with active data load on demand
     */
    class QueryBuilder extends CI_DB_query_builder
    {

        private $db;
        private $from;
        private $result;
        private $fields;
        private $rootClass;
        private $autoSelectJoin;

        private $eagerFetch = false;
        private $fetchClass = array();
        private $resultSeparator = "__";

        public $results;
        public $rootName;
        public $rootObject;

        private function parseField($table, $alias = null)
        {
            $alias = $alias == null ? $table : $alias;
            $resultSeparator = $this->resultSeparator;

            $fields = is_array($this->fields) ? $this->fields : $this->db->list_fields($table);
            $fields = is_array($fields) ? $fields : [];

            return count($fields) <= 0 ? "*" : implode(", ",
                array_map(function ($column) use ($alias, $resultSeparator) {
                    return $alias . "." . $column . " AS " . $alias . $resultSeparator . $column;
                }, $fields));
        }

        private function parseTable($table)
        {
            $re = '/^\W?(?<name>[\w-]+)\W?(\W+as\W+)?(?<alias>[\w-]+)?\W?$/i';

            preg_match($re, $table, $splits);

            return array(
                "name" => isset($splits["name"]) ? $splits["name"] : $table,
                "alias" => isset($splits["alias"]) ? $splits["alias"] : $table
            );
        }

        protected function _compile_wh($qb_key)
        {
            $where = parent::_compile_wh($qb_key);

            if ($this->rootClass != null) {
                $rootName = $this->rootClass->rootName;

                preg_match_all('/' . $rootName . '\.([^\s]+)/', $where, $matchs);

                foreach ($matchs[1] as $match) {
                    foreach (array('/\"/', "/\'/", "/\`/") as $quote) {
                        $match = preg_replace($quote, "", $match);
                    }

                    $where = str_replace($rootName . "." . $match, $this->rootObject->$match, $where);
                }
            }

            return $where;
        }

        public function init($db, $rootClass = null, $rootName = null, $fields = null)
        {
            $this->db = $db;
            $this->rootName = $this->parseTable($rootName)["alias"];
            $this->rootClass = $rootClass;
            $this->fields = $fields;

            return $this;
        }

        public function from($name)
        {
            $table = $this->parseTable($name);

            if (!isset($this->from)) {
                $this->from = $table["alias"];
            }

            if (count($this->qb_select) == 0) {
                $this->autoSelectJoin = true;

                $this->select($this->parseField($table["name"], $table["alias"]));
            }

            return parent::from($name);
        }

        public function join($name, $cond, $type = '', $escape = NULL)
        {
            $table = $this->parseTable($name);

            if ($this->autoSelectJoin) {
                $this->select($this->parseField($table["name"], $table["alias"]));
            }

            return parent::join($name, $cond, $type, $escape);
        }

        public function toString() {
            if (!isset($this->from) && isset($this->rootName)) {
                $this->from($this->rootName);
            }

            return $this->_compile_select();
        }

        public function get($name = '', $limit = NULL, $offset = NULL)
        {
            $this->result = $this->db->query($this->toString());

            return $this;
        }

        public function result()
        {
            if (!isset($this->results)) {
                $datas = call_user_func_array(array($this->result, "result"), array());

                if (is_array($datas)) {
                    $this->results = array();
                    $parentName = $this->rootName == null ? $this->from : $this->rootName;
                    $parentName = $parentName == " " ? "" : $parentName;

                    foreach ($datas as $index => $data) {
                        $this->results[$index] = new ActiveRecord();
                        $parents = array();

                        foreach (get_object_vars($data) as $field => $value) {
                            $splitField = explode($this->resultSeparator, $field);
                            $key = count($splitField) > 1 ? $splitField[0] : "";
                            $field = count($splitField) > 1 ? $splitField[1] : $splitField[0];

                            $parents[$key] = !isset($parents[$key]) ? $key : $parents[$key];
                            $parentName = $parentName == null ? array_key_first($parents) : $parentName;

                            if ($parentName == $key) {
                                $this->results[$index]->$field = $value;
                            } else {
                                if (!isset($this->results[$index]->$key)) {
                                    $this->results[$index]->$key = new ActiveRecord();
                                }

                                $this->results[$index]->$key->$field = $value;
                            }
                        }

                        foreach ($this->fetchClass as $fetchClass) {
                            $fetchName = $fetchClass->rootName;

                            $fetchClass->rootObject = $this->results[$index];

                            $this->results[$index]->addFetchClass($fetchName, $fetchClass, $fetchClass->eagerFetch);
                        }
                    }
                }
            }

            return $this->results;
        }

        public function fetch($rootName, $fields = null)
        {
            $queryBuilder = (new QueryBuilder(null))
                ->init($this->db, $this, $rootName, $fields)
                ->from($rootName);

            array_push($this->fetchClass, $queryBuilder);

            return $queryBuilder;
        }

        public function eager($val = true)
        {
            $this->eagerFetch = $val;

            return $this;
        }

        public function __call($name, $arguments)
        {
            if (method_exists($this->db, $name)) {
                return call_user_func_array(array($this->db, $name), $arguments);
            } else if ($this->result != null && method_exists($this->result, $name)) {
                return call_user_func_array(array($this->result, $name), $arguments);
            }

            return null;
        }

    }
}

if (!function_exists('queryBuilder')) {
    /**
     * Create new QueryBuilder instance
     *
     * @param    CI_DB_query_builder $db
     * @param    $rootName string root name
     * @param    $fields array list selected field
     * @return   object QueryBuilder
     */
    function queryBuilder($db, $rootName = null, $fields = null)
    {
        return (new QueryBuilder(null))->init($db, null, $rootName, $fields);
    }
}
