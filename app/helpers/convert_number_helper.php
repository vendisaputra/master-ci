<?php
defined('BASEPATH') or exit('No direct script access allowed');
    if (!function_exists('serial_number')) {
        function serial_number($number)
        {
            return str_pad($number, 3, '0', STR_PAD_LEFT);
        }
    }

    if (!function_exists('currency')) {
        function currency($number)
        {
            return number_format($number);
        }
    }

    if (!function_exists('indo_currency')) {
        function indo_currency($number, $decimal= false)
        {
            if (isset($number)) {
                if ($decimal){
                    $rupiah_result = "Rp. " . number_format($number,2,',','.');
                }else{
                    $rupiah_result = "Rp. " . number_format($number,0,'.','.');
                }
                return $rupiah_result;
            } else {
                return "";
            }
        }
    }

?>
